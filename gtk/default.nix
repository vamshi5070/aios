{  pkgs, ... }:
{
  gtk = {
    theme = {
      package = pkgs.pop-gtk-theme;
      name = "Pop dark";
    };
  };
  home.packages = with pkgs; [
    dracula-theme
    lxappearance
    papirus-icon-theme
    hicolor-icon-theme
  ];
}
