{ pkgs, ... }:{

  home.packages = with pkgs; [
      libnotify
  ];

  services.dunst = {
  	enable = true;
    #iconTheme = {
    #  name = "Adwaita";
    #  package = pkgs.gnome3.adwaita-icon-theme;
    #  size = "16x16";
    #};
		# settings = {
		# 	global = {
		# 		geometry = "600x6-90+50";
		# 		font = "Lucida MAC 20";
		# 		line_height = 12;
		# 	};
		# };
     settings = {
      global = {
        monitor = 0;
        follow = "mouse";
        geometry = "0x0-100+50";
        indicate_hidden = "yes";
        shrink = "yes";
        separator_height = 2;
        notification_height = 2;
        transparency = 0;
        padding = 8;
        horizontal_padding = 8;
        frame_width = 5;
        frame_color = "#46d9ff";
        separator_color = "auto";
        idle_threshold = 100;
        # font = "SF Pro Text 19";
        font = "39";
        # font = "Consolas bold 35";
        # markup = "full";
        #font = "xft:Ubuntu :weight=bold:pixelsize=43:antialias=true:hinting=true"; # "SF Pro Text 19";
        line_height = 0;
        format = ''<b>%s</b>\n%b'';
        alignment = "left";
        show_age_threshold = 60;
        word_wrap = "yes";
        ellipsize = "middle";
        ignore_newline = "no";
        stack_duplicates = true;
        hide_duplicate_count = false;
        show_indicators = "yes";
        icon_position = "left";
        max_icon_size = 32;
        verbosity = "mesg";
        corner_radius = 7;
      };
      shortcuts = {
          close = "ctrl+space";
      };
      urgency_low = {
        background = "#282c34"; #"#46d9ff";
        foreground = "#ffffff";
        timeout = 5;
        # icon = "/etc/nixos/services/dunst/normal.png";
      };
      urgency_normal = {
        background =  "#000000"; #"#46d9ff";
        foreground = "#fcfcfc";
        timeout = 5;
    # Icon for notifications with normal urgency, uncomment to enable
        # icon = "/etc/nixos/services/dunst/normal.png";
      };
      urgency_critical = {
        background = "#900000";
        foreground = "#ffffff";
        frame_color = "#ff0000";
        timeout = 5;
    # Icon for notifications with critical urgency, uncomment to enable
        # icon = "/etc/nixos/services/dunst/critical.png";
      };
    };
  };
}
