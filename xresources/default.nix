{ config, lib, pkgs, ... }:{
  xresources = {
    extraConfig = "#define KSYMRIGHT XK_h
                   #define KSYMLEFT XK_l
                   #define KSYDOWN  XK_j
                   #define KSYMUP   XK_k";
    properties = {
      "xmenu.borderWidth"="3";
      "xmenu.separatorWidth"="39";
      "xmenu.height"="59";
      "xmenu.width"="120";
      "xmenu.gap"="2";
      "xmenu.background"="#0f0f0f";
      "xmenu.foreground"="#bbbbbb";
      "xmenu.selbackground"="#bbbbbb";
      "xmenu.selforeground"="#0f0f0f";
      "xmenu.separator"="#555753";
      "xmenu.triangle_width"="15";
      "xmenu.triangle_height"="18";
      "xmenu.font" = "Consolas:size=28";
      # "xmenu.alignment"="right";
    };
  };
}
