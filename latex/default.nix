{  pkgs, ... }:
{
  home.packages = with pkgs;
   [
     # pkgs.texlive.combined.scheme-full
    texlive.combined.scheme-medium
   ];
}
