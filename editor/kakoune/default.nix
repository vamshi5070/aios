{ pkgs, ... }:
{
  programs.kakoune = {
    enable = true;
    config = {
	    colorScheme = "gruvbox-dark";
	    # ui = {
    	#     		assistant =  "none";
    	#     		statusLine = "top";
	    # };
# 	    keyMappings = [

# 	    ];
# 	    wrapLines.enable = true;
# 	    # hooks = [:
    };
    plugins = with pkgs.kakounePlugins; [
    #     	kak-fzf
		powerline-kak
		kakboard
    ];

    extraConfig = ''
    # map global normal '<space> s s' ': w <ret>'
    # Define custom faces to keep them organized
    set-face global PrimaryCursorNormal +r
    set-face global PrimaryCursorInsert bright-magenta+r
    set-face global PrimaryCursor PrimaryCursorNormal
    #
    # # EOL cursor
    set-face global PrimaryCursorEolNormal default,bright-cyan
    set-face global PrimaryCursorEolInsert PrimaryCursorInsert
    set-face global PrimaryCursorEol PrimaryCursorEolNormal
    #
    #
    # # [...]
    #
    hook global ModeChange (push|pop):.*insert %{
       	set-face window PrimaryCursor PrimaryCursorInsert
    	set-face window PrimaryCursorEol PrimaryCursorEolInsert
    }
        
    hook global ModeChange (push|pop):insert:.* %{
    	set-face window PrimaryCursor PrimaryCursorNormal
    	set-face window PrimaryCursorEol PrimaryCursorEolNormal
    }
	# powerline-start
	hook global WinCreate .* %{ kakboard-enable }
        hook global BufCreate .* %[
                    add-highlighter buffer/ number-lines -relative -hlcursor
                        add-highlighter buffer/ wrap
          ]
#     "hook global RegisterModified '\"' %{ nop %sh{
#           printf %s \"$kak_main_reg_dquote\" | xsel --input --clipboard

# }}
# # relative
#           #  line numbers
              
#           # Delete buffer and quit
#           map global normal <c-q> \": db;q<ret>\"
#          # set status on top
#           set-option global ui_options ncurses_status_on_top=true
          set-option -add global ui_options terminal_assistant=none
#
#           # Clipboard management mappings
#           map -docstring \"yank the selection into the clipboard\" global user y \"<a-|> xsel -i<ret>\"
#           map -docstring \"paste the clipboard\" global user p \"<a-!> xsel<ret>\"

'';
     
   };
}
