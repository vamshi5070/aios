(setq user-full-name "Vamshi krishna"
      user-mail-address "vamshi5070k@gmail.com")

(setq gc-cons-percentage 0.6)

	   (setq tab-bar-show nil)

 (setq inhibit-startup-message t)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1)
  (fset 'yes-or-no-p 'y-or-n-p)

(setq blink-cursor-interval 0.6)
(blink-cursor-mode 0)
 ;; (setq recentf-filename-handlers  
        ;; (append '(abbreviate-file-name) recentf-filename-handlers))
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)
(setq fast-but-imprecise-scrolling t)

   (defun private-config ()
		   (interactive )
		    (find-file (expand-file-name "~/aios/editor/emacs/init.el")))

    (defun reload ()
		   (interactive )
		   (load-file (expand-file-name "~/aios/editor/emacs/init.el")))

  (set-face-attribute 'default nil
                      :font "Source Code Pro"
                   :height 120
                   :weight 'regular)
(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)
(straight-use-package 'use-package)

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; (setq modus-themes-italic-constructs t
 ;; modus-themes-bold-constructs t
 ;; modus-themes-region '(bg-only no-extend))

;; (setq
 ;; modus-themes-deuteranopia t
  ;; modus-themes-syntax '(faint alt-syntax)
;; modus-themes-prompts '(background gray)	   
 ;; modus-themes-mode-line '(borderless 3d accented)
 ;; )

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(column-number-mode t)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)
(dolist (mode '(
	       ;;org-mode-hook
	       term-mode-hook
	       vterm-mode-hook
	       shell-mode-hook
	       dired-mode-hook
	       eshell-mode-hook))
 (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;;;;;;;;;;; general

(use-package general
      :straight t
  :after evil
  :config
  (general-create-definer rgv/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC"))


;;;;;;;;;;;;  evil

(use-package evil
      :straight t
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
      :straight t
  :after evil
  :ensure t
  :config
  (evil-collection-init))
;; Non-insert mode keymaps
(general-def
  :states '(normal visual motion)
  "gc" 'comment-dwim
  "gC" 'comment-line
  "j" 'evil-next-visual-line ;; I prefer visual line navigation
  "k" 'evil-previous-visual-line ;; ""
  ;; "|" '(lambda () (interactive) (org-agenda nil "n")) ;; Opens my n custom org-super-agenda view
  ;; "C-|" '(lambda () (interactive) (org-agenda nil "m")) ;; Opens my m custom org-super-agenda view
  )
;;;;;;;;;;;; frame 

  (use-package envrc    
    :straight t)
  
  (envrc-global-mode 1)

   ;; "fde" '(lambda () (interactive) (find-file (expand-file-name "~/.emacs.d/Emacs.org")))))
;;;;;; file
 (rgv/leader-keys
    "f"  '(:ignore t) ;;:which-key "files")
    "fp" 'private-config
    "fs" 'save-buffer
    "ff" 'find-file
    "fd" 'dired-jump
    ) ;;:which-key "choose theme"))
    ;; "fde" '(lambda () (interactive) (find-file (expand-file-name "~/.emacs.d/Emacs.org")))))

 (rgv/leader-keys
    "p"  '(:ignore t) 
    "pp" 'project-find-file
    "pP" 'project-switch-project
    ) 

 ;; (rgv/leader-keys
 ;;    "<tab>"  '(:ignore t) 
 ;;     ("<tab> N" 'tab-bar-new-tab-to )
 ;;     ("<tab> n" 'tab-bar-switch-to-next-tab )
 ;;     ("<tab> d" 'tab-bar-close-tab)
 ;;     ("<tab> b" 'switch-to-buffer-other-tab)
 ;;     ("<tab> f" 'find-file-other-tab)
 ;;    ("<tab> a"  'tab-bar-close-other-tabs )
 ;; 	     )
    
 (rgv/leader-keys
    "w"  '(:ignore t) 
    "w d" 'delete-window
    "w s" 'split-window-below
    "wv" 'split-window-right 
    "w C-o" 'delete-other-windows
    "ww" 'other-window
    ) 

 (rgv/leader-keys
    "b"  '(:ignore t) 
    "bb" 'switch-to-buffer
    ) 

(use-package consult
  :straight t)

 (rgv/leader-keys
    "h"  '(:ignore t) ;;:which-key "files")
    "hrr" 'reload
    "ht" 'consult-theme
    ;; "ff" 'find-file
    ) ;;:which-key "choose theme"))

 (rgv/leader-keys
    "s"  '(:ignore t) 
    "ss" 'consult-line
    ) 

	  (savehist-mode 1)
(use-package vertico
  :straight t)
 (defun rational-completion/minibuffer-backward-kill (arg)
	 "When minibuffer is completing a file name delete up to parent
       folder, otherwise delete a word"
 (interactive "p")
 (if minibuffer-completing-file-name
	 ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
     (if (string-match-p "/." (minibuffer-contents))
	 (zap-up-to-char (- arg) ?/)
       (delete-minibuffer-contents))
	   (backward-kill-word arg)))

 (use-package vertico
     :straight t
     :init
     (vertico-mode))
 (setq vertico-count 10
       vertico-cycle t)

(define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
(define-key vertico-map (kbd "C-j") #'vertico-next)
(define-key vertico-map (kbd "C-k") #'vertico-previous)
(define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

 (use-package orderless
   :straight t)
 ;; Configure a custom style dispatcher (see the Consult wiki)
 ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
 ;;       orderless-component-separator #'orderless-escapable-split-on-space)
 (setq completion-styles '(orderless)
     completion-category-defaults nil
     completion-category-overrides '((file (styles partial-completion))))

(use-package haskell-mode
   :straight t)
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
(setq  haskell-interactive-popup-errors nil)

(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")

(use-package magit
    :straight t)

 (rgv/leader-keys
    ":" 'execute-extended-command)

 (rgv/leader-keys
    "g"  '(:ignore t) 
    "gg" 'magit-status
    "gc" 'magit-clone
    ) 

(use-package evil-nerd-commenter
    :straight t
  :bind ("M-;" . evilnc-comment-or-uncomment-lines))

(use-package exotica-theme
    :straight t)

(load-theme 'modus-operandi t)

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)


;;;;;;;;;;;;;;;;; dired
(put 'dired-find-alternate-file 'disabled nil)
(define-key dired-mode-map (kbd "/") 'dired-goto-file)
(define-key dired-mode-map (kbd "j") 'dired-next-line)
(define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)
(evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-up-directory
    "l" 'dired-find-alternate-file)

(setq dired-listing-switches "-ahl -agho --group-directories-first"
				 dired-omit-files "^\\.[^.].*"
				 dired-omit-verbose nil
				 dired-hide-details-hide-symlink-targets nil
				 delete-by-moving-to-trash t)


(setq-default header-line-format mode-line-format)

(straight-use-package 'hide-mode-line)
(global-hide-mode-line-mode 1)

