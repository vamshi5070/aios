* Nano
#+begin_src emacs-lisp

	;; (straight-use-package '(nano-theme :type git :host github
	;; 					:repo "rougier/nano-theme"))
	;; 		      (use-package white-theme
	;; 			:straight t)
	;; 		    (use-package doom-themes
	;; 		      :straight t)
    ;; (load-theme 'doom-dracula t)
  ;; (load-theme 'nano-dark t)

  ;; (use-package nano-modeline
  ;; :straight t)
  ;; (nano-modeline-mode 1)
      ;; (straight-use-package
      ;;   '(nano-emacs :type git :host github :repo "rougier/nano-emacs"))
    ;;(require 'nano)
    ;; (setq nano-font-family-monospaced "Cousine")	 ;; (require 'nano-modeline)
    ;; (setq nano-font-family-proportional nil)
    ;; (setq nano-font-size 14)
    ;; (require 'nano-base-colors)
    ;; (require 'nano-faces)

	      ;; (require 'nano-layout)
#+end_src
* Nano sidebar
#+begin_src emacs-lisp   

    ;; (straight-use-package '(nano-sidebar :type git :host github
    ;; 				       :repo "rougier/nano-sidebar"))

  ;; (require 'nano-sidebar)

  ;; (nano-sidebar-toggle)
#+end_src
* Nano theme
#+begin_src emacs-lisp   
    ;; (nano-dark)
   ;;    (load-theme 'nano-dark t)

  ;; Bar cursor
  ;; (nano-mode) 
  ;; (require 'nano-theme)
  ;; (nano-dark)
  ;;(load-theme 'nano-dark t)		  ;; (load-theme 'white t)
#+end_src
* Nano modeline
#+begin_src emacs-lisp   

#+end_src

* Recentf
  #+begin_src emacs-lisp
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)
  #+end_src
* Save place
#+begin_src emacs-lisp
  (setq save-place-limit 100)
    (save-place-mode 1)
#+end_src
* External Themes
 #+begin_src emacs-lisp

		     ;; (use-package rand-theme
		     ;;   :straight t)

	     (use-package bespoke-themes
	       :straight (:host github :repo "mclear-tools/bespoke-themes" :branch "main")
	       :config
	       ;; Set use of italics
	       (setq bespoke-set-italic-comments t
		     bespoke-set-italic-keywords t)
	       ;; Set variable pitch
	       (setq bespoke-set-variable-pitch t)
	       ;; Set initial theme variant
	       (setq bespoke-set-theme 'dark))
   ;; (load-theme 'bespoke t))
	       ;; Load theme

     (use-package bespoke-modeline
       :straight (:type git :host github :repo "mclear-tools/bespoke-modeline") 
       :init
       ;; Set header line
       (setq bespoke-modeline-position 'top)
       ;; Set mode-line height
       (setq bespoke-modeline-size 3)
       ;; Show diff lines in mode-line
       (setq bespoke-modeline-git-diff-mode-line t)
       ;; Set mode-line cleaner
       (setq bespoke-modeline-cleaner t)
       ;; Use mode-line visual bell
       (setq bespoke-modeline-visual-bell t)
       ;; Set vc symbol
       (setq  bespoke-modeline-vc-symbol "G:"))
       ;; :config
       ;; (bespoke-modeline-mode))    ;; Vertical window divider
	 ;; (use-package frame
	 ;;   :straight (:type built-in)
	 ;;   :custom
	 ;;   (window-divider-default-right-width 12)
	 ;;   (window-divider-default-bottom-width 1)
	 ;;   (window-divider-default-places 'right-only)
	 ;;   (window-divider-mode t))
	 ;; Make sure new frames use window-divider
	 ;; (add-hook 'before-make-frame-hook 'window-divider-mode)

		 ;; Themes I *only* want to be selected
		 ;; (setq rand-theme-wanted '(white modus-vivendi modus-operandi))
		;; (rand-theme)
	     ;; (global-set-key (kbd "C-z") 'rand-theme-iterate)
#+end_src

* Diminish
#+begin_src emacs-lisp
  ;; (use-package diminish
  ;;   :straight t
  ;; :defer 5
  ;; :config ;; Let's hide some markers.
  ;;   (diminish  'org-indent-mode))
  
#+end_src

* Vertico posframe
#+begin_src emacs-lisp
  (use-package vertico-posframe
      :straight t
      )

  (setq vertico-posframe-parameters
	'((left-fringe . 8)
	  (right-fringe . 8)))
   ;; (vertico-posframe-mode 1)
      (ryo-modal-key
       "SPC" '(
	        ("w i" ace-window)))
#+end_src
* kakoune
  #+begin_src emacs-lisp
    (add-to-list 'load-path "~/aios/editor/emacs/")

	  (require 'kakoune)

	  (subword-mode 1)
	  (use-package ryo-modal
	    :straight t
	    :commands ryo-modal-mode
	    ;; :bind ("C-z" . ryo-modal-mode)
	    :config
	    (setq ryo-modal-cursor-color "#ffffff")
	    (ryo-modal-keys
	     ;; Basic keybindings
	     (:mc-all t)
	     ("<RET>" next-line)
	     (";" kakoune-deactivate-mark)
	     ;; ("0" beginning-of-line )
	     ("<backspace>" backward-char :first '(kakoune-deactivate-mark) )
	     ("%" mark-whole-buffer)
	     ("," ryo-modal-repeat)
	     ("`" kakoune-downcase)
	     ("~" kakoune-upcase)
	     ;;("/" consult-line)
	     ("C-=" text-scale-increase)   
	     ("C--" text-scale-decrease)
	     ("a" ryo-after :first '(kakoune-deactivate-mark))
	     ("b" subword-backward :first '(kakoune-set-mark-here))
	     ("c" kakoune-d :exit t)
	     ;; ("c" comment-line)
	     ("d" kakoune-d)
	     ("f" kakoune-select-to-char :first '(kakoune-set-mark-here ))
	     ;;("f" evil-snipe-f :first '(kakoune-set-mark-here))
	     ("h" backward-char :first '(kakoune-deactivate-mark))
	     ("i" kakoune-insert-mode :first '(kakoune-deactivate-mark))
	     ("j" next-line :first '(kakoune-deactivate-mark))
	     ("k" previous-line :first '(kakoune-deactivate-mark))
	     ("l" forward-char :first '(kakoune-deactivate-mark))
	     ("o" kakoune-o)
	     ("p" kakoune-p)
	     ("r" kakoune-replace-char)
	     ("R" kakoune-replace-selection)
	     ;;("q" ryo-modal-mode)
	     ;; ("s" save-buffer)
	     ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here ))
	     ("w" subword-forward :first '(kakoune-set-mark-here))
	     ("x" kakoune-x)
	     ("y" kill-ring-save)
	     ("A" kakoune-A)
	     ("B" subword-backward :first '(kakoune-set-mark-if-inactive))
	     ("C" kill-line :exit t)
	     ("F" kakoune-select-to-char :first '(kakoune-set-mark-if-inactive ))
	     ;;("F" evil-snipe-F :first '(kakoune-set-mark-here ))
	     ("H" backward-char :first '(kakoune-set-mark-if-inactive))
	     ("J" next-line :first '(kakoune-set-mark-if-inactive))
	     ("K" previous-line :first '(kakoune-set-mark-if-inactive))
	     ("L" forward-char :first '(kakoune-set-mark-if-inactive))
	     ("M-w" forward-symbol :first '(kakoune-set-mark-here))
	     ("O" kakoune-O)
	     ("P" kakoune-P)
	     ("T" kakoune-T)
	     ("W" subword-forward :first '(kakoune-set-mark-if-inactive))
	     ("X" kakoune-X)
	     ("ZZ" kill-this-buffer)
	     ("M-;" kakoune-exchange)
	     )

	    (ryo-modal-keys
	     ;; first argument to ryo-modal-keys may be a list of keywords.
	     ;; These keywords will be applied to all keybindings.
	     (:norepeat t)
	     ;;("0" "M-0")
	     ("1" "M-1")
	     ("2" "M-2")
	     ("3" "M-3")
	     ("4" "M-4")
	     ("5" "M-5")
	     ("6" "M-6")
	     ("7" "M-7")
	     ("8" "M-8")
	     ("7" "M-7")
	     ("9" "M-9")))
    ;;(add-hook 'prog-mode-hook #'kakoune-normal-mode)
    ;;(add-hook 'org-mode-hook #'kakoune-normal-mode)
    ;;	      (ryo-modal-mode 1)

		(ryo-modal-key
		 "SPC" '(
		     ;;("a" consult-line)
		     ;; ("b b" consult-buffer)
		     ("b [" previous-buffer)
		     ("b ]" next-buffer)
		     ("f f" find-file)
		     ("f d" dired-jump)
		     ("f s" save-buffer)
		     ("f p" private-config)
		     ;;("g g" magit-status)
		     ("h f" describe-function)
		     ("h k" describe-key)
		     ;;("h t" consult-theme)
		     ;; ("o t" vterm-toggle)
		     ;; ("j" next-buffer)
		     ;; ("k" previous-buffer)
		     ("p p" project-find-file)
		     ("p P" project-switch-project)
		     ;; ("s s" consult-line);;save-buffer)
		     ("r r" reload)
		     ("w C-o" delete-other-windows)
		     ("w d" delete-window)
		     ("w q" kill-buffer-and-window)
		     ("w s" split-window-below)
		     ("w v" split-window-right)
		     ("w w" other-window)
		     ("SPC" execute-extended-command)
		     ("<tab> N" tab-bar-new-tab-to )
		     ("<tab> n"  tab-bar-switch-to-next-tab )
		     ("<tab> d" tab-bar-close-tab)
		     ("<tab> b" switch-to-buffer-other-tab)
		     ("<tab> f" find-file-other-tab)
		     ("<tab> a" tab-bar-close-other-tabs )
		     ;; ("<tab> F" dired-other-tab)
		     ))


		(ryo-modal-key
		 "g" '(("h" beginning-of-line)
		   ("j" end-of-buffer)
		   ("g" kakoune-gg)
		   ("l" end-of-line)
		   ("c" comment-dwim)        
		   ("k" beginning-of-buffer)))

		  ;;(global-set-key (kbd "<escape>")  'kakoune-normal-mode)
	      ;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)


      (ryo-modal-key
       "SPC" '(
	        ("w i" ace-window)))

        (ryo-modal-key
             "SPC" '(
                  ("g g" magit-status)
                  ("g c" magit-clone)
                  ))


	  (ryo-modal-key
	       "SPC" '(
		    ("s i" consult-imenu)
    ("s s" consult-line)
  ;; ("h t" modus-themes-toggle)
  ("f r" consult-recent-file)	       
    ("b i" ibuffer)
    ("b b" consult-buffer)
		    ))
#+end_src

* External Themes
 #+begin_src emacs-lisp

		     ;; (use-package rand-theme
		     ;;   :straight t)

	     (use-package bespoke-themes
	       :straight (:host github :repo "mclear-tools/bespoke-themes" :branch "main")
	       :config
	       ;; Set use of italics
	       (setq bespoke-set-italic-comments t
		     bespoke-set-italic-keywords t)
	       ;; Set variable pitch
	       (setq bespoke-set-variable-pitch t)
	       ;; Set initial theme variant
	       (setq bespoke-set-theme 'dark))
   ;; (load-theme 'bespoke t))
	       ;; Load theme

     (use-package bespoke-modeline
       :straight (:type git :host github :repo "mclear-tools/bespoke-modeline") 
       :init
       ;; Set header line
       (setq bespoke-modeline-position 'top)
       ;; Set mode-line height
       (setq bespoke-modeline-size 3)
       ;; Show diff lines in mode-line
       (setq bespoke-modeline-git-diff-mode-line t)
       ;; Set mode-line cleaner
       (setq bespoke-modeline-cleaner t)
       ;; Use mode-line visual bell
       (setq bespoke-modeline-visual-bell t)
       ;; Set vc symbol
       (setq  bespoke-modeline-vc-symbol "G:"))
       ;; :config
       ;; (bespoke-modeline-mode))    ;; Vertical window divider
	 ;; (use-package frame
	 ;;   :straight (:type built-in)
	 ;;   :custom
	 ;;   (window-divider-default-right-width 12)
	 ;;   (window-divider-default-bottom-width 1)
	 ;;   (window-divider-default-places 'right-only)
	 ;;   (window-divider-mode t))
	 ;; Make sure new frames use window-divider
	 ;; (add-hook 'before-make-frame-hook 'window-divider-mode)

		 ;; Themes I *only* want to be selected
		 ;; (setq rand-theme-wanted '(white modus-vivendi modus-operandi))
		;; (rand-theme)
	     ;; (global-set-key (kbd "C-z") 'rand-theme-iterate)
#+end_src

* Diminish
#+begin_src emacs-lisp
  ;; (use-package diminish
  ;;   :straight t
  ;; :defer 5
  ;; :config ;; Let's hide some markers.
  ;;   (diminish  'org-indent-mode))
  
#+end_src

* Vertico posframe
#+begin_src emacs-lisp
  (use-package vertico-posframe
      :straight t
      )

  (setq vertico-posframe-parameters
	'((left-fringe . 8)
	  (right-fringe . 8)))
   ;; (vertico-posframe-mode 1)
#+end_src

* Dimmer
#+begin_src emacs-lisp
  ;; Dim inactive windows
  ;; (use-package dimmer
  ;;   :straight (:host github :repo "gonewest818/dimmer.el")
  ;;   :hook (after-init . dimmer-mode)
  ;;   :config
  ;;   (setq dimmer-fraction 0.5)
  ;;   (setq dimmer-adjustment-mode :foreground)
  ;;   ;; (setq dimmer-use-colorspace :rgb)
  ;;   (setq dimmer-watch-frame-focus-events nil)
  ;;   (dimmer-configure-which-key)
  ;;   (dimmer-configure-magit)
  ;;   (dimmer-configure-posframe))
  ;;(require 'setup-splash)
  #+end_src

* Mini modeline
#+begin_src emacs-lisp

  ;; (use-package mini-modeline
  ;;   :straight t
  ;;   :config
  ;;   (mini-modeline-mode t)
  ;;   )
#+end_src

* Tree sitter
#+begin_src emacs-lisp
  ;; (straight-use-package 'tree-sitter)
  ;; (straight-use-package 'tree-sitter-langs)
#+end_src

* Highlight numbers
#+begin_src emacs-lisp
  ;; (use-package highlight-numbers
  ;; :straight t
  ;;    :hook ((prog-mode conf-mode org-mode) . highlight-numbers-mode)
  ;;   :config (setq highlight-numbers-generic-regexp "\\_<[[:digit:]]+\\(?:\\.[0-9]*\\)?\\_>"))

#+end_src

* Vterm toggle
#+begin_src emacs-lisp
  ;; (use-package vterm-toggle
  ;; :straight t
  ;; :ryo
  
#+end_src

* Doom modeline
#+begin_src emacs-lisp

	     ;; (straight-use-package 'hide-mode-line)
	     ;;   (global-hide-mode-line-mode 1)

  ;; (setq-default header-line-format
      ;; nil)
	      (use-package doom-modeline
		 :straight t
		 ;; :init (doom-modeline-mode)
		 :config
		 (setq doom-modeline-buffer-file-name-style 'file-name ;; Just show file name (no path)
		   doom-modeline-enable-word-count t
		   doom-modeline-buffer-encoding nil
		   doom-modeline-icon t ;; Enable/disable all icons
		   doom-modeline-modal-icon nil ;; Icon for Evil mode
		   doom-modeline-major-mode-icon t
		   doom-modeline-major-mode-color-icon t
		   doom-modeline-height 2
		   doom-modeline-bar-width 3)
		 )


#+end_src

* TEXT SIZE MANIPULATION
#+begin_src emacs-lisp
;; zoom in/out like we do everywhere else.
#+end_src
* Font
#+begin_src emacs-lisp   
    ;; (add-hook 'org-mode-hook  #'variable-pitch-mode)
    ;; (add-hook 'prog-mode-hook  #'variable-pitch-mode)
   ;;  (set-face-attribute 'fixed-pitch nil
  ;;			 :font "SF Mono"
  ;;			 :height 200
  ;;			 :weight 'regular)
      ;; Uncomment the following line if line spacing needs adjusting.

       ;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
    ;;   (add-to-list 'default-frame-alist '(font . "SF Mono-18"))

#+end_src
* Theme
#+begin_src emacs-lisp

  #+end_src
* Basic UI
#+begin_src emacs-lisp

  ;;(load-theme 'modus-vivendi t)
#+end_src

* Undo tree
#+begin_src emacs-lisp
    (use-package undo-tree
    :straight t
    :config
    (global-undo-tree-mode)
    ;; :ryo
    ;; ("u" undo-tree-undo)
    ;; ("U" undo-tree-redo)
    :bind (:map undo-tree-visualizer-mode-map
		("h" . undo-tree-visualize-switch-branch-left)
		("j" . undo-tree-visualize-redo)
		("k" . undo-tree-visualize-undo)
		("l" . undo-tree-visualize-switch-branch-right)))
  ;; (setq undo-tree-auto-save-history t)
#+end_src
