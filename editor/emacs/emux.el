(add-to-list 'load-path "~/aios/editor/emacs/emux/")

(require 'emux-config)

(setq user-full-name "Vamshi krishna"
      user-mail-address "vamshi5070k@gmail.com")

(require 'emux-packages)

(require 'emux-tab-bar)
(global-set-key (kbd "C-c C-n") 'tab-next)
(require 'emux-appearance)

(require 'emux-org)

(require 'emux-littering)

(require 'emux-window)
(require'emux-dired)
(require 'emux-buffer)

(require 'emux-history)

(require 'emux-git)

(require 'emux-cape)

(require 'emux-recentf)

(require 'emux-bling)

(require 'emux-languages)

(require 'emux-envrc)
;; (use-package magit
;; :straight t)

(require 'emux-multiple-cursors)
 ;; (grep-apply-setting
 ;;   'grep-find-command
 ;;   '("rg -n -H --no-heading -e '' $(git rev-parse --show-toplevel || pwd)" . 27)
 ;; )
 ;; (global-set-key (kbd "C-x C-g") 'grep-find)

(require 'emux-pdf)

;;  (add-path
;;   (add-to-list 'load-path "~/aios/editor/emacs/emux/")
(require 'emux-keybindings)
