#     init = {
#       enable = true;
#       packageQuickstart = false;
#       recommendedGcSettings = true;
#       usePackageVerbose = false;

#       earlyInit = ''
#                ;; Disable some GUI distractions. We set these manually to avoid starting
#                ;; the corresponding minor modes.
#                (push '(menu-bar-lines . 0) default-frame-alist)
#                (push '(tool-bar-lines . nil) default-frame-alist)
#                (push '(vertical-scroll-bars . nil) default-frame-alist)
#         ;;       (push '(tooltip-mode . nil) default-frame-alist)
#          (load-theme 'modus-vivendi t)
#              '';
#       prelude = ''
                      
# (setq user-full-name "Vamshi krishna")
# (setq user-mail-address "vamshi5070k@gmail.com")
#            (save-place-mode 1)
#         (setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

#         (make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

#         (setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
#               auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

#         (setq create-lockfiles nil)
#                            (defalias 'yes-or-no-p 'y-or-n-p)

#                            (set-face-attribute 'default nil
#                 		          :font "Consolas "
#                 				     :height 170
#                 				   :weight 'regular)
                           
#       (setq-default line-spacing 4)
#         (setq-default cursor-type 'bar) 
#             (setq ryo-modal-cursor-color "#ffffff")
#             (blink-cursor-mode 0)

#         (defvar my-subword-forward-regexp "[[[:upper:]]*[[:digit:][:lower:]]+\\|[[:upper:]]+\\|[^[:word:][:space:]_\n]+\\|-|(|")

#             (defvar my-subword-backward-regexp "[[:space:][:word:]_\n][^\n[:space:][:word:]_]+\\|\\(\\W\\|[[:lower:]]\\)[[:upper:]]\\|\\W\\w+")
#             ;; (defvar my-subword-backward-regexp "[^[:word:][:space:]_\n]+")
#             (defun my-subword-forward-internal () ""
#         	   (interactive)  
#         	   (let ((case-fold-search nil))
#         	     (re-search-forward my-subword-forward-regexp nil t))
#         	   (goto-char (match-end 0)))

#             (defun my-subword-backward-internal () ""
#         	   (interactive)
#         	   (let ((case-fold-search nil))
#         	     (if (re-search-backward my-subword-backward-regexp nil "don't panic!")
#         		 (goto-char (1+ (match-beginning 0))))))

#             (setq subword-forward-regexp 'my-subword-forward-regexp)
#             (setq subword-backward-regexp 'my-subword-backward-regexp)
#             (setq subword-backward-function 'my-subword-backward-internal)
#             (setq subword-forward-function 'my-subword-forward-internal)
#           (subword-mode 1)

#             (defun kakoune-a (count)
#               "Select COUNT lines from the current line.
#         				   Note that kakoune's x does behaves exactly like this,
#         				   and I like this behavior better."
#               (interactive "p")
#               (forward-char count)
#               (ryo-modal-mode 0))

#             (defun kakoune-set-mark-here () "Set the mark at the location of the point."
#         	   (interactive) (set-mark (point)))

#             (defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
#         	   (interactive)
#         	   (unless (use-region-p) (set-mark (point))))

#             (defun kakoune-deactivate-mark ()
#               "Deactivate the mark.
#         				   Deactivate the mark unless mark-region-mode is active."
#               (interactive)
#               ;;(unless rectangle-mark-mode (deactivate-mark)))
#               (deactivate-mark))
#             (defun kakoune-O (count)
#               "Open COUNT lines above the cursor and go into insert mode."
#               (interactive "p")
#               (beginning-of-line)
#               (dotimes (_ count)
#         	(newline)
#         	(forward-line -1)))

#             (defun kakoune-x (count)
#               "Select COUNT lines from the current line.
#         				   Note that kakoune's x does behaves exactly like this,
#         				   and I like this behavior better."
#               (interactive "p")
#               (beginning-of-line count)
#               (set-mark (point))
#               (end-of-line count)
#               (forward-char count))

#             (defun kakoune-d (count)
#               "Kill selected text or COUNT chars."
#               (interactive "p")
#               (if (use-region-p)
#         	  (kill-region (region-beginning) (region-end))
#         	(delete-char count t)))

#             (defun kakoune-insert-mode () "Return to insert mode."
#         	   (interactive)
#         	   (ryo-modal-mode 0))

#             (defun kakoune-exchange () "Return to insert mode."
#         	   (interactive)
#         	   (exchange-point-and-mark))

#             (defun kakoune-normal-mode () "Return to insert mode."
#         	   (interactive)
#         	   (ryo-modal-mode 1))

#             (defun ryo-after () "Enter normal mode"
#         	   (interactive)
#         	   (forward-char)
#         	   (ryo-modal-mode 0))

#             (defun kakoune-X (count)
#               "Extend COUNT lines from the current line."
#               (interactive "p")
#               (beginning-of-line)
#               (unless (use-region-p) (set-mark (point)))
#               (forward-line count))

#             (defun kakoune-T (count)
#               "Extend COUNT lines from the current line."
#               (interactive "p")
#               (beginning-of-line)
#               (unless (use-region-p) (set-mark (point)))
#               (previous-line count))

#             (defun kakoune-gg (count)
#               "Go to the beginning of the buffer or the COUNTth line."
#               (interactive "p")
#               (goto-char (point-min))
#               (when count (forward-line (1- count))))

#             (defun kakoune-o (count)
#               "Open COUNT lines under the cursor and go into insert mode."
#               (interactive "p")
#               (end-of-line)
#               (dotimes (_ count)
#         	(electric-newline-and-maybe-indent))
#               (ryo-modal-mode 0))

#             (defun kakoune-p (count)
#               "Yank COUNT times after the point."
#               (interactive "p")
#               (forward-line)
#               (dotimes (_ count) (save-excursion (yank))))

#             (defun kakoune-P (count)
#               "Yank COUNT times after the point."
#               (interactive "p")
#               (dotimes (_ count) (save-excursion (yank))))

#             (defvar kakoune-last-t-or-f ?f
#               "Using t or f command sets this variable.")

#             (defun kakoune-select-to-char (arg char)
#               "Select up to, and including ARGth occurrence of CHAR.
#         			   Case is ignored if `case-fold-search' is non-nil in the current buffer.
#         			   Goes backward if ARG is negative; error if CHAR not found.
#         			   Ignores CHAR at point."
#               (interactive "p\ncSelect to char: ")
#               (setq kakoune-last-char-selected-to char)
#               (setq kakoune-last-t-or-f ?f)
#               (let ((direction (if (>= arg 0) 1 -1)))
#         	(forward-char direction)
#         	(unwind-protect
#         	    (search-forward (char-to-string char) nil nil arg))
#         	(point)))

#             (defun kakoune-select-up-to-char (arg char)
#               "Select up to, but not including ARGth occurrence of CHAR.
#         			   Case is ignored if `case-fold-search' is non-nil in the current buffer.
#         			   Goes backward if ARG is negative; error if CHAR not found.
#         			   Ignores CHAR at point."
#               (interactive "p\ncSelect up to char: ")
#               (setq kakoune-last-char-selected-to char)
#               (setq kakoune-last-t-or-f ?t)
#               (let ((direction (if (>= arg 0) 1 -1)))
#         	(forward-char direction)
#         	(unwind-protect
#         	    (search-forward (char-to-string char) nil nil arg)
#         	  (backward-char direction))
#         	(point)))
#             (defun kakoune-word-backward ()
#               (interactive)
#               ;; (progn (forward-char)
#               ;;     (forward-word))
#               (subword-backward
#                ))

#             (defun kakoune-word-forward ()
#               (interactive)
#               ;; (progn (forward-char)
#               ;;     (forward-word))
#               (subword-forward
#                ))

#             (defun kakoune-forward-char ()
#               (interactive)
#               (forward-char))

#             (defun kakoune-A (count)
#               "Yank COUNT times after the point."
#               (interactive "p")
#               (end-of-line)
#               (ryo-modal-mode 0))


#           (defun kakoune-downcase ()
#             "Downcase region."
#             (interactive)
#             (if (use-region-p)
#         	(downcase-region (region-beginning) (region-end))
#               (downcase-region (point) (+ 1 (point)))))

#           (defun kakoune-upcase ()
#             "Upcase region."
#             (interactive)
#             (if (use-region-p)
#         	(upcase-region (region-beginning) (region-end))
#               (upcase-region (point) (1+ (point)))))

#           (defun kakoune-replace-char (char)
#           "Replace selection with CHAR."
#           (interactive "cReplace with char: ")
#           (mc/execute-command-for-all-cursors
#            (lambda () (interactive)
#              (if (use-region-p)
#                  (progn (let ((region-size (- (region-end) (region-beginning))))
#         	              (delete-region (region-beginning) (region-end))
#         	              (mc/save-excursion
#         		           (insert-char char region-size t))))
#                (progn (delete-region (point) (1+ (point)))
#         	          (mc/save-excursion
#         	           (insert-char char)))))))

#         (defun kakoune-replace-selection ()
#           "Replace selection with killed text."
#           (interactive)
#           (if (use-region-p)
#               (progn (delete-region (region-beginning) (region-end))
#         	         (yank))
#             (progn (delete-region (point) (1+ (point)))
#         	       (yank))))

#         (defun kakoune-insert-line-below (count)
#           "Insert COUNT empty lines below the current line."
#           (interactive "p")
#           (save-excursion
#             (end-of-line)
#             (open-line count)))

#         (defun kakoune-insert-line-above (count)
#           "Insert COUNT empty lines above the current line."
#           (interactive "p")
#           (save-excursion
#             (end-of-line 0)
#             (open-line count)))

#         (defun kakoune-paste-above (count)
#           "Paste (yank) COUNT times above the current line."
#           (interactive "p")
#           (save-excursion
#             (dotimes (_ count) (end-of-line 0)
#         	     (newline)
#         	     (yank))))


#               '';

#       usePackage = {
#         vterm = {
#           enable = true;
#           command = [ "vterm" ];
#           hook = [ "(vterm-mode . rah-disable-trailing-whitespace-mode)" ];
#           config = ''
#             (setq vterm-kill-buffer-on-exit t
#                   vterm-max-scrollback 10000)
#           '';
#         };

#         orderless = {
#           enable = true;
#           config = ''
#                         (setq completion-styles '(orderless)
#             completion-category-defaults nil
#             	     completion-category-overrides '((file (styles partial-completion)))
#                         )
#                       '';
#         };
#         # subword   = {
#         # enable = true;
#         # command = ["subword-mode"];
#         # config = ''
#         # (defvar my-subword-forward-regexp "[[[:upper:]]*[[:digit:][:lower:]]+\\|[[:upper:]]+\\|[^[:word:][:space:]_\n]+\\|-|(|")

#         # (defvar my-subword-backward-regexp "[[:space:][:word:]_\n][^\n[:space:][:word:]_]+\\|\\(\\W\\|[[:lower:]]\\)[[:upper:]]\\|\\W\\w+")
#         # ;; (defvar my-subword-backward-regexp "[^[:word:][:space:]_\n]+")
#         # (defun my-subword-forward-internal () ""
#         # (interactive)  
#         # (let ((case-fold-search nil))
#         # (re-search-forward my-subword-forward-regexp nil t))
#         # (goto-char (match-end 0)))

#         # (defun my-subword-backward-internal () ""
#         # (interactive)
#         # (let ((case-fold-search nil))
#         # (if (re-search-backward my-subword-backward-regexp nil "don't panic!")
#         # (goto-char (1+ (match-beginning 0))))))

#         # (setq subword-forward-regexp 'my-subword-forward-regexp)
#         # (setq subword-backward-regexp 'my-subword-backward-regexp)
#         # (setq subword-backward-function 'my-subword-backward-internal)
#         # (setq subword-forward-function 'my-subword-forward-internal)
#         # (subword-mode 1)
#         # '';
#         # };
#         recentf = {
#           enable = true;
#           command = [ "recentf-mode" ];
#           config = ''
#                    (setq recentf-save-file (locate-user-emacs-file "recentf")
#                          recentf-max-menu-items 20
#                          recentf-max-saved-items 500
#                          recentf-exclude '("COMMIT_MSG" "COMMIT_EDITMSG"))

#             ;; Save the file list every 10 minutes.
#                     (run-at-time nil (* 10 60) 'recentf-save-list)
#                     (global-set-key "\C-x\ \C-r" 'recentf-open-files)
#                     (recentf-mode 1)
#           '';

#         };
#         expand-region = {
#           enable = true;
#           bind = { "C-+" = "er/expand-region"; };
#         };
#         mini-modeline = {
#           enable = true;
#           command = [ "mini-modeline-mode" ];
#           # after = [ "doom-modeline" ];
#           init  = "(mini-modeline-mode 1)";
#         };

#         envrc = {
#           enable = true;
#           command = [ "envrc-global-mode" ];
#           init = "(envrc-global-mode 1)";
#           # config 
#         };

#         vertico = {
#           enable = true;
#           command = [ "vertico-mode" "vertico-next" ];
#           init = "(vertico-mode)";
#           config = ''
#                       (setq vertico-count 14
#             			       vertico-cycle t)

#             	       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
#             	       (define-key vertico-map (kbd "C-j") #'vertico-next)
#             	       (define-key vertico-map (kbd "C-k") #'vertico-previous)
#                  	   (define-key vertico-map (kbd "M-h") #'backward-kill-word)
#                       '';
#           # bindlocal = {
#           # vertico-map = { 
#           # "c-j" = "vertico-next"; 
#           # "c-k" = "vertico-previous";
#           # "c-f" = "vertico-exit";
#           # "<escape>" = "keyboard-escape-quit";

#           # };
#           # minibuffer-local-map = {
#           # "m-h" = "backward-kill-word";
#           # };
#           # };
#           # extraconfig = ''
#           # :custom
#           # (vertico-cycle t)
#           # '';
#         };

#         vertico-posframe = {
#           enable = true;
#           command = [ "vertico-posframe-mode" ];
#           init = "(vertico-posframe-mode 1)";
#           config = ''
#             (setq vertico-posframe-parameters
#             '((left-fringe . 8)
#             (right-fringe . 8)))
#           '';
#         };

#         undo-tree = {
#           enable = true;
#           defer = 1;
#           command = [ "global-undo-tree-mode" ];
#           config = ''
#             (setq undo-tree-visualizer-relative-timestamps t
#                  undo-tree-visualizer-timestamps t
#                  undo-tree-enable-undo-in-region t)
#                  (global-undo-tree-mode)
#                  (ryo-modal-keys
#                   ("u" undo-tree-undo)
#                   ("U" undo-tree-redo)
#                   ("SPC u" undo-tree-visualize)
#                  )
#                  '';
#         };

#         # savehist for vertico
#         savehist = {
#           enable = true;
#           config = ''
#             (setq history-length 25)
#             (savehist-mode 1)
#           '';
#         };

#         nix-mode = {
#           enable = true;
#           # extraconfig = ''
#           # :mode "\\.nix\\'"
#           # '';
#         };

#         multiple-cursors = {
#           enable = true;
#           bind = {
#             "C-s-c c-s-c" = "mc/edit-lines";
#             "C-c m" = "mc/mark-all-like-this";
#             "C->" = "mc/mark-next-like-this";
#             "C-<" = "mc/mark-previous-like-this";
#           };
#         };

#         consult = {
#           enable = true;
#           demand = true;
#           # bind = {
#           # "C-s" = "consult-line";
#           # "c-x b" = "consult-buffer";
#           # "m-g m-g" = "consult-goto-line";
#           # "m-g g" = "consult-goto-line";
#           # "m-s f" = "consult-find";
#           # "m-s r" = "consult-ripgrep";
#           # "<escape>" = "keyboard-escape-quit";
#           # };
#           # bindlocal = { minibuffer-local-map = { c-r = "consult-history"; };
#           # };

#         };

#         ryo-modal = {
#           enable = true;
#           command = [ "ryo-modal-mode" ];
#           config = ''
#                   (ryo-modal-keys
#                    ;; Basic keybindings
#                    (:mc-all t)
#                    ("<RET>" next-line)
#                    (";" kakoune-deactivate-mark)
#                    ;; ("0" beginning-of-line )
#                    ("<backspace>" backward-char :first '(kakoune-deactivate-mark) )
#                    ("%" mark-whole-buffer)
#                    ("," ryo-modal-repeat)
#                    ("`" kakoune-downcase)
#                    ("~" kakoune-upcase)
#                    ("/" consult-line)
#                    ("C-=" text-scale-increase)   
#                    ("C--" text-scale-decrease)
#                    ("a" ryo-after :first '(kakoune-deactivate-mark))
#                    ("b" kakoune-word-backward :first '(kakoune-set-mark-here))
#                    ("c" kakoune-d :exit t)
#                    ;; ("c" comment-line)
#                    ("d" kakoune-d)
#                    ("f" kakoune-select-to-char :first '(kakoune-set-mark-here ))
#                    ;;("f" evil-snipe-f :first '(kakoune-set-mark-here))
#                    ("h" backward-char :first '(kakoune-deactivate-mark))
#                    ("i" kakoune-insert-mode :first '(kakoune-deactivate-mark))
#                    ("j" next-line :first '(kakoune-deactivate-mark))
#                    ("k" previous-line :first '(kakoune-deactivate-mark))
#                    ("l" kakoune-forward-char :first '(kakoune-deactivate-mark))
#                    ("o" kakoune-o)
#                    ("p" kakoune-p)
#                    ("r" kakoune-replace-char)
#                    ("R" kakoune-replace-selection)
#                    ;;("q" ryo-modal-mode)
#                    ;; ("s" save-buffer)
#                    ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here ))
#              ;;      ("u" undo-tree-undo)
#                    ("w" kakoune-word-forward :first '(kakoune-set-mark-here))
#                    ("x" kakoune-x)
#                    ("y" kill-ring-save)
#                    ("A" kakoune-A)
#                    ("B" backward-word :first '(kakoune-set-mark-if-inactive))
#                    ("C" kill-line :exit t)
#                    ("F" kakoune-select-to-char :first '(kakoune-set-mark-if-inactive ))
#                    ;;("F" evil-snipe-F :first '(kakoune-set-mark-here ))
#                    ("H" backward-char :first '(kakoune-set-mark-if-inactive))
#                    ("J" next-line :first '(kakoune-set-mark-if-inactive))
#                    ("K" previous-line :first '(kakoune-set-mark-if-inactive))
#                    ("L" forward-char :first '(kakoune-set-mark-if-inactive))
#                    ("M-w" forward-symbol :first '(kakoune-set-mark-here))
#                    ("O" kakoune-O)
#                    ("P" kakoune-P)
#                    ("T" kakoune-T)
#                ;;    ("U" undo-tree-redo)
#                    ("W" forward-word :first '(kakoune-set-mark-if-inactive))
#                    ("X" kakoune-X)
#                    ("ZZ" kill-this-buffer)
#                    ("M-;" kakoune-exchange)
#                    )

#                   (ryo-modal-keys
#                    ;; first argument to ryo-modal-keys may be a list of keywords.
#                    ;; These keywords will be applied to all keybindings.
#                    (:norepeat t)
#                    ;;("0" "M-0")
#                    ("1" "M-1")
#                    ("2" "M-2")
#                    ("3" "M-3")
#                    ("4" "M-4")
#                    ("5" "M-5")
#                    ("6" "M-6")
#                    ("7" "M-7")
#                    ("8" "M-8")
#                    ("7" "M-7")
#                    ("9" "M-9"))

#                 (ryo-modal-key
#                  "g" '(("h" beginning-of-line)
#             	   ("j" end-of-buffer)
#             	   ("g" kakoune-gg)
#             	   ("l" end-of-line)
#             	   ("k" beginning-of-buffer)))
#                 ;; (ryo

#              (ryo-modal-key
#                  "SPC" '(
#             	     ;;("a" consult-line)
#             	     ("b b" consult-buffer)
#             	     ("b [" previous-buffer)
#             	     ("b ]" next-buffer)
#             	     ("f f" find-file)
#             	     ("f s" save-buffer)
#             	     ;;("g g" magit-status)
#             	     ("h f" describe-function)
#             	     ("h k" describe-key)
#             	     ("h t" consult-theme)
#             	     ;; ("o t" vterm-toggle)
#             	     ;; ("j" next-buffer)
#             	     ;; ("k" previous-buffer)
#             	     ("p p" project-find-file)
#             	     ("p P" project-switch-project)
#             	     ("s s" consult-line);;save-buffer)
#             	;;     ("r r" reload)
#             	     ("w C-o" delete-other-windows)
#             	     ("w d" delete-window)
#             	     ("w q" kill-buffer-and-window)
#             	     ("w s" split-window-below)
#             	     ("w v" split-window-right)
#             	     ("w w" other-window)
#             	     ("SPC" execute-extended-command)
#             	     ("<tab> n" tab-bar-new-tab-to )
#             	     ("<tab> d" tab-bar-close-tab)
#             	     ("<tab> b" switch-to-buffer-other-tab)
#             	     ("<tab> f" find-file-other-tab)
#             	     ("<tab> a" tab-bar-close-other-tabs )
#             	     ;; ("<tab> F" dired-other-tab)
#              ))
#           '';

#         };
#         haskell-mode = {
#           enable = true;
#           config = ''
#             (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
#             (setq  haskell-interactive-popup-errors nil)
#           '';

#         };
#         haskell-doc = { enable = true; };
#         magit = { enable = true; };
#         gcmh = {
#           enable = true;
#           defer = 1;
#           command = [ "gcmh-mode" ];
#           config = ''
#             (setq gcmh-idle-delay 'auto)
#             (gcmh-mode)
#           '';
#         };

#         dired = {
#           enable = true;
#           config = ''
#                           			   (put 'dired-find-alternate-file 'disabled nil)
#             		(define-key dired-mode-map (kbd "/") 'dired-goto-file)
#             			 (define-key dired-mode-map (kbd "j") 'dired-next-line)
#             			 (define-key dired-mode-map (kbd "k") 'dired-previous-line)
#             			 (define-key dired-mode-map (kbd "h") 'dired-up-directory)
#             			 (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
#             			 ;;(evil-define-key 'normal dired-mode-map
#             		;;		(kbd "M-RET") 'dired-display-file
#             		;;		(kbd "h") 'dired-up-directory
#             		;;		(kbd "l") 'dired-find-alternate-file ); use dired-find-file instead of dired-open.
#                   ;; (defun dired-config ()
#             	;; "my dired config"
#             	;; (dired-hide-details-mode 1)
#                 ;;     )
#                  (add-hook 'dired-mode-hook 'dired-hide-details-mode)
#                  (add-hook 'dired-mode-hook 'dired-omit-mode)

#             			  (setq dired-listing-switches "-ahl -agho --group-directories-first"
#             				 dired-omit-files "^\\.[^.].*"
#             				 dired-omit-verbose nil
#             				 dired-hide-details-hide-symlink-targets nil
#             				 delete-by-moving-to-trash t)         
#           '';
#         };

#       };

#       postlude = ''
#         	(global-set-key (kbd "<escape>")  'kakoune-normal-mode)
#          (add-hook 'prog-mode-hook #'kakoune-normal-mode)
#             (add-hook 'org-mode-hook #'kakoune-normal-mode)
#             (add-hook 'fundamental-mode-hook #'kakoune-normal-mode)
#             ;;		      (add-hook 'dired-mode-hook #'kakoune-normal-mode)
#             (add-hook 'latex-mode-hook #'kakoune-normal-mode)

#       '';
#       # overrides =
#       # overrides = self: super: rec {
#       # haskell-mode = self.melpaPackages.haskell-mode;
#     };
