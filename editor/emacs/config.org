* Personal info
#+begin_src emacs-lisp
(setq user-full-name "Vamshi krishna")
(setq user-mail-address "vamshi5070k@gmail.com")
#+end_src
* Garbage collection
#+begin_src emacs-lisp
(setq gc-cons-percentage 0.6)
#+end_src
* Basic ui
  #+begin_src emacs-lisp
  (setq inhibit-startup-message t)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1)
  (fset 'yes-or-no-p 'y-or-n-p)
  #+end_src

* TEXT SIZE MANIPULATION
#+begin_src emacs-lisp
;; zoom in/out like we do everywhere else.
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
#+end_src
* Private Config
#+begin_src emacs-lisp
   (defun privateConfig ()
		   (interactive )
		    (find-file (expand-file-name "~/aios/editor/emacs/config.org")))
#+end_src

* Insert text
#+begin_src emacs-lisp
  (defun func ()
    (interactive)
    (insert " = "))

#+end_src
* Revert
#+begin_src emacs-lisp
;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

#+end_src
* Theme
  #+begin_src emacs-lisp
     (load-theme 'modus-vivendi t)
  #+end_src
* Transparency
  #+begin_src emacs-lisp
	;; (set-frame-parameter (selected-frame) 'alpha '(92 . 90))
    ;; (add-to-list 'default-frame-alist '(alpha . (92 . 90)))
  #+end_src
* Line number mode
  #+begin_src emacs-lisp
	(column-number-mode t)
	(global-display-line-numbers-mode t)
	(setq display-line-numbers-type 'relative)
	(dolist (mode '(
			;;org-mode-hook
			term-mode-hook
			vterm-mode-hook
			shell-mode-hook
			dired-mode-hook
			eshell-mode-hook))
	  (add-hook mode (lambda () (display-line-numbers-mode 0))))
  #+end_src
* Reload
  #+begin_src emacs-lisp
    (defun reload ()
		   (interactive )
		    (load-file (expand-file-name "~/.emacs.d/init.el")))
#+end_src
* Keep folders clean
#+begin_src emacs-lisp
(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)
#+end_src
* Font
#+begin_src emacs-lisp   
  (set-face-attribute 'default nil
		      :font "Mononoki Nerd Font Mono"
				     :height 170
				   :weight 'regular)

  ;;   (set-face-attribute 'variable-pitch nil
  ;;			   :font "SF Mono"
  ;;			 :height 160
  ;;			  :weight 'regular)
   ;;  (set-face-attribute 'fixed-pitch nil
  ;;			 :font "SF Mono"
  ;;			 :height 200
  ;;			 :weight 'regular)
      ;; Uncomment the following line if line spacing needs adjusting.
      (setq-default line-spacing 0)

       ;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
    ;;   (add-to-list 'default-frame-alist '(font . "SF Mono-18"))

     (set-face-attribute 'font-lock-comment-face nil
       :slant 'italic)
     (set-face-attribute 'font-lock-keyword-face nil
			 :slant 'italic)

#+end_src

* Straight
** Straight
   #+begin_src emacs-lisp
(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)
#+end_src
** Use-package
#+begin_src emacs-lisp
(straight-use-package 'use-package)
#+end_src

* Vertico
   #+begin_src emacs-lisp
					;;(setup (:pkg vertico)
					;; :straight '(vertico :host github
					;;                     :repo "minad/vertico"
					;;                     :branch "main")
				       ;; (vertico-mode))


	       (defun rational-completion/minibuffer-backward-kill (arg)
		 "When minibuffer is completing a file name delete up to parent
	       folder, otherwise delete a word"
		 (interactive "p")
		 (if minibuffer-completing-file-name
		     ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
		     (if (string-match-p "/." (minibuffer-contents))
			 (zap-up-to-char (- arg) ?/)
		       (delete-minibuffer-contents))
		   (backward-kill-word arg)))

	       (use-package vertico
				     :straight t
				     :init
				     (vertico-mode))
			 (setq vertico-count 14
			       vertico-cycle t)

	       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
	       (define-key vertico-map (kbd "C-j") #'vertico-next)
	       (define-key vertico-map (kbd "C-k") #'vertico-previous)
     	       (define-key vertico-map (kbd "M-h") #')


     ;;    :bind (("C-j" vertico-next)
			      ;;           ("C-k" vertico-previous)
			      ;;           ("C-f" vertico-exit)))
#+end_src

* Orderless
     #+begin_src emacs-lisp
       (use-package orderless
       :straight t
       :init
       ;; Configure a custom style dispatcher (see the Consult wiki)
       ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
       ;;       orderless-component-separator #'orderless-escapable-split-on-space)
       (setq completion-styles '(orderless)
	     completion-category-defaults nil
	     completion-category-overrides '((file (styles partial-completion)))))
#+end_src
* Savehist
  #+begin_src emacs-lisp
      ;; Persist history over Emacs restarts. Vertico sorts by history position.
    (use-package savehist
        :init
      (savehist-mode))
#+end_src

* Magit
#+begin_src emacs-lisp
(use-package magit
      :straight t)
#+end_src
* Nix mode
  #+begin_src emacs-lisp
(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")
#+end_src
* Hlint
  #+begin_src 

;;; hs-lint.el --- minor mode for HLint code checking

;; Copyright 2009 (C) Alex Ott
;;
;; Author: Alex Ott <alexott@gmail.com>
;; Keywords: haskell, lint, HLint
;; Requirements:
;; Status: distributed under terms of GPL2 or above

;; Typical message from HLint looks like:
;;
;; /Users/ott/projects/lang-exp/haskell/test.hs:52:1: Eta reduce
;; Found:
;;   count1 p l = length (filter p l)
;; Perhaps:
;;   count1 p = length . filter p


(require 'compile)

(defgroup hs-lint nil
  "Run HLint as inferior of Emacs, parse error messages."
  :group 'tools
  :group 'haskell)

(defcustom hs-lint-command "hlint"
  "The default hs-lint command for \\[hlint]."
  :type 'string
  :group 'hs-lint)

(defcustom hs-lint-save-files t
  "Save modified files when run HLint or no (ask user)"
  :type 'boolean
  :group 'hs-lint)

(defcustom hs-lint-replace-with-suggestions nil
  "Replace user's code with suggested replacements"
  :type 'boolean
  :group 'hs-lint)

(defcustom hs-lint-replace-without-ask nil
  "Replace user's code with suggested replacements automatically"
  :type 'boolean
  :group 'hs-lint)

(defun hs-lint-process-setup ()
  "Setup compilation variables and buffer for `hlint'."
  (run-hooks 'hs-lint-setup-hook))

;; regex for replace suggestions
;;
;; ^\(.*?\):\([0-9]+\):\([0-9]+\): .*
;; Found:
;; \s +\(.*\)
;; Perhaps:
;; \s +\(.*\)

(defvar hs-lint-regex
  "^\\(.*?\\):\\([0-9]+\\):\\([0-9]+\\): .*[\n\C-m]Found:[\n\C-m]\\s +\\(.*\\)[\n\C-m]Perhaps:[\n\C-m]\\s +\\(.*\\)[\n\C-m]"
  "Regex for HLint messages")

(defun make-short-string (str maxlen)
  (if (< (length str) maxlen)
      str
    (concat (substring str 0 (- maxlen 3)) "...")))

(defun hs-lint-replace-suggestions ()
  "Perform actual replacement of suggestions"
  (goto-char (point-min))
  (while (re-search-forward hs-lint-regex nil t)
    (let* ((fname (match-string 1))
          (fline (string-to-number (match-string 2)))
          (old-code (match-string 4))
          (new-code (match-string 5))
          (msg (concat "Replace '" (make-short-string old-code 30)
                       "' with '" (make-short-string new-code 30) "'"))
          (bline 0)
          (eline 0)
          (spos 0)
          (new-old-code ""))
      (save-excursion
        (switch-to-buffer (get-file-buffer fname))
        (goto-line fline)
        (beginning-of-line)
        (setf bline (point))
        (when (or hs-lint-replace-without-ask
                  (yes-or-no-p msg))
          (end-of-line)
          (setf eline (point))
          (beginning-of-line)
          (setf old-code (regexp-quote old-code))
          (while (string-match "\\\\ " old-code spos)
            (setf new-old-code (concat new-old-code
                                 (substring old-code spos (match-beginning 0))
                                 "\\ *"))
            (setf spos (match-end 0)))
          (setf new-old-code (concat new-old-code (substring old-code spos)))
          (remove-text-properties bline eline '(composition nil))
          (when (re-search-forward new-old-code eline t)
            (replace-match new-code nil t)))))))

(defun hs-lint-finish-hook (buf msg)
  "Function, that is executed at the end of HLint execution"
  (if hs-lint-replace-with-suggestions
      (hs-lint-replace-suggestions)
      (next-error 1 t)))

(define-compilation-mode hs-lint-mode "HLint"
  "Mode for check Haskell source code."
  (set (make-local-variable 'compilation-process-setup-function)
       'hs-lint-process-setup)
  (set (make-local-variable 'compilation-disable-input) t)
  (set (make-local-variable 'compilation-scroll-output) nil)
  (set (make-local-variable 'compilation-finish-functions)
       (list 'hs-lint-finish-hook))
  )

(defun hs-lint ()
  "Run HLint for current buffer with haskell source"
  (interactive)
  (save-some-buffers hs-lint-save-files)
  (compilation-start (concat hs-lint-command " \"" buffer-file-name "\"")
                     'hs-lint-mode))

(provide 'hs-lint)
;;; hs-lint.el ends here
  #+end_src
* Haskell mode
  #+begin_src emacs-lisp
		(use-package haskell-mode
		  :straight t)
		;;        :config
		;;;;        (load "haskell-mode-autoloads"))
		(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
		(setq  haskell-interactive-popup-errors nil)
     
    (add-to-list 'load-path (expand-file-name "~/aios/editor/emacs"))
    
    ;; (require 'awesome-tab)
    
    ;; (awesome-tab-mode t)	;; (use-package hs-lint
	      ;; :load-path "/home/vamshi/aios/editor/emacs/hlint.el"
	     ;; :config
	       (require 'hs-lint)
	(defun my-haskell-mode-hook ()
	    (local-set-key "\C-cl" 'hs-lint))
	(add-hook 'haskell-mode-hook 'my-haskell-mode-hook)
#+end_src
* Envrc
  #+begin_src emacs-lisp
    (use-package envrc
      :straight t)
(envrc-global-mode)
  #+end_src
* GENERAL
#+begin_src emacs-lisp

  (use-package general
    :straight t
    :config
    (general-evil-setup t)

    (general-create-definer vc/leader-key-def
      :keymaps '(normal insert visual emacs)
      :prefix "SPC"
      :global-prefix "M-SPC")

    (general-create-definer vc/ctrl-x-keys
      :prefix "C-x")

    (general-create-definer vc/ctrl-c-keys
      :prefix "C-c"))
#+end_src

* CONSULT
#+begin_src emacs-lisp
  (use-package consult
    :straight t)
#+end_src
* Files
#+begin_src emacs-lisp
       (vc/leader-key-def
	  "f"  '(:ignore t :which-key "files" )
	  ;"t w" 'whitespace-mode
       "f p" '(privateConfig :which-key "edit emacs config")
       "r" '(reload  :which-key "reload emacs config")
  ;;       "." '(find-file :which-key "find-file")

	 "f f" '(find-file :which-key "find-file")
	  "f s" '(save-buffer :which-key "save the world"))
#+end_src
* Company
#+begin_src emacs-lisp
    (use-package company
     :straight t)
   (global-company-mode)

  ;; (use-package company
    ;; :straight t
    ;; :ensure t
    ;; :config
    ;; (add-to-list 'company-backends 'company-capf))
    ;; (global-company-mode))
   ;;(setq ac-ignore-case nil)
#+end_src
* Search
  #+begin_src emacs-lisp

       (vc/leader-key-def
	  "s"  '(:ignore t :which-key "search" )
	  ;"t w" 'whitespace-mode
       "s s" '(consult-line :which-key "search line"))
 ;;      "r" '(reload  :which-key "reload emacs config")
  ;;       "." '(find-file :which-key "find-file")

	 ;;"f f" '(find-file :which-key "find-file")
	 ;; "f s" '(save-buffer :which-key "save the world"))
  #+end_src
* Org
** Tempo
#+begin_src emacs-lisp
(use-package org-tempo
  :ensure nil)
#+end_src
** Indent
#+begin_src emacs-lisp
;;(add-hook 'org-mode-hook 'org-indent-mode)
#+end_src
** Ellipsis
   #+begin_src emacs-lisp
(setq org-ellipsis "⤵")
   #+end_src
** Superstar
   #+begin_src emacs-lisp
     ;;(use-package org-superstar
      ;; :straight t)
;;(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
#+end_src

** Backtab
   #+begin_src emacs-lisp
    (add-hook 'org-mode-hook #'org-shifttab)
   #+end_src
* Which key
  Used for suggesstions 
#+begin_src emacs-lisp
    (use-package which-key
          :straight t
          :defer 0
          :diminish which-key-mode
          :config
          (which-key-mode)
          (setq which-key-idle-delay 1.0))
#+end_src

* Windows
#+begin_src emacs-lisp
  
  ;;(vc/leader-key-def
  ;;    "w" '(:ignore t :which-key "windows")
  ;;    "w k" '(delete-window :which-key "kill windows")
  ;;    "w C-o" '(delete-other-windows :which-key "kill all but this")
  ;;    "w w" '(other-window :which-key "next window")
  ;;    "w v" '(split-window-right :which-key "vertical split")
  ;;    "w h" '(split-window-below :which-key "horizontal split"))
#+end_src
* Vterm toggle
#+begin_src emacs-lisp
  (use-package vterm-toggle
      :straight t)
					  ; (
   ;; vc/leader-key-def
      ;; "o" '(:ignore t :which-key "open")
      ;; "o t" '(vterm-toggle :which-key "vterm-toggle"))

#+end_src

* Evil snipe
  #+begin_src emacs-lisp
 ;;  (use-package evil-snipe
   ;;   :straight t)
  #+end_src
* Escape
  #+begin_src emacs-lisp
	(global-set-key (kbd "<escape>")  'kakoune-normal-mode)
    ;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)
  #+end_src
* Highlight line
  #+begin_src emacs-lisp
(global-hl-line-mode 1)
  #+end_src
* Recentf
  #+begin_src emacs-lisp
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)
  #+end_src
  
* Electric pair
  #+begin_src emacs-lisp
(electric-pair-mode 1)
  #+end_src
* Subword
#+begin_src emacs-lisp
  (defvar my-subword-forward-regexp "\\W?[[:upper:]]*[[:digit:][:lower:]]+\\|\\W?[[:upper:]]+\\|[^[:word:][:space:]_\n]+\\|-")

  (defvar my-subword-backward-regexp "[[:space:][:word:]_\n][^\n[:space:][:word:]_]+\\|\\(\\W\\|[[:lower:]]\\)[[:upper:]]\\|\\W\\w+")

  (defun my-subword-forward-internal () ""
	 (interactive)
	 (let ((case-fold-search nil))
	   (re-search-forward my-subword-forward-regexp nil t))
	 (goto-char (match-end 0)))

  (defun my-subword-backward-internal () ""
	 (interactive)
	 (let ((case-fold-search nil))
	   (if (re-search-backward my-subword-backward-regexp nil "don't panic!")
	       (goto-char (1+ (match-beginning 0))))))

  (setq subword-forward-regexp 'my-subword-forward-regexp)
  (setq subword-backward-regexp 'my-subword-backward-regexp)
  (setq subword-backward-function 'my-subword-backward-internal)
  (setq subword-forward-function 'my-subword-forward-internal)
(subword-mode 1)

#+end_src
* Kakoune
#+begin_src emacs-lisp
				   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (setq-default cursor-type 'bar) 
  (setq ryo-modal-cursor-color "lightblue")
  ;;(global-set-key (kbd "<escape>")  'ryo-modal-mode)
  (defun kakoune-a (count)
    "Select COUNT lines from the current line.
				 Note that kakoune's x does behaves exactly like this,
				 and I like this behavior better."
    (interactive "p")
    (forward-char count)
    (ryo-modal-mode 0))

  (defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
	 (interactive)
	 (unless (use-region-p) (set-mark (point))))

  (defun kakoune-deactivate-mark ()
    "Deactivate the mark.
				 Deactivate the mark unless mark-region-mode is active."
    (interactive)
    ;;(unless rectangle-mark-mode (deactivate-mark)))
    (deactivate-mark))

  (defun kakoune-O (count)
    "Open COUNT lines above the cursor and go into insert mode."
    (interactive "p")
    (beginning-of-line)
    (dotimes (_ count)
      (newline)
      (forward-line -1)))

  (defun kakoune-x (count)
    "Select COUNT lines from the current line.
				 Note that kakoune's x does behaves exactly like this,
				 and I like this behavior better."
    (interactive "p")
    (beginning-of-line count)
    (set-mark (point))
    (end-of-line count)
    (forward-char count))

  (defun kakoune-d (count)
    "Kill selected text or COUNT chars."
    (interactive "p")
    (if (use-region-p)
	(kill-region (region-beginning) (region-end))
      (delete-char count t)))

  (defun kakoune-insert-mode () "Return to insert mode."
	 (interactive)
	 (ryo-modal-mode 0))

  (defun kakoune-normal-mode () "Return to insert mode."
	 (interactive)
	 (ryo-modal-mode 1))

  (defun ryo-after () "Enter normal mode"
	 (interactive)
	 (forward-char)
	 (ryo-modal-mode 0))

  (defun kakoune-X (count)
    "Extend COUNT lines from the current line."
    (interactive "p")
    (beginning-of-line)
    (unless (use-region-p) (set-mark (point)))
    (forward-line count))

  (defun kakoune-T (count)
    "Extend COUNT lines from the current line."
    (interactive "p")
    (beginning-of-line)
    (unless (use-region-p) (set-mark (point)))
    (previous-line count))

  (defun kakoune-gg (count)
    "Go to the beginning of the buffer or the COUNTth line."
    (interactive "p")
    (goto-char (point-min))
    (when count (forward-line (1- count))))

  (defun kakoune-o (count)
    "Open COUNT lines under the cursor and go into insert mode."
    (interactive "p")
    (end-of-line)
    (dotimes (_ count)
      (electric-newline-and-maybe-indent))
    (ryo-modal-mode 0))

  (defun kakoune-set-mark-here () "Set the mark at the location of the point."
	 (interactive) (set-mark (point)))

  (defun kakoune-p (count)
    "Yank COUNT times after the point."
    (interactive "p")
    (forward-line)
    (dotimes (_ count) (save-excursion (yank))))

  (defun kakoune-P (count)
    "Yank COUNT times after the point."
    (interactive "p")
    (dotimes (_ count) (save-excursion (yank))))

  (defvar kakoune-last-t-or-f ?f
    "Using t or f command sets this variable.")

  (defun kakoune-select-to-char (arg char)
    "Select up to, and including ARGth occurrence of CHAR.
			 Case is ignored if `case-fold-search' is non-nil in the current buffer.
			 Goes backward if ARG is negative; error if CHAR not found.
			 Ignores CHAR at point."
    (interactive "p\ncSelect to char: ")
    (setq kakoune-last-char-selected-to char)
    (setq kakoune-last-t-or-f ?f)
    (let ((direction (if (>= arg 0) 1 -1)))
      (forward-char direction)
      (unwind-protect
	  (search-forward (char-to-string char) nil nil arg))
      (point)))

  (defun kakoune-select-up-to-char (arg char)
    "Select up to, but not including ARGth occurrence of CHAR.
			 Case is ignored if `case-fold-search' is non-nil in the current buffer.
			 Goes backward if ARG is negative; error if CHAR not found.
			 Ignores CHAR at point."
    (interactive "p\ncSelect up to char: ")
    (setq kakoune-last-char-selected-to char)
    (setq kakoune-last-t-or-f ?t)
    (let ((direction (if (>= arg 0) 1 -1)))
      (forward-char direction)
      (unwind-protect
	  (search-forward (char-to-string char) nil nil arg)
	(backward-char direction))
      (point)))

  (defun kakoune-word ()
    (interactive)
    ;; (progn (forward-char)
    ;;     (forward-word))
    (subword-forward
     ))

  (defun kakoune-forward-char ()
    (interactive)
    (forward-char))

  (defun kakoune-A (count)
    "Yank COUNT times after the point."
    (interactive "p")
    (end-of-line)
    (ryo-modal-mode 0))

  (use-package expand-region
    :straight t
    :bind ("C-+" . er/expand-region))

  (use-package ryo-modal
    :straight t
    :commands ryo-modal-mode
    :bind ("C-a" . ryo-modal-mode)
    :config
    (ryo-modal-keys
     ("<RET>" next-line)
     (";" kakoune-deactivate-mark)
     ;; ("0" beginning-of-line )
     ("<backspace>" backward-char :first '(kakoune-deactivate-mark) )
     ("%" mark-whole-buffer)
     ("," ryo-modal-repeat)
     ("/" consult-line)
     ("C-=" text-scale-increase)   
     ("C--" text-scale-decrease)
     ("a" ryo-after)
     ("b" backward-word :first '(kakoune-set-mark-here))
     ("c" comment-line)
     ("d" kakoune-d)
     ("f" kakoune-select-to-char :first '(kakoune-set-mark-here ))
     ;;("f" evil-snipe-f :first '(kakoune-set-mark-here))
     ("h" backward-char :first '(kakoune-set-mark-here))
     ("i" kakoune-insert-mode)
     ("j" next-line :first '(kakoune-deactivate-mark))
     ("k" previous-line :first '(kakoune-deactivate-mark))
     ("l" kakoune-forward-char :first '(kakoune-deactivate-mark))
     ("o" kakoune-o)
     ("p" kakoune-p)
     ;;("q" ryo-modal-mode)
     ;; ("s" save-buffer)
     ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here ))
     ("w" kakoune-word :first '(kakoune-set-mark-here))
     ("x" kakoune-x)
     ("y" kill-ring-save)
     ("A" kakoune-A)
     ("B" backward-word :first '(kakoune-set-mark-if-inactive))
     ("F" kakoune-select-to-char :first '(kakoune-set-mark-if-inactive ))
     ;;("F" evil-snipe-F :first '(kakoune-set-mark-here ))
     ("H" backward-char :first '(kakoune-set-mark-if-inactive))
     ("J" next-line :first '(kakoune-set-mark-if-inactive))
     ("K" previous-line :first '(kakoune-set-mark-if-inactive))
     ("L" forward-char :first '(kakoune-set-mark-if-inactive))
     ("M-w" forward-symbol :first '(kakoune-set-mark-here))
     ("O" kakoune-O)
     ("P" kakoune-P)
     ("T" kakoune-T)
     ("W" forward-word :first '(kakoune-set-mark-if-inactive))
     ("X" kakoune-X)
     ("ZZ" kill-this-buffer)
     )

    (ryo-modal-keys
     ;; First argument to ryo-modal-keys may be a list of keywords.
     ;; These keywords will be applied to all keybindings.
     (:norepeat t)
     ;;("0" "M-0")
     ("1" "M-1")
     ("2" "M-2")
     ("3" "M-3")
     ("4" "M-4")
     ("5" "M-5")
     ("6" "M-6")
     ("7" "M-7")
     ("8" "M-8")
     ("7" "M-7")
     ("9" "M-9")))


  ;; (ryo-modal-key
  ;; 	 "M" '( "x" 

  ;; general

  ;;  (ryo-modal-key
  ;;   "C" '(("s" save-buffer)))
  (ryo-modal-key
   "SPC" '(
	   ;;("a" consult-line)
	   ("b b" consult-buffer)
	   ("b [" previous-buffer)
	   ("b ]" next-buffer)
	   ("f f" find-file)
	   ("f s" save-buffer)
	   ;;("g g" magit-status)
	   ("h f" describe-function)
	   ("h k" describe-key)
	   ("h t" consult-theme)
	   ;; ("o t" vterm-toggle)
	   ;; ("j" next-buffer)
	   ;; ("k" previous-buffer)
	   ("p p" project-find-file)
	   ("p P" project-switch-project)
	   ("s s" consult-line);;save-buffer)
	   ("r r" reload)
	   ("w C-o" delete-other-windows)
	   ("w d" delete-window)
	   ("w q" kill-buffer-and-window)
	   ("w s" split-window-below)
	   ("w v" split-window-right)
	   ("w w" other-window)
	   ("SPC" execute-extended-command)
	   ("<tab> n" tab-bar-new-tab-to )
	   ("<tab> d" tab-bar-close-tab)
	   ("<tab> b" switch-to-buffer-other-tab)
	   ("<tab> f" find-file-other-tab)
	   ("<tab> a" tab-bar-close-other-tabs )
	   ;; ("<tab> F" dired-other-tab)
	   ))

  ;; (ryo-modal-key "h" '(("r" haskell-process-reload)))

  (ryo-modal-key
   "g" '(("h" beginning-of-line)
	 ("j" end-of-buffer)
	 ("g" kakoune-gg)
	 ("l" end-of-line)
	 ("k" beginning-of-buffer)))

  (defun haskellCode ()
    (interactive )
    (ryo-modal-key "SPC" '(
			   ("c r" haskell-process-reload)
			   ("c l" haskell-process-load-file)
			   ("c c" haskell-interactive-switch)
			   )
		   ))
  (defun haskellCodeRepl ()
    (interactive )
    (ryo-modal-key "SPC" '(
			   ;;("c r" haskell-process-reload)
			   ;;("c l" haskell-process-load-file)
			   ("c c" haskell-interactive-switch-back)
			   )))

  ;; ("h" dired-up-directory )
  ;; ("l" dired-find-alternate-file ))
  (add-hook 'haskell-mode-hook #'haskellCode)
  (add-hook 'haskell-interactive-mode-hook #'haskellCodeRepl)
  ;; (global-set-key (kbd "<escape>") 'kakoune-normal-mode)
  (add-hook 'prog-mode-hook #'kakoune-normal-mode)
  (add-hook 'org-mode-hook #'kakoune-normal-mode)
  (add-hook 'fundamental-mode-hook #'kakoune-normal-mode)
  ;;		      (add-hook 'dired-mode-hook #'kakoune-normal-mode)
  (add-hook 'latex-mode-hook #'kakoune-normal-mode)

  ;; (add-hook 'interactive-haskell-mode-hook 'ac-haskell-process-setup)
  (add-hook 'haskell-interactive-mode-hook #'kakoune-normal-mode)
  (push '((nil . "ryo:.*:") . (nil . "")) which-key-replacement-alist)
  (ryo-modal-mode 1)

#+end_src
* Dired
   #+begin_src emacs-lisp

			 ;; (defun cursorToBar () "Set the mark at the location of the point."
				;; (interactive)	
		    ;; (ryo-modal-keys
		       ;; ("h" 'dired-up-directory ))
     ;; )

       ;; (add-hook 'dired-mode-hook #'cursorToBar)

			(put 'dired-find-alternate-file 'disabled nil)

     (define-key dired-mode-map (kbd "/") 'dired-goto-file)
	      (define-key dired-mode-map (kbd "j") 'dired-next-line)
	      (define-key dired-mode-map (kbd "k") 'dired-previous-line)
	      (define-key dired-mode-map (kbd "h") 'dired-up-directory)
	      (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
	      ;;(evil-define-key 'normal dired-mode-map
     ;;		(kbd "M-RET") 'dired-display-file
     ;;		(kbd "h") 'dired-up-directory
     ;;		(kbd "l") 'dired-find-alternate-file ); use dired-find-file instead of dired-open.
	       (setq dired-listing-switches "-agho --group-directories-first"
		      dired-omit-files "^\\.[^.].*"
		      dired-omit-verbose nil
		      dired-hide-details-hide-symlink-targets nil
		      delete-by-moving-to-trash t)
;;  (ryo-modal-major-mode-keys
;; 'dired-mode-hook
;; ("h" dired-up-directory )
;; ("l" dired-find-alternate-file ))
 #+end_src
* TAB BAR
  #+begin_src emacs-lisp
        (setq tab-bar-new-tab-choice "*scratch*")     
               (setq tab-bar-show nil)
  #+end_src
* Undo tree
#+begin_src emacs-lisp
  (use-package undo-tree
  :straight t
  :config
  (global-undo-tree-mode)
  :ryo
  ("u" undo-tree-undo)
  ("U" undo-tree-redo)
  ("SPC u" undo-tree-visualize)
  :bind (:map undo-tree-visualizer-mode-map
              ("h" . undo-tree-visualize-switch-branch-left)
              ("j" . undo-tree-visualize-redo)
              ("k" . undo-tree-visualize-undo)
              ("l" . undo-tree-visualize-switch-branch-right)))
#+end_src

* Ws butler
  #+begin_src emacs-lisp
	(use-package ws-butler
	  :straight t)
(add-hook 'prog-mode-hook #'ws-butler-mode)

  #+end_src
* EMMS
#+begin_src emacs-lisp
  (use-package emms
    :straight t)
(emms-all)
(emms-default-players)
;;(emms-mode-line 1)
(emms-playing-time 1)
(setq emms-source-file-default-directory "~/aios/music/"
      emms-playlist-buffer-name "*Music*"
      emms-info-asynchronously t
      emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)

#+end_src

* Mode line
  #+begin_src emacs-lisp


		    ;; (setq-default header-line-format mode-line-format)
		  (straight-use-package 'hide-mode-line)

;;      (use-package doom-modeline
;;	:straight t)
;;(doom-modeline-mode 1)

    (setq mode-line-format '("%e" 
			     mode-line-position
			     mode-line-front-space
			     mode-line-buffer-identification
			     mode-line-modified "  "
			     "                        "
			    mode-line-buffer-info
		;; mode-line-buffer-identification
			 "                "
			     vc-mode
			     mode-line-modes
			      ) ) 

			;; (global-hide-mode-line-mode 1)

  #+end_src

* Yasnippet
#+begin_src emacs-lisp
    (use-package yasnippet
      :straight t)
    (setq yas-snippet-dirs
	  '("~/aios/editor/emacs/snippets/"))
(yas-global-mode 1)
#+end_src

