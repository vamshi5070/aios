(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

(straight-use-package 'use-package)

(defun reload ()
	   (interactive )
		(load-file (expand-file-name "~/.emacs.d/init.el")))

(straight-use-package
  '(nano-emacs :type git :host github :repo "rougier/nano-emacs"))

(menu-bar-mode -1)
      (setq nano-font-family-monospaced "SF Mono")
    (setq nano-font-size 13)
    (setq nano-font-family-proportional "Consolas")
   ;;     (require 'nano)

  (add-to-list 'load-path ".")

  ;; Default layout (optional)
  (require 'nano-layout)

  ;; Theming Command line options (this will cancel warning messages)
  (add-to-list 'command-switch-alist '("-dark"   . (lambda (args))))
  (add-to-list 'command-switch-alist '("-light"  . (lambda (args))))
  (add-to-list 'command-switch-alist '("-default"  . (lambda (args))))
  (add-to-list 'command-switch-alist '("-no-splash" . (lambda (args))))
  (add-to-list 'command-switch-alist '("-no-help" . (lambda (args))))
  (add-to-list 'command-switch-alist '("-compact" . (lambda (args))))


         (require 'nano-theme-dark)
(use-package exotica-theme
  :straight t)

  ;; Customize support for 'emacs -q' (Optional)
  ;; You can enable customizations by creating the nano-custom.el file
  ;; with e.g. `touch nano-custom.el` in the folder containing this file.
  (let* ((this-file  (or load-file-name (buffer-file-name)))
         (this-dir  (file-name-directory this-file))
         (custom-path  (concat this-dir "nano-custom.el")))
    (when (and (eq nil user-init-file)
               (eq nil custom-file)
               (file-exists-p custom-path))
      (setq user-init-file this-file)
      (setq custom-file custom-path)
      (load custom-file)))

  ;; Theme
  (require 'nano-faces)
  (nano-faces)

  (require 'nano-theme)
  (nano-theme)

  ;; Nano default settings (optional)
  (require 'nano-defaults)

  ;; Nano session saving (optional)
  (require 'nano-session)

  ;; Nano header & mode lines (optional)
  (require 'nano-modeline)

  ;; Nano key bindings modification (optional)
  (require 'nano-bindings)

  ;; Compact layout (need to be loaded after nano-modeline)
  (when (member "-compact" command-line-args)
    (require 'nano-compact))

  ;; Nano counsel configuration (optional)
  ;; Needs "counsel" package to be installed (M-x: package-install)
  ;; (require 'nano-counsel)

  ;; Welcome message (optional)
  (let ((inhibit-message t))
    (message "Welcome to GNU Emacs / N Λ N O edition")
    (message (format "Initialization time: %s" (emacs-init-time))))

  ;; Splash (optional)
  (unless (member "-no-splash" command-line-args)
    (require 'nano-splash))

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
    :init
  (savehist-mode))

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(save-place-mode 1)

(defvar my-subword-forward-regexp "[[[:upper:]]*[[:digit:][:lower:]]+\\|[[:upper:]]+\\|[^[:word:][:space:]_\n]+\\|-|(|")

  (defvar my-subword-backward-regexp "[[:space:][:word:]_\n][^\n[:space:][:word:]_]+\\|\\(\\W\\|[[:lower:]]\\)[[:upper:]]\\|\\W\\w+")
  ;; (defvar my-subword-backward-regexp "[^[:word:][:space:]_\n]+")
  (defun my-subword-forward-internal () ""
	 (interactive)  
	 (let ((case-fold-search nil))
	   (re-search-forward my-subword-forward-regexp nil t))
	 (goto-char (match-end 0)))

  (defun my-subword-backward-internal () ""
	 (interactive)
	 (let ((case-fold-search nil))
	   (if (re-search-backward my-subword-backward-regexp nil "don't panic!")
	   (goto-char (1+ (match-beginning 0))))))

  (setq subword-forward-regexp 'my-subword-forward-regexp)
  (setq subword-backward-regexp 'my-subword-backward-regexp)
  (setq subword-backward-function 'my-subword-backward-internal)
  (setq subword-forward-function 'my-subword-forward-internal)
(subword-mode 1)

(use-package diminish
  :straight t
:defer 5
:config ;; Let's hide some markers.
  (diminish  'org-indent-mode))

(defun rational-completion/minibuffer-backward-kill (arg)
"When minibuffer is completing a file name delete up to parent
  folder, otherwise delete a word"
(interactive "p")
(if minibuffer-completing-file-name
	;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
	(if (string-match-p "/." (minibuffer-contents))
	(zap-up-to-char (- arg) ?/)
	  (delete-minibuffer-contents))
  (backward-kill-word arg)))

  (use-package vertico
	:straight t
	:init
	(vertico-mode))
	(setq vertico-count 9
		  vertico-cycle t)

  (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
  (define-key vertico-map (kbd "C-j") #'vertico-next)
  (define-key vertico-map (kbd "C-k") #'vertico-previous)
      (define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

(use-package mini-frame
:straight t)

(use-package orderless
:straight t)
;; Configure a custom style dispatcher (see the Consult wiki)
;; (setq orderless-style-dispatchers '(+orderless-dispatch)
;;       orderless-component-separator #'orderless-escapable-split-on-space)
(setq completion-styles '(orderless)
  completion-category-defaults nil
  completion-category-overrides '((file (styles partial-completion))))

(use-package vertico-posframe
    :straight t
    )

(setq vertico-posframe-parameters
      '((left-fringe . 8)
        (right-fringe . 8)))
(vertico-posframe-mode 1)

(use-package haskell-mode
  :straight t)
	(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
	(setq  haskell-interactive-popup-errors nil)

(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")

(use-package envrc    
  :straight t)

(envrc-global-mode 1)

(use-package magit
      :straight t)

;; (defun cursorToBar () "Set the mark at the location of the point."
				   ;; (interactive)	
			   ;; (ryo-modal-keys
			  ;; ("h" 'dired-up-directory ))
	;; )

	  ;; (add-hook 'dired-mode-hook #'cursorToBar)

			   (put 'dired-find-alternate-file 'disabled nil)

	(define-key dired-mode-map (kbd "/") 'dired-goto-file)
		 (define-key dired-mode-map (kbd "j") 'dired-next-line)
		 (define-key dired-mode-map (kbd "k") 'dired-previous-line)
		 (define-key dired-mode-map (kbd "h") 'dired-up-directory)
		 (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
		 ;;(evil-define-key 'normal dired-mode-map
	;;		(kbd "M-RET") 'dired-display-file
	;;		(kbd "h") 'dired-up-directory
	;;		(kbd "l") 'dired-find-alternate-file ); use dired-find-file instead of dired-open.
  ;; (defun dired-config ()
;; "my dired config"
;; (dired-hide-details-mode 1)
;;     )
 (add-hook 'dired-mode-hook 'dired-hide-details-mode)
 (add-hook 'dired-mode-hook 'dired-omit-mode)

		  (setq dired-listing-switches "-ahl -agho --group-directories-first"
			 dired-omit-files "^\\.[^.].*"
			 dired-omit-verbose nil
			 dired-hide-details-hide-symlink-targets nil
			 delete-by-moving-to-trash t)

  ;;  (ryo-modal-major-mode-keys
   ;; 'dired-mode-hook
   ;; ("h" dired-up-directory )
   ;; ("l" dired-find-alternate-file ))

;; Use dabbrev with Corfu!
(use-package dabbrev
    :straight t
  ;; Swap M-/ and C-M-/
  :bind (("M-/" . dabbrev-completion)
	 ("C-M-/" . dabbrev-expand)))

(use-package org-modern
  :straight t)

    ;; Choose some fonts
    ;; (set-face-attribute 'default nil :family "???")
    ;; (set-face-attribute 'variable-pitch nil :family "???")

    ;; Add frame borders and window dividers
   ;; (modify-all-frames-parameters
    ;; '((right-divider-width . 40)
  ;; (internal-border-width . 40)))
   ;; (dolist (face '(window-divider
		   ;; window-divider-first-pixel
		  ;; window-divider-last-pixel))
     ;; (face-spec-reset-face face)
     ;; (set-face-foreground face (face-attribute 'default :background)))
   ;; (set-face-background 'fringe (face-attribute 'default :background))

    (setq
     ;; Edit settings
     org-auto-align-tags nil
     org-tags-column 0
     org-catch-invisible-edits 'show-and-error
     org-special-ctrl-a/e t
     org-insert-heading-respect-content t

     ;; Org styling, hide markup etc.
     org-hide-emphasis-markers t
     org-pretty-entities t
     org-ellipsis "⤵"
     ;; "…"

     ;; Agenda styling
     org-agenda-block-separator ?─
     org-agenda-time-grid
     '((daily today require-timed)
   (800 1000 1200 1400 1600 1800 2000)
   " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
     org-agenda-current-time-string
     "⭠ now ─────────────────────────────────────────────────")

    ;; Enable org-modern-mode
(add-hook 'org-mode-hook #'org-modern-mode)
    (add-hook 'org-agenda-finalize-hook #'org-modern-agenda)

(add-hook 'org-mode-hook #'org-shifttab)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    (setq-default cursor-type 'bar) 
    (setq ryo-modal-cursor-color "#000000")
    (blink-cursor-mode 0)
    ;;(global-set-key (kbd "<escape>")  'ryo-modal-mode)
    (defun kakoune-a (count)
  "Select COUNT lines from the current line.
                   Note that kakoune's x does behaves exactly like this,
                   and I like this behavior better."
  (interactive "p")
  (forward-char count)
  (ryo-modal-mode 0))

    (defun kakoune-set-mark-here () "Set the mark at the location of the point."
       (interactive) (set-mark (point)))

    (defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
       (interactive)
       (unless (use-region-p) (set-mark (point))))

    (defun kakoune-deactivate-mark ()
  "Deactivate the mark.
                   Deactivate the mark unless mark-region-mode is active."
  (interactive)
  ;;(unless rectangle-mark-mode (deactivate-mark)))
  (deactivate-mark))
    (defun kakoune-O (count)
  "Open COUNT lines above the cursor and go into insert mode."
  (interactive "p")
  (beginning-of-line)
  (dotimes (_ count)
    (newline)
    (forward-line -1)))

    (defun kakoune-x (count)
  "Select COUNT lines from the current line.
                   Note that kakoune's x does behaves exactly like this,
                   and I like this behavior better."
  (interactive "p")
  (beginning-of-line count)
  (set-mark (point))
  (end-of-line count)
  (forward-char count))

    (defun kakoune-d (count)
  "Kill selected text or COUNT chars."
  (interactive "p")
  (if (use-region-p)
      (kill-region (region-beginning) (region-end))
    (delete-char count t)))

    (defun kakoune-insert-mode () "Return to insert mode."
       (interactive)
       (ryo-modal-mode 0))


    (defun kakoune-exchange () "Return to insert mode."
       (interactive)
       (exchange-point-and-mark))

    (defun kakoune-normal-mode () "Return to insert mode."
       (interactive)
       (ryo-modal-mode 1))

    (defun ryo-after () "Enter normal mode"
       (interactive)
       (forward-char)
       (ryo-modal-mode 0))

    (defun kakoune-X (count)
  "Extend COUNT lines from the current line."
  (interactive "p")
  (beginning-of-line)
  (unless (use-region-p) (set-mark (point)))
  (forward-line count))

    (defun kakoune-T (count)
  "Extend COUNT lines from the current line."
  (interactive "p")
  (beginning-of-line)
  (unless (use-region-p) (set-mark (point)))
  (previous-line count))

    (defun kakoune-gg (count)
  "Go to the beginning of the buffer or the COUNTth line."
  (interactive "p")
  (goto-char (point-min))
  (when count (forward-line (1- count))))

    (defun kakoune-o (count)
  "Open COUNT lines under the cursor and go into insert mode."
  (interactive "p")
  (end-of-line)
  (dotimes (_ count)
    (electric-newline-and-maybe-indent))
  (ryo-modal-mode 0))

    (defun kakoune-p (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (forward-line)
  (dotimes (_ count) (save-excursion (yank))))

    (defun kakoune-P (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (dotimes (_ count) (save-excursion (yank))))

    (defvar kakoune-last-t-or-f ?f
  "Using t or f command sets this variable.")

    (defun kakoune-select-to-char (arg char)
  "Select up to, and including ARGth occurrence of CHAR.
               Case is ignored if `case-fold-search' is non-nil in the current buffer.
               Goes backward if ARG is negative; error if CHAR not found.
               Ignores CHAR at point."
  (interactive "p\ncSelect to char: ")
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?f)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg))
    (point)))

    (defun kakoune-select-up-to-char (arg char)
  "Select up to, but not including ARGth occurrence of CHAR.
               Case is ignored if `case-fold-search' is non-nil in the current buffer.
               Goes backward if ARG is negative; error if CHAR not found.
               Ignores CHAR at point."
  (interactive "p\ncSelect up to char: ")
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?t)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg)
      (backward-char direction))
    (point)))
    (defun kakoune-word-backward ()
  (interactive)
  ;; (progn (forward-char)
  ;;     (forward-word))
  (subword-backward
   ))

    (defun kakoune-word-forward ()
  (interactive)
  ;; (progn (forward-char)
  ;;     (forward-word))
  (subword-forward
   ))

    (defun kakoune-forward-char ()
  (interactive)
  (forward-char))

    (defun kakoune-A (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (end-of-line)
  (ryo-modal-mode 0))


  (defun kakoune-downcase ()
    "Downcase region."
    (interactive)
    (if (use-region-p)
    (downcase-region (region-beginning) (region-end))
  (downcase-region (point) (+ 1 (point)))))

  (defun kakoune-upcase ()
    "Upcase region."
    (interactive)
    (if (use-region-p)
    (upcase-region (region-beginning) (region-end))
  (upcase-region (point) (1+ (point)))))

  (defun kakoune-replace-char (char)
  "Replace selection with CHAR."
  (interactive "cReplace with char: ")
  (mc/execute-command-for-all-cursors
   (lambda () (interactive)
     (if (use-region-p)
     (progn (let ((region-size (- (region-end) (region-beginning))))
          (delete-region (region-beginning) (region-end))
          (mc/save-excursion
               (insert-char char region-size t))))
   (progn (delete-region (point) (1+ (point)))
          (mc/save-excursion
           (insert-char char)))))))

(defun kakoune-replace-selection ()
  "Replace selection with killed text."
  (interactive)
  (if (use-region-p)
  (progn (delete-region (region-beginning) (region-end))
         (yank))
    (progn (delete-region (point) (1+ (point)))
       (yank))))

(defun kakoune-insert-line-below (count)
  "Insert COUNT empty lines below the current line."
  (interactive "p")
  (save-excursion
    (end-of-line)
    (open-line count)))

(defun kakoune-insert-line-above (count)
  "Insert COUNT empty lines above the current line."
  (interactive "p")
  (save-excursion
    (end-of-line 0)
    (open-line count)))

(defun kakoune-paste-above (count)
  "Paste (yank) COUNT times above the current line."
  (interactive "p")
  (save-excursion
    (dotimes (_ count) (end-of-line 0)
         (newline)
         (yank))))

(defun kakoune-paste-below (count)
  "Paste (yank) COUNT times below the current line."
  (interactive "p")
  (save-excursion
    (dotimes (_ count) (end-of-line)
         (newline)
         (yank))))

    (use-package ryo-modal
  :straight t
  :commands ryo-modal-mode
  :bind ("C-a" . ryo-modal-mode)
  :config
  (ryo-modal-keys
   ;; Basic keybindings
   (:mc-all t)
   ("<RET>" next-line)
   (";" kakoune-deactivate-mark)
   ;; ("0" beginning-of-line )
   ("<backspace>" backward-char :first '(kakoune-deactivate-mark) )
   ("%" mark-whole-buffer)
   ("," ryo-modal-repeat)
   ("`" kakoune-downcase)
   ("~" kakoune-upcase)
   ;;("/" consult-line)
   ("C-=" text-scale-increase)   
   ("C--" text-scale-decrease)
   ("a" ryo-after :first '(kakoune-deactivate-mark))
   ("b" kakoune-word-backward :first '(kakoune-set-mark-here))
   ("c" kakoune-d :exit t)
   ;; ("c" comment-line)
   ("d" kakoune-d)
   ("f" kakoune-select-to-char :first '(kakoune-set-mark-here ))
   ;;("f" evil-snipe-f :first '(kakoune-set-mark-here))
   ("h" backward-char :first '(kakoune-deactivate-mark))
   ("i" kakoune-insert-mode :first '(kakoune-deactivate-mark))
   ("j" next-line :first '(kakoune-deactivate-mark))
   ("k" previous-line :first '(kakoune-deactivate-mark))
   ("l" kakoune-forward-char :first '(kakoune-deactivate-mark))
   ("o" kakoune-o)
   ("p" kakoune-p)
   ("r" kakoune-replace-char)
   ("R" kakoune-replace-selection)
   ;;("q" ryo-modal-mode)
   ;; ("s" save-buffer)
   ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here ))
   ("w" kakoune-word-forward :first '(kakoune-set-mark-here))
   ("x" kakoune-x)
   ("y" kill-ring-save)
   ("A" kakoune-A)
   ("B" backward-word :first '(kakoune-set-mark-if-inactive))
   ("C" kill-line :exit t)
   ("F" kakoune-select-to-char :first '(kakoune-set-mark-if-inactive ))
   ;;("F" evil-snipe-F :first '(kakoune-set-mark-here ))
   ("H" backward-char :first '(kakoune-set-mark-if-inactive))
   ("J" next-line :first '(kakoune-set-mark-if-inactive))
   ("K" previous-line :first '(kakoune-set-mark-if-inactive))
   ("L" forward-char :first '(kakoune-set-mark-if-inactive))
   ("M-w" forward-symbol :first '(kakoune-set-mark-here))
   ("O" kakoune-O)
   ("P" kakoune-P)
   ("T" kakoune-T)
   ("W" forward-word :first '(kakoune-set-mark-if-inactive))
   ("X" kakoune-X)
   ("ZZ" kill-this-buffer)
   ("M-;" kakoune-exchange)
   )

  (ryo-modal-keys
   ;; first argument to ryo-modal-keys may be a list of keywords.
   ;; These keywords will be applied to all keybindings.
   (:norepeat t)
   ;;("0" "M-0")
   ("1" "M-1")
   ("2" "M-2")
   ("3" "M-3")
   ("4" "M-4")
   ("5" "M-5")
   ("6" "M-6")
   ("7" "M-7")
   ("8" "M-8")
   ("7" "M-7")
   ("9" "M-9")))


    ;; (ryo-modal-key
    ;; 	 "M" '( "x" 

    ;; general

    ;;  (ryo-modal-key
    ;;   "C" '(("s" save-buffer)))
    (ryo-modal-key
     "SPC" '(
         ;;("a" consult-line)
         ;;("b b" consult-buffer)
         ("b [" previous-buffer)
         ("b ]" next-buffer)
         ("f f" find-file)
         ("f s" save-buffer)
         ;;("g g" magit-status)
         ("h f" describe-function)
         ("h k" describe-key)
         ;; ("h t" consult-theme)
         ;; ("o t" vterm-toggle)
         ;; ("j" next-buffer)
         ;; ("k" previous-buffer)
         ("p p" project-find-file)
         ("p P" project-switch-project)
        ;; ("s s" consult-line);;save-buffer)
         ("r r" reload)
         ("w C-o" delete-other-windows)
         ("w d" delete-window)
         ("w q" kill-buffer-and-window)
         ("w s" split-window-below)
         ("w v" split-window-right)
         ("w w" other-window)
         ("SPC" execute-extended-command)
         ("<tab> N" tab-bar-new-tab-to )
         ("<tab> n"  tab-bar-switch-to-next-tab )
         ("<tab> d" tab-bar-close-tab)
         ("<tab> b" switch-to-buffer-other-tab)
         ("<tab> f" find-file-other-tab)
         ("<tab> a" tab-bar-close-other-tabs )
         ;; ("<tab> F" dired-other-tab)
         ))

    (ryo-modal-key
     "g" '(("h" beginning-of-line)
       ("j" end-of-buffer)
       ("g" kakoune-gg)
       ("l" end-of-line)
       ("k" beginning-of-buffer)))

    (defun haskellCode ()
  (interactive )
  (ryo-modal-key "SPC" '(
                 ("c r" haskell-process-reload)
                 ("c l" haskell-process-load-file)
                 ("c c" haskell-interactive-switch)
                 )
             ))
    (defun haskellCodeRepl ()
  (interactive )
  (ryo-modal-key "SPC" '(
                 ;;("c r" haskell-process-reload)
                 ;;("c l" haskell-process-load-file)
                 ("c c" haskell-interactive-switch-back)
                 )))

    ;; ("h" dired-up-directory )
    ;; ("l" dired-find-alternate-file ))
    (add-hook 'haskell-mode-hook #'haskellCode)
    (add-hook 'haskell-interactive-mode-hook #'haskellCodeRepl)
    ;; (global-set-key (kbd "<escape>") 'kakoune-normal-mode)
    (add-hook 'prog-mode-hook #'kakoune-normal-mode)
    (add-hook 'org-mode-hook #'kakoune-normal-mode)
    (add-hook 'fundamental-mode-hook #'kakoune-normal-mode)
    ;;		      (add-hook 'dired-mode-hook #'kakoune-normal-mode)
    (add-hook 'latex-mode-hook #'kakoune-normal-mode)

    ;; (add-hook 'interactive-haskell-mode-hook 'ac-haskell-process-setup)
    (add-hook 'haskell-interactive-mode-hook #'kakoune-normal-mode)
    ;;(push '((nil . "ryo:.*:") . (nil . "")) which-key-replacement-alist)
    (ryo-modal-mode 1)

(use-package undo-tree
  :straight t
  :config
  (global-undo-tree-mode)
  :ryo
  ("u" undo-tree-undo)
  ("U" undo-tree-redo)
  ("SPC u" undo-tree-visualize)
  :bind (:map undo-tree-visualizer-mode-map
              ("h" . undo-tree-visualize-switch-branch-left)
              ("j" . undo-tree-visualize-redo)
              ("k" . undo-tree-visualize-undo)
              ("l" . undo-tree-visualize-switch-branch-right)))
;;(setq undo-tree-auto-save-history t)

(use-package multiple-cursors
  :straight t)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

(global-set-key (kbd "<escape>")  'kakoune-normal-mode)
;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(use-package ace-window
    :straight t)
(global-set-key (kbd "M-o") 'ace-window)

    (ryo-modal-key
     "SPC" '(
	      ("w i" ace-window)))

;; (setq tab-bar-new-tab-choice "*scratch*")     
   (setq tab-bar-show nil)

(use-package consult
          :straight t
          )
        (global-set-key (kbd "C-x b") 'consult-buffer)

        (ryo-modal-key
             "SPC" '(
                  ("s i" consult-imenu)
  ("s s" consult-line)
("h t" consult-theme)
	       
  ("b b" consult-buffer)

                  ))
