(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

(straight-use-package 'use-package)

(set-face-attribute 'font-lock-comment-face nil
      :slant 'italic)
(add-to-list 'load-path "~/aios/editor/emacs/emux/")

(require 'emux-nano)
    (set-face-attribute 'font-lock-keyword-face nil
	    :slant 'italic)

(use-package hydra
    :straight t)

(defhydra hydra-zoom (global-map "<f2>")
  "zoom"
  ("g" text-scale-increase "in")
  ("l" text-scale-decrease "out")
  ("q" nil "quit")
  )

(defhydra emux/buffer (:timeout 4)
  "buffer"
  ("j" (next-buffer) "next")
  ("k"  (previous-buffer) "prev")
  ("d"  (kill-this-buffer) "kill")
  ;; ("k"  "out")
  ("q" nil "quit")
  )

(defhydra emux/window (global-map "C-c w")
  "window"
  ("j" (other-window 1) "next")
  ("k"  (other-window -1) "prev")
  ;; ("k"  "out")
  ("q" nil "quit")
  )

(defhydra emux/tab (global-map "C-c t")
  "tab"
  ("j" tab-next "next")
  ("k" tab-previous "prev")
  ("n"  tab-new "new")
  ("s"  tab-switch "switch")
  ;; ("k"  "out")
  ("q" nil "quit")
  )

(setq-default cursor-type '(hbar . 2 ))
(setq-default cursor-in-non-selected-windows nil)
(setq blink-cursor-mode nil)
;; (require 'emux-splash)

(setq user-full-name "Vamshi krishna"
      user-mail-address "vamshi5070k@gmail.com")

(require 'emux-appearance)

(setq gc-cons-percentage 0.6)

(column-number-mode t)
(global-display-line-numbers-mode 0)
   (setq display-line-numbers-type 'relative)
   (dolist (mode '(
		   org-mode-hook
		   prog-mode-hook
		   ;; term-mode-hook
		   ;; vterm-mode-hook
		   ;; shell-mode-hook
		   ;; dired-mode-hook
		   ;; eshell-mode-hook
		   ))
     (add-hook mode (lambda () (display-line-numbers-mode 1))))

(require 'emux-tab-bar)

;; Persist history over Emacs restarts. Vertico sorts by history position.
;;    (use-package savehist
 ;;       :init
(require 'savehist)
      (savehist-mode 1)
;;)

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

(require 'emux-config)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)
;; NOTE: If you want to move everything out of the ~/.emacs.d folder
;; reliably, set `user-emacs-directory` before loading no-littering!
;(setq user-emacs-directory "~/.cache/emacs")

(use-package no-littering
      :straight t
    )
;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(setq save-place-limit 100)
  (save-place-mode 1)

(defun rational-completion/minibuffer-backward-kill (arg)
	"When minibuffer is completing a file name delete up to parent
      folder, otherwise delete a word"
	(interactive "p")
	(if minibuffer-completing-file-name
	    ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
	    (if (string-match-p "/." (minibuffer-contents))
		(zap-up-to-char (- arg) ?/)
	      (delete-minibuffer-contents))
	  (backward-kill-word arg)))
 (require 'ido)
 (ido-mode t)
(setq ido-enable-flex-matching t)
  ;; (define-key ido-completion-map (kbd "C-d") 'ido-switch-to-dired)
;; (define-key ido-completion-map (kbd "<escape>") #'keyboard-escape-quit)

      (use-package vertico
	    :straight t
	    :init
	    (vertico-mode))
	  (setq vertico-count 10
		vertico-cycle t)

      (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
      (define-key vertico-map (kbd "C-j") #'vertico-next)
      (define-key vertico-map (kbd "C-k") #'vertico-previous)
      (define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

(use-package haskell-mode
  :straight t)
	    (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
	    (setq  haskell-interactive-popup-errors nil)

(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")

(use-package rust-mode
    :straight t)

(use-package orderless
:straight t)
;; Configure a custom style dispatcher (see the Consult wiki)
;; (setq orderless-style-dispatchers '(+orderless-dispatch)
;;       orderless-component-separator #'orderless-escapable-split-on-space)
(setq completion-styles '(orderless)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

(use-package envrc    
  :straight t)

(envrc-global-mode 1)

(use-package multiple-cursors
  :straight t)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

(use-package ace-window
    :straight t)
(global-set-key (kbd "M-o") 'ace-window)

(use-package consult
  :straight t
  )
(global-set-key (kbd "C-x b") 'consult-buffer)

(require 'emux-buffer)

	;; (global-set-key (kbd "C-x C-b") (progn '-buffer ))
    ;;  (setq mylist (list "red" "blue" "yellow" "clear" "i-dont-know"))
    ;; (ido-completing-read "What, ... is your favorite color? " mylist)

(require'emux-dired)

(require 'emux-org)

(use-package magit
 :straight t)

(use-package popper
   :straight t
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Warnings\\*"
          help-mode
          compilation-mode))
  (popper-mode +1))

(use-package super-save
      :straight t
      ;; :ensure t
      :config
 (setq super-save-auto-save-when-idle t)
  (setq super-save-idle-duration 5) ;; after 5 seconds of not typing autosave
  ;; add integration with ace-window
  (add-to-list 'super-save-triggers 'ace-window)

  ;; save on find-file
  (add-to-list 'super-save-hook-triggers 'find-file-hook)
  (super-save-mode +1))

;; (use-package auctex

(use-package auctex
  :straight t
  :ensure t
  :defer t)
  ;; :hook (LaTeX-mode . (lambda ()
  ;;           (push (list 'output-pdf "Zathura")
  ;;                 TeX-view-program-selection))))

(use-package corfu
	 :straight t
  :bind (:map corfu-map
	      ("C-j" . corfu-next)
	 ("C-k" .  corfu-previous)
	 ("TAB" .  corfu-insert)
       ("C-f" . corfu-insert))    ;; Optional customizations
   :custom
  ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
   (corfu-auto t)                 ;; Enable auto completion
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect-first nil)    ;; Disable candidate preselection
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-echo-documentation nil) ;; Disable documentation in the echo area
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; You may want to enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since dabbrev can be used globally (M-/).
  :init
  (corfu-global-mode))

;; Use dabbrev with Corfu!
(use-package dabbrev
  ;; Swap M-/ and C-M-/
  :bind (("M-/" . dabbrev-completion)
	 ("C-M-/" . dabbrev-expand)))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  (setq completion-cycle-threshold 3)

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete))

;; (use-package eglot
;; :straight t)
  ;; (add-hook 'haskell-mode-hook 'eglot-ensure)

(use-package pdf-tools
    :defer t
:commands (pdf-view-mode pdf-tools-install)
 :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
 :magic ("%PDF" . pdf-view-mode)     :config
	(pdf-tools-install)
	(setq-default pdf-view-display-size 'fit-page)
    :bind (:map pdf-view-mode-map
	  ("\\" . hydra-pdftools/body)
	  ("<s-spc>" .  pdf-view-scroll-down-or-next-page)
	  ("g"  . pdf-view-first-page)
	  ("G"  . pdf-view-last-page)
	  ("l"  . image-forward-hscroll)
	  ("h"  . image-backward-hscroll)
	  ("j"  . pdf-view-next-page)
	  ("k"  . pdf-view-previous-page)
	  ("e"  . pdf-view-goto-page)
	  ("u"  . pdf-view-revert-buffer)
	  ("al" . pdf-annot-list-annotations)
	  ("ad" . pdf-annot-delete)
	  ("aa" . pdf-annot-attachment-dired)
	  ("am" . pdf-annot-add-markup-annotation)
	  ("at" . pdf-annot-add-text-annotation)
	  ("y"  . pdf-view-kill-ring-save)
	  ("i"  . pdf-misc-display-metadata)
	  ("s"  . pdf-occur)
	  ("b"  . pdf-view-set-slice-from-bounding-box)
	  ("r"  . pdf-view-reset-slice)))

    ;; (use-package org-pdfview
    ;;     :config 
    ;;             (add-to-list 'org-file-apps
    ;;             '("\\.pdf\\'" . (lambda (file link)
    ;;             (org-pdfview-open link)))))

(require 'emux-window)

(require 'emux-keybindings)
