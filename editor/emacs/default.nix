{ pkgs, nurNoPkgs, ... }: {
  programs.emacs = {
    enable = true;
    package = pkgs.emacs; # PgtkGcc;
   extraPackages = epkgs: with epkgs; [
    #orderless
    vterm
    pdf-tools
    org-pdftools
    # org-pdfview
    ];
  };
  services.emacs = {
    enable = true;
    defaultEditor = true;
    client = {
      enable = true;
      arguments = [ "-c" ];
    };
    # socketActivation.enable = true;
  };

  home.packages = with pkgs; [
    # emacs27Packages.all-the-icons-dired
   # emacs-all-the-icons-fonts
   ripgrep
   # fd
   # python38
  #  hlint
  #  nixfmt
  ];
  home.file.".emacs.d/init.el".source = ./init.el;
  # home.file.".doom.d/config.el".source = ./doom.d/config.el;
  # home.file.".doom.d/init.el".source = ./doom.d/init.el;
  # home.file.".doom.d/packages.el".source = ./doom.d/packages.el;
  #home.activation.emacs = {
  #  before = [ ];
  #  after = [ ];
  #  data = "$DRY_RUN_CMD mkdir -p ~/.emacs.d/autosave";
  #};
}
