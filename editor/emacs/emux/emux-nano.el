(straight-use-package
	     '(nano-emacs :type git :host github :repo "rougier/nano-emacs"))
(set-face-attribute 'default t
			:background "#000000"
			:foreground "#ffffff"
			:family "Operator Mono"
			:height 18)

    (setq nano-font-family-monospaced "Operator Mono")
    (setq nano-font-size 14)
    (require 'nano-layout)

    (defun nano-theme-set-spaceduck ()
      (setq frame-background-mode 'dark)
      (setq nano-color-foreground "#ecf0c1")
      (setq nano-color-background "#0f111b")
      (setq nano-color-highlight  "#1b1c36")
      (setq nano-color-critical   "#e33400")
      (setq nano-color-salient    "#00a4cc")
      (setq nano-color-strong     "#eeeee3")
      (setq nano-color-popout     "#f2ce00")
      (setq nano-color-subtle     "#7a5ccc")
      (setq nano-color-faded      "#b3a1e6"))

    (nano-theme-set-spaceduck)

    (require 'nano-faces)
    (nano-faces)
    (require 'nano-theme)
    (nano-theme)
    (set-face-attribute 'bold nil :weight 'bold)
 ;;   (require 'nano-defaults)
  (require 'nano-session)

  (require 'nano-modeline)

(let ((inhibit-message t))
  (message "Welcome to GNU Emacs / N Λ N O edition")
  (message (format "Initialization time: %s" (emacs-init-time))))

(require 'nano-splash)

(provide 'emux-nano)
