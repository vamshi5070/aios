	(use-package vertico
	      :straight t
	      :init
	      (vertico-mode))
	    (setq vertico-count 10
		  vertico-cycle t)

	(define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
	(define-key vertico-map (kbd "C-j") #'vertico-next)
	(define-key vertico-map (kbd "C-k") #'vertico-previous)
    (use-package consult
      :straight t
      )
       (use-package orderless
       :straight t)
       ;; Configure a custom style dispatcher (see the Consult wiki)
       ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
       ;;       orderless-component-separator #'orderless-escapable-split-on-space)
       (setq completion-styles '(orderless)
	     completion-category-defaults nil
	     completion-category-overrides '((file (styles partial-completion))))

(provide 'emux-bling)
