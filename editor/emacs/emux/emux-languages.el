    (use-package haskell-mode
      :straight t)
		(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
		(setq  haskell-interactive-popup-errors nil)
(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")

   (use-package rust-mode
       :straight t)
(provide 'emux-languages)
