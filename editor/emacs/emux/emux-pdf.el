
  (use-package pdf-tools
     :defer t
 :commands (pdf-view-mode pdf-tools-install)
  :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)     :config
	 (pdf-tools-install)
	 (setq-default pdf-view-display-size 'fit-page)
     :bind (:map pdf-view-mode-map
	   ("\\" . hydra-pdftools/body)
	   ("<s-spc>" .  pdf-view-scroll-down-or-next-page)
	   ("g"  . pdf-view-first-page)
	   ("G"  . pdf-view-last-page)
	   ("l"  . image-forward-hscroll)
	   ("h"  . image-backward-hscroll)
	   ("j"  . pdf-view-next-page)
	   ("k"  . pdf-view-previous-page)
	   ("e"  . pdf-view-goto-page)
	   ("u"  . pdf-view-revert-buffer)
	   ("al" . pdf-annot-list-annotations)
	   ("ad" . pdf-annot-delete)
	   ("aa" . pdf-annot-attachment-dired)
	   ("am" . pdf-annot-add-markup-annotation)
	   ("at" . pdf-annot-add-text-annotation)
	   ("y"  . pdf-view-kill-ring-save)
	   ("i"  . pdf-misc-display-metadata)
	   ("s"  . pdf-occur)
	   ("b"  . pdf-view-set-slice-from-bounding-box)
	   ("r"  . pdf-view-reset-slice)))

     ;; (use-package org-pdfview
     ;;     :config 
     ;;             (add-to-list 'org-file-apps
     ;;             '("\\.pdf\\'" . (lambda (file link)
     ;;             (org-pdfview-open link)))))

(provide 'emux-pdf)
