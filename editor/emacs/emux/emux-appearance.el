;; basic ui

 (setq inhibit-startup-message t)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1)
  (fset 'yes-or-no-p 'y-or-n-p)

;; font

(set-face-attribute 'default nil
		      :font "Source Code Pro"
				     :height 140
				     :weight 'regular)

  ;;   (set-face-attribute 'variable-pitch nil
;;			   :font "Cousine"
;;			 :height 160
;;			  :weight 'regular)

     (set-face-attribute 'fixed-pitch nil
			   :font "Source Code Pro"
			 :height 160
			  :weight 'regular)
;;       (setq-default line-spacing 0)

;;      (set-face-attribute 'font-lock-comment-face nil
;;        :slant 'italic)
;;      (set-face-attribute 'font-lock-keyword-face nil
;; 			 :slant 'italic)

;; ui

;; (load-theme 'modus-vivendi t)
    ;; (setq-default header-line-format 
    ;;  	      (list  "  " mode-line-modified "  | %m:" " %b |"))

(setq-default mode-line-format 
     	      (list  "  " mode-line-modified "  | %m:" " %b |"))
;; (setq-default mode-line-format nil)
;; (set-face-attribute 'header-line nil
;;                    :background "white" ; "#353644"
;;                    :foreground "black"
;;                    :box '(:line-width 4 :color "black")
;;                    :overline nil
;;                    :underline nil)
		    ;:family "Source Code Pro " :height 250)

;; (set-face-attribute 'mode-line-inactive nil
;;                    :background "#353644";"#565063"
;;                    :foreground "white"
;;                    :box '(:line-width 4 :color "#353644")
;;                    :overline nil
;;                    :underline nil)

;;(blink-cursor-mode 1)
(global-hl-line-mode 1)

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
;; built in theme

(setq modus-themes-italic-constructs t
	  modus-themes-bold-constructs nil
	  ;; modus-themes-variable-pitch-ui t
	;;  modus-themes-region '(bg-only no-extend)
	  modus-themes-hl-line '(accented) 
	  ;; modus-themes-syntax '(yellow-comments)
	  modus-themes-mode-line '(accented 3d))
;; Color modeline in active window, remove border
   (setq modus-themes-headings ;; Makes org headings more colorful
	 '((t . (rainbow))))

(use-package modus-themes
  :straight t)
  (load-theme 'modus-vivendi t)
 ;; (load-theme 'tango-dark t)
;; (load-theme 'modus-operandi t)
;; line number

(column-number-mode t)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'relative)
      (dolist (mode '(
		       org-mode-hook
		       prog-mode-hook
		       ;; term-mode-hook
		       ;; vterm-mode-hook
		       ;; shell-mode-hook
		       ;; dired-mode-hook
		       ;; eshell-mode-hook
		       ))
	 (add-hook mode (lambda () (display-line-numbers-mode 1))))

(provide 'emux-appearance)
