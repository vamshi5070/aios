    # init = {
    #   enable = true;
    #   packageQuickstart = false;
    #   recommendedGcSettings = true;
    #   usePackageVerbose = false;

#       earlyInit = ''
#         ;; Disable some GUI distractions. We set these manually to avoid starting
#         ;; the corresponding minor modes.
#         (push '(menu-bar-lines . 0) default-frame-alist)
#         (push '(tool-bar-lines . nil) default-frame-alist)
#         (push '(vertical-scroll-bars . nil) default-frame-alist)
#         (load-theme 'modus-vivendi t)
# (defun kakoune-l ()
#     (interactive)
#     (forward-char))

# (defun kakoune-h ()
#     (interactive)
#     (backward-char))

# (defun kakoune-insert-mode () "Return to insert mode."
# 	 (interactive)
# 	 (ryo-modal-mode 0))

# (defun kakoune-normal-mode () "Enter normal mode"
# 	 (interactive)
# 	 (forward-char)
# 	 (ryo-modal-mode 0))

#   (defun kakoune-a (count)
#     "Select COUNT lines from the current line.
# 				 Note that kakoune's x does behaves exactly like this,
# 				 and I like this behavior better."
#     (interactive "p")
#     (forward-char count)
#     (ryo-modal-mode 0))

# (defun kakoune-j () "Go to next line"
# 	 (interactive)
# 	 (next-line))

#       '';

#       prelude = ''

#       ;; Accept 'y' and 'n' rather than 'yes' and 'no'.
#       (defalias 'yes-or-no-p 'y-or-n-p)

#   (set-face-attribute 'default nil
# 		      :font "Mononoki Nerd Font Mono"
# 				     :height 170
# 				   :weight 'regular)

#     ;;;;;;;;;;; KEYBINDS
#         ;; Make ESC quit prompts
# ;;         (global-set-key (kbd "<escape>") 'keyboard-escape-quit)
# (global-set-key (kbd "<escape>")  'kakoune-normal-mode)
#   (column-number-mode t)
# 	(global-display-line-numbers-mode t)
# 	(setq display-line-numbers-type 'relative)
# 	(dolist (mode '(
# 			;;org-mode-hook
# 			term-mode-hook
# 			vterm-mode-hook
# 			shell-mode-hook
# 			dired-mode-hook
# 			eshell-mode-hook))
# 	  (add-hook mode (lambda () (display-line-numbers-mode 0))))


#   (defun kakoune-p (count)
#     "Yank COUNT times after the point."
#     (interactive "p")
#     (forward-line)
#     (dotimes (_ count) (save-excursion (yank))))

#   (defun kakoune-P (count)
#     "Yank COUNT times after the point."
#     (interactive "p")
#     (dotimes (_ count) (save-excursion (yank))))
  
#   (defun kakoune-normal  (defun kakoune-normal-mode () "Return to insert mode."
# 	 (interactive)
# 	 (ryo-modal-mode 1))

# (defun kakoune-b ()
#        (interactive)
#        (backward-word))

# (defun kakoune-w ()
#        (interactive)
#        (forward-word))

# (defun kakoune-k ()
#        (interactive)
#        (previous-line))

#  (defun kakoune-d (count)
#     "Kill selected text or COUNT chars."
#     (interactive "p")
#     (if (use-region-p)
# 	(kill-region (region-beginning) (region-end))
#       (delete-char count t)))

#   (defun kakoune-o (count)
#     "Open COUNT lines under the cursor and go into insert mode."
#     (interactive "p")
#     (end-of-line)
#     (dotimes (_ count)
#       (electric-newline-and-maybe-indent))
#     (kakoune-insert-mode))
# '';
#       postlude = ''
# (add-hook 'prog-mode-hook #'kakoune-normal-mode) 
#  (add-hook 'org-mode-hook #'kakoune-normal-mode)

# ;; zoom in/out like we do everywhere else.
# (global-set-key (kbd "C-=") 'text-scale-increase)
# (global-set-key (kbd "C--") 'text-scale-decrease)
# (global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
# (global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
#   (setq-default cursor-type 'bar)
# (setq ryo-modal-cursor-color "lightblue")
#  (global-hl-line-mode 1)
# '';
#       usePackage = {
#         ryo-modal = {
#           enable = true;
#           command = [ "ryo-modal-mode" ];
#           config = ''

# (ryo-modal-keys
#    ("," ryo-modal-repeat)
#    ("a" kakoune-a)
#    ("b" kakoune-b) 
#    ("d" kakoune-d)
#    ("h" kakoune-h)
#    ("i" kakoune-insert-mode)
#    ("j" kakoune-j)
#    ("k" kakoune-k)
#    ("l" kakoune-l)
#    ("o" kakoune-o)
#    ("p" kakoune-p)
#    ("w" kakoune-w)
#    ("P" kakoune-P)

   
# )

#   (ryo-modal-keys
#    ;; First argument to ryo-modal-keys may be a list of keywords.
#    ;; These keywords will be applied to all keybindings.
#    (:norepeat t)
#    ("0" "M-0")
#    ("1" "M-1")
#    ("2" "M-2")
#    ("3" "M-3")
#    ("4" "M-4")
#    ("5" "M-5")
#    ("6" "M-6")
#    ("7" "M-7")
#    ("8" "M-8")
#    ("9" "M-9"))
# '';
#           #functions = ["(back)"];
#         };
#         expand-region = {
#           enable = true;
#         };
#         saveplace = {
#           enable = true;
#           command = ["save-place-mode"];
#           config = ''
#                  (save-place-mode 1)
#           '';
#         };
#         recentf = {
#           enable = true;
#           command = ["recentf-mode"];
#           config = ''
#                  (setq recentf-save-file (locate-user-emacs-file "recentf")
#                        recentf-max-menu-items 20
#                        recentf-max-saved-items 500
#                        recentf-exclude '("COMMIT_MSG" "COMMIT_EDITMSG"))

#           ;; Save the file list every 10 minutes.
#                   (run-at-time nil (* 10 60) 'recentf-save-list)
#                   (global-set-key "\C-x\ \C-r" 'recentf-open-files)
#                   (recentf-mode 1)
#         '';

#         };
#         envrc = {
#           enable = true;
#           command = [ "envrc-mode" ];
#         };

#         vertico = {
#           enable = true;
#           command = [ "vertico-mode" "vertico-next" ];
#           init = "(vertico-mode)";
#           bindLocal = {
#             vertico-map = { 
#               "C-j" = "vertico-next"; 
#               "C-k" = "vertico-previous";
#               "C-f" = "vertico-exit";
#               "<escape>" = "keyboard-escape-quit";
             
#             };
#             minibuffer-local-map = {
#               "M-h" = "backward-kill-word";
#             };
#           };
#           extraConfig = ''
#             :custom
#             (vertico-cycle t)
#           '';
#         };
#         # completion style for vertico
#         orderless = {
#           enable = true;
#           config = ''
#             (setq completion-styles '(orderless)
# completion-category-defaults nil
# 	     completion-category-overrides '((file (styles partial-completion)))
#             )
#           '';
#         };

#  # undo-tree = {
#  #        enable = true;
#  #        defer = 1;
#  #        command = [ "global-undo-tree-mode" ];
#  #        config = ''
#  #          (setq undo-tree-visualizer-relative-timestamps t
#  #                undo-tree-visualizer-timestamps t
#  #                undo-tree-enable-undo-in-region t)
#  #          (global-undo-tree-mode)
#  #        '';
#  #      };
#         # Savehist for vertico
#         savehist = {
#           enable = true;
#           config = ''
#             (setq history-length 25)
#             (savehist-mode 1)
#           '';
#         };
#         # Consult with vertico
#         consult = {
#           enable = true;
#           demand = true;
#           bind = {
#             "C-s" = "consult-line";
#             "C-x b" = "consult-buffer";
#             "M-g M-g" = "consult-goto-line";
#             "M-g g" = "consult-goto-line";
#             "M-s f" = "consult-find";
#             "M-s r" = "consult-ripgrep";
#             "<escape>" = "keyboard-escape-quit"; 
#           };
#           bindLocal = { minibuffer-local-map = { C-r = "consult-history"; }; };
#         };
# 	nix-mode = {
# 		 enable = true;
#      extraConfig = ''
# 	          :mode "\\.nix\\'"
# 	        '';
# 	      };
#         nix-flake = {
#           enable = true;
#           after = [ "nix-mode" ];
#         };

#         # haskell using haskell-mode
#         haskell-mode = {
#           enable = true;
          
#         };
#         haskell-doc = {
#           enable = true;
#         };
#          all-the-icons-dired = {
#         enable = true;
#         hook = [ "(dired-mode . all-the-icons-dired-mode)" ];
#       };
#       };
#     };
