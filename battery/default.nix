{...}:
{
    services.cbatticon = {
		enable = true;
		lowLevelPercent = 10;
		iconType = "notification";
		criticalLevelPercent = 5;
		commandCriticalLevel = ''
					               	notify-send "battery critical!"
						'';
  };

}
