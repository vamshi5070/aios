{ pkgs, ... }: {
  fonts.fontconfig.enable = true;

 imports = [
   ./iosevka
 ];

  home.packages = with pkgs; [
    # fantasque-sans-mono
    fira
    fira-code
    fira-mono
    # font-awesome-ttf
    # inconsolata
    # libertine
    # symbola
    # dejavu_fonts
    # siji
    # unifont
    # crimson
    # ibm-plex
    # mononoki
    # roboto-mono
    fontforge-gtk
  ];
}
