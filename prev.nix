{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    # unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    # master.url = "github:nixos/nixpkgs/master";
    ## nix-doom-emacs.url = "github:vlaci/nix-doom-emacs";
    home-manager = {
      url = "github:nix-community/home-manager";
      # url = "github:rycee/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    custom-taffybar = {
      url = "gitlab:vamshi5070/custom-taffybar";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    lambda-launcher.url = "github:balsoft/lambda-launcher";
    # custom-taffybar = {
    # url = "gitlab:vamshi5070/custom-taffybar";
    # url = path:../dotfiles/config/taffybar/taffybar;
    # inputs.nixpkgs.follows = "nixpkgs";
    # };
    kmonad = {
      url = "github:kmonad/kmonad?dir=nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    #  xmonad.url = "github:IvanMalison/xmonad";
    #  xmonad-contrib.url = "github:IvanMalison/xmonad-contrib";
    # taffybar.url = "github:IvanMalison/taffybar";
    taffybar = {
      url =  "github:/taffybar/taffybar";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
    #dmenu = {
    # url = "github:michaelforney/dmenu";
    #  flake = false;
    # };
  };
  outputs = inputs@{ self, nixpkgs, ... }:

	let
system = "x86_64-linux";

    pkgs = import nixpkgs {

     inherit system;

     config = {
       	allowUnfree = true;
    };
  };

  lib = nixpkgs.lib;
  
  nurNoPkgs = import inputs.nur {
      pkgs = null;
      nurpkgs = pkgs;
  };
in
  {
    homeConfigurations = {
      cosmos = inputs.home-manager.lib.homeManagerConfiguration {
        pkgs = import inputs.nixpkgs {
          system = "x86_64-linux";
          config.allowUnfree = true;
        };
        system = "x86_64-linux";
        homeDirectory = "/home/vamshi";
        username = "vamshi";
        configuration = { dmenu, config, lib, pkgs, ... }: {
          nixpkgs.overlays = with inputs; [
            emacs-overlay.overlay
            nur.overlay
            # (import /home/vamshi/custom-taffybar/overlay.nix)
            kmonad.overlay
            # taffybar.overlay
            neovim-nightly-overlay.overlay
          ];
          # ++ taffybar.overlays;
          nixpkgs.config = {
            allowUnfree = true;
            # allowBroken = true;
            # packageOverrides = pkgs: rec{
            #   dmenu = pkgs.dmenu.override {
            #     patches = [./dmenu/patches/center.diff];
            #   };
            # };
          };
          # This value determines the Home Manager release that your
          # configuration is compatible with. This helps avoid breakage
          # when a new Home Manager release introduces backwards
          # incompatible changes.
          #
          # You can update Home Manager without changing this value. See
          # the Home Manager release notes for a list of state version
          # changes in each release.
          # home.stateVersion = "21.05";
          # home.keyboard = null;
          # Let Home Manager install and manage itself.
          programs = {
            home-manager.enable = true;
          };
          imports = [
             # nurNoPkgs.repos.rycee.hmModules.emacs-init
            # ./keyboard.nix
            # ./classy.nix
            #./files/ghci
           # ./gtk
           # ./gnome
             # ./eww
            ./editor/kakoune
           ./battery
             ./editor/emacs
           ./books
            ./desktop
            ./programs/firefox
            ./editor/neovim
           # ./editor/kakoune
           # ./fonts
           ./genPackages
           ./git
           # ./latex
           ./multimedia
            # ./music
           ./nixEnv
           ./notifications
           # ./pdf
           ./spotify
            ./terminal
             # ./xsession
            #./programs/java
            #./programs/htop
            # ./programs/xmobar
           ./services/unclutter
            #./gtk
            # ./services/notify-osd
            # ./services/picom
            # ./services/status-notifier-watcher
            # ./services/taffybar
            # ./xsession/xsession.nix
            #./fonts
            #./programs/#alacritty
            # ./programs/command-not-found
            #./programs/broot
            #./programs/direnv
            # ./programs/#emacs
            # ./programs/exa
            #./programs/fish
            #./programs/git
            # ./programs/kakoune
            # ./programs/kitty
            # ./programs/mako
            # ./programs/mpv
            #./programs/neovim
            #./programs/quteBrowser
            #./programs/obs-studio
            # ./programs/powerline-go
            #./programs/starship
            # ./programs/ssh
            #./programs/terminator
            # ./programs/texlive
            # ./programs/tmux
            #./programs/vscode#
            # ./programs/waybar
            #./programs/zathura
            # ./programs/zoxide
            # ./xresources
            # ./pkgs/dmenu
              # ./wayland
            # ./pkgs/haskell-env
            # ./pkgs/python-env
            # ./pkgs/applications
            # ./pkgs/sl
            # ./pkgs/cd
            #  ./pkgs/clojure-env
            #./services/cbatticon
            #./services/dunst
            #./services/lorri
            # ./services/polybar
            # ./services/poweralertd
            #./services/redshift
            # ./services/gammastep
            # ./services/networkManagr
            # ./services/sxhkd
            # ./services/randomBg
            # ./services/screen-locker
            #./services/stalonetray
            # ./services/trayer
            # ./services/xcape
          ];
        };
      };
    };

    packages.x86_64-linux =
      builtins.mapAttrs (_name: config: config.activationPackage)
      self.homeConfigurations;

    apps.x86_64-linux = builtins.mapAttrs (name: activationPackage: {
      type = "app";
      program = "${activationPackage}/activate";
    }) self.packages.x86_64-linux;
  };
}
