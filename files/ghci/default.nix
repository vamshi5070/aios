{ config, lib, pkgs, ... }:
{
  # home.file.".ghci".source = ./dot-ghci;

  home.file.".ghc/ghci.conf".text = with pkgs.haskellPackages; ''
    :def hoogle \s -> pure $ ":!${hoogle}/bin/hoogle --count=15 \"" <> s <> "\""
    :def hlint \s -> pure $ ":!${hlint}/bin/hlint \"" <> s <> "\""
    :def pf \s -> pure $ ":!${pointfree}/bin/pointfree \"" <> s <> "\""
    :set prompt "λ> "
    :set +t
  '';
}
