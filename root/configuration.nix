{ config, pkgs, ... }:
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
	  loader = {
	  	systemd-boot.enable = true;
		efi.canTouchEfiVariables = true;
	};
	plymouth.enable = true;
};
  

    networking = {
	hostName = "cosmos";   # Define your hostname.
	wireless = {
	enable = true;  # Enables wireless support via wpa_supplicant.
	userControlled.enable = true;  # Enables wireless support via wpa_supplicant.
	interfaces = ["wlp2s0"];
	networks.Varikuti.pskRaw = "2f5385967f945b489e113af64fb060b86319cf195898249a8cd0189e63078cbd";  # Enables wireless support via wpa_supplicant.
};
#networkmanager.enable = true;
};
fonts = {
 fonts = with pkgs; [
     anonymousPro
  source-code-pro
  office-code-pro
ubuntu_font_family
  fira-code
  roboto-mono
  font-awesome
  font-awesome_4
  cascadia-code
  (nerdfonts.override { fonts = [ "RobotoMono" "CascadiaCode" "Hasklig" "JetBrainsMono" "Mononoki" "SourceCodePro" ]; })
  emacs-all-the-icons-fonts
  vistafonts
iosevka
noto-fonts-extra
noto-fonts
];
 fontconfig = {
             defaultFonts = {
 		monospace = [ "Cousine" "Mononoki Nerd Font"  ];
             };
 };

};

 nix = {
	gc = {
        	automatic = true;
                dates = "daily";
             };
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
               '';
   };
  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

environment.shellInit = ''
     xsetroot -solid "#1c2023"
     xsetroot -cursor_name left_ptr
   '';


  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

##plymouth

  services.gvfs.enable = true;
  services.udisks2.enable = true;
  services.devmon.enable = true;

 # trusted-public-keys = [...] hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ= [...];
 # substituters = [...] https://hydra.iohk.io [...];
nix.trustedUsers = [ "root" "vamshi" ];
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable CUPS to print documents.
  # services.printing.enable = true;
  
  hardware.bluetooth = {
       enable = true;
       powerOnBoot = true;
     };
  # Enable sound.
  sound.enable = true;
  # hardware.pulseaudio.enable = true;
  hardware.pulseaudio = {
               enable = true;
               extraModules = [ pkgs.pulseaudio-modules-bt ];
               package = pkgs.pulseaudioFull;
               support32Bit = true; # Steam
    };
#    services.pantheon.apps.enable = false;
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  services.xserver =
    {
      enable = true;
      # Configure keymap in X11
      layout = "us";
      libinput= {
        enable = true;
	touchpad = {
        naturalScrolling = true;      # Enable touchpad support (enabled default in most desktopManager).
		 additionalOptions = ''
                                                                      Option "AccelSpeed" "0.5"        # Mouse sensivity
                 #                                                     Option "TapButton2" "0"          # Disable two finger tap
                 #                                                     Option "VertScroll Delta" "-180"  # scroll sensitivity
                 #                                                     Option "HorizScroll Delta" "-180"
                 #                                                     Option "FingerLow" "40"          # when finger pressure drops below this value, the driver counts it as a release.
                 #                                                     Option "FingerHigh" "70"
                 '';
		};
		  };

      windowManager = {
         xmonad = {
                                enable = true;
		                enableContribAndExtras = true;
};

      };
        displayManager = {
	    defaultSession = "none+xmonad";
	      autoLogin = 
	      		    { enable = true;
         			user = "vamshi";
	 };
			    };
  };

programs.sway.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
  };
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    dmenu
#firefox
alacritty
  pavucontrol
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  security.sudo.wheelNeedsPassword = false;
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.upower.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}

