# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

#let
#  compiledLayout = pkgs.runCommand "keyboard-layout" {} ''
#    ${pkgs.xorg.xkbcomp}/bin/xkbcomp ${./keymap/layout.xkb} $out
#  '';
#in
#
let
  my-exwm-emacs = pkgs.emacsWithPackagesFromUsePackage {
    config = /home/vamshi/nixos/programs/emacs/init.el;
    package = pkgs.emacsGcc;
  };

  exwm-load-script = pkgs.writeText "exwm-load.el" ''
    (progn
      (require 'exwm)
      ;;(configure-ivy-posframe-for-exwm)
      ;;(require 'exwm-config)
      ;;(exwm-config-default))
      (exwm-enable)
      (require 'exwm-randr)
      (setq exwm-randr-workspace-monitor-plist '(0 "eDP-1"))
      (add-hook 'exwm-randr-screen-change-hook
             (lambda ()
               (start-process-shell-command
                "xrandr" nil "xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal")))
      (exwm-randr-enable)
      )
  '';

in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
     # ./stumpper.nix
      #./wm/window-manager.nix
    ];

#services.gnome.chrome-gnome-shell.enable = true;
  # Use the systemd-boot EFI boot loader.
  boot = {
	  loader = {
	  	systemd-boot.enable = true;
		efi.canTouchEfiVariables = true;
	};
	plymouth.enable = true;
};
  

    networking = {
	hostName = "cosmos";   # Define your hostname.
	wireless = {
	enable = true;  # Enables wireless support via wpa_supplicant.
	userControlled.enable = true;  # Enables wireless support via wpa_supplicant.
	interfaces = ["wlp2s0"];
	networks.Varikuti.pskRaw = "2f5385967f945b489e113af64fb060b86319cf195898249a8cd0189e63078cbd";  # Enables wireless support via wpa_supplicant.
};
#networkmanager.enable = true;
};
fonts =
{ fonts = with pkgs; [
  (nerdfonts.override { fonts = [ "CascadiaCode" "Hasklig" "JetBrainsMono" "Mononoki" "SourceCodePro" ]; })
  emacs-all-the-icons-fonts
  vistafonts
  hack-font
 # fira-code
 # fira-code-symbols
  font-awesome-ttf
  roboto

];
# ubuntu_font_family
 fontconfig = {
         #penultimate.enable = false;
             defaultFonts = {
#                       serif = [ "Vazir" "Ubuntu" ];
#                             sansSerif = [ "Vazir" "Ubuntu" ];
 		monospace = [ "SF Mono" "Consolas" ];
             };
 };


};
 location = {
                       latitude = 17.492829;
                       longitude = 78.281284;
 };
  services.redshift = {
    enable = true;
    #latitude = 17.5;
    #longitude = 78.3;
    brightness.day = "0.85";
    brightness.night = "0.4";
    extraOptions = ["-g" "0.8:0.8:0.8" ];
    #settings = {
    #  redshift = {
    #    gamma = "0.8";
    #  };
    #};
    temperature = {
      day = 3400;
      night = 3400;
    };
  };

 nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
               '';
   };
  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

environment.shellInit = ''
     xsetroot -solid "#1c2023"
     xsetroot -cursor_name left_ptr
   '';


  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

##plymouth

services.gvfs.enable = true;
  services.udisks2.enable = true;
  services.devmon.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };


  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";
  # Enable CUPS to print documents.
  # services.printing.enable = true;
# nixpkgs.overlays = [
#          (self: super: { hie-nix = import /home/vamshi/src/hie-nix {}; })
#        ];
  hardware.bluetooth = {
       enable = true;
       powerOnBoot = true;
     };
  # Enable sound.
  sound.enable = true;
  # hardware.pulseaudio.enable = true;
hardware.pulseaudio = {
               enable = true;
               extraModules = [ pkgs.pulseaudio-modules-bt ];
               package = pkgs.pulseaudioFull;
               support32Bit = true; # Steam
               # extraConfig = ''7
               #                # load-module module-bluetooth-policy auto_switch=2
               #                	             # load-module module-switch-on-connect
               #                	             	             # load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1
               #                	             	                            #   '';
               #                	             	                              };

    };
#    services.pantheon.apps.enable = false;
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  services.xserver =
    {
      enable = true;
      # Configure keymap in X11
      layout = "us";
     # xkbOptions = "ctrl:swapcaps";
      xkbOptions = "caps:swapescape";
    #  xkbDir = "/etc/nixos/";
      libinput= {
        enable = true;
	touchpad = {
        naturalScrolling = true;      # Enable touchpad support (enabled default in most desktopManager).
		 additionalOptions = ''
                                                                      Option "AccelSpeed" "0.5"        # Mouse sensivity
                 #                                                     Option "TapButton2" "0"          # Disable two finger tap
                 #                                                     Option "VertScroll Delta" "-180"  # scroll sensitivity
                 #                                                     Option "HorizScroll Delta" "-180"
                 #                                                     Option "FingerLow" "40"          # when finger pressure drops below this value, the driver counts it as a release.
                 #                                                     Option "FingerHigh" "70"
                 '';
		};
		  };

	#desktopManager.plasma5.enable = true;
	#desktopManager.xfce = {
			#enable = true;
			#noDesktop = true;
			#enableXfwm = false;
	#};
	#displayManager.sddm.enable = true;
 #desktopManager = {
  #    xterm.enable = false;
  #    xfce = {
  #      enable = true;
  #      noDesktop = true;
  #      enableXfwm = false;
  #    };
  #  };
      windowManager = {
        # stumpwm.enable = true;
        #stumpwm-wrapper.enable = true;
	#  myExwm = {
 #     enable = true;
 #     enableDefaultConfig = false;
 #     executable = my-exwm-emacs;
 #     loadScript = exwm-load-script;
 #     alwaysTangle = true;
 #   extraEmacsPackages = epkgs: [
#     epkgs.exwm
#     epkgs.emacsql-sqlite
 #     epkgs.vterm
 #     epkgs.pdf-tools
 #   ];
  #};
#    qtile.enable = true;
#
         xmonad = {
                                enable = true;
		                enableContribAndExtras = true;
		                #configFile = "./xmonad.hs";
				#extraPackages = haskellPackages: [
  				#			haskellPackages.aeson
 # haskellPackages.split
 #haskellPackages.multimap
 # haskellPackages.tuple
 #haskellPackages.hostname
 #haskellPackages.safe
 #haskellPackages.xdg-desktop-entry
  							#haskellPackages.monad-logger	
				#];
};

      };
	#desktopManager.gnome.enable = true;
	#
#desktopManager.pantheon.enable = true;
        displayManager = {
 #
#  defaultSession = "xfce+xmonad";
	#gdm = {
	#		enable = true;
	#};
	    defaultSession = "none+xmonad";
	      autoLogin = 
	      		    { enable = true;
         			user = "vamshi";
	 };
       #  sessionCommands = "${pkgs.xorg.xkbcomp}/bin/xkbcomp ${compiledLayout} $DISPLAY";
			    };
  };

#programs.sway.enable = true;

#fonts.fonts = with pkgs; [
 # ubuntu_font_family
#  (nerdfonts.override { fonts = [  "CascadiaCode" "FiraCode" "Hasklig" "JetBrainsMono" "Mononoki" ]; })
#  emacs-all-the-icons-fonts
#  font-awesome-ttf
#  profont
#];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
  };
  nixpkgs.config.allowUnfree = true;
   # programs.java = { enable = true; package = pkgs.openjdk8; };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  wget
   # For taffybar?
  # hicolor-icon-theme
   # xcape
   # xorg.xmodmap
   # xorg.xkbcomp
   # dzen2
    # obs-studio
   # pcmanfm
   # lxappearance
   # kdenlive
   unzip
    dmenu
git
firefox
alacritty
    brightnessctl
   # okular
   # brave
   # vlc
   # lispPackages.clx-truetype
  pavucontrol
 #   libnotify
 #   neofetch
 #   scrot
    #alacritty
 #   libreoffice
 #   nitrogen
#taffybar
 #  unzip
 #   transmission-gtk
 #   scrot
    #gimp
    # haskell
    #ghc
#   clojure
#   clojure-lsp
#   leiningen
#   clj-kondo
  # ((emacsPackagesNgGen emacs).emacsWithPackages (epkgs: [
  #  epkgs.spinner
  # ]))
    # git
 #   ripgrep
 #   fd
   spotify
    # lispPackages.quicklisp
    # sbcl
    # stumpish

    # kdenlive
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  security.sudo.wheelNeedsPassword = false;
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

 # services.upower.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
 # services.upower.enable = true;
  # trusted-public-keys = [...] hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ= [...]
  # substituters = [...] https://hydra.iohk.io [...]
}

