{ config, lib, pkgs, ... }:
{
  programs.tmux = {
    enable = true;
    keyMode = "vi";
  };
}
