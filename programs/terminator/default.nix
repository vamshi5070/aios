{ config, lib, pkgs, ... }:
{
  programs.terminator = {
    enable = true;
  };
}
