{ config, lib, pkgs, ... }: {
  programs.zathura = {

    enable = true;
    options = {
      selection-clipboard = "clipboard";
      font = "Iosevka Custom 10";
    };
    #                   default-bg = palette.withGrey.black;
    #                                 default-fg = palette.withGrey.white;
    # };
    extraConfig = "  map <C-i> recolor";
  };
}
