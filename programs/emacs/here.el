
;; (load-file "./kakoune.el")


;; load theme
(load-theme 'modus-vivendi)
(setq modus-vivendi-theme-distinct-org-blocks t
        modus-vivendi-theme-rainbow-headings t
        modus-vivendi-theme-slanted-constructs t
        modus-vivendi-theme-bold-constructs t
        modus-vivendi-theme-scale-headings t
        modus-vivendi-theme-scale-1 1.05
        modus-vivendi-theme-scale-2 1.1
        modus-vivendi-theme-scale-3 1.15
        modus-vivendi-theme-scale-4 1.2)


;; Zoom in and out
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; font

(global-set-key (kbd "<escape>")  'ryo-modal-mode)
(setq ryo-modal-cursor-color "pink")

;; line numbers
(global-display-line-numbers-mode 1)

(setq display-line-numbers 'relative)

(dolist (mode '(org-mode-hook
                term-mode-hook
                vterm-mode-hook
                shell-mode-hook
		dired-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; basic ui
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
					
;; switch between buffers					
(global-set-key (kbd "C-<tab>")  'previous-buffer)
(global-set-key (kbd "C-<iso-lefttab>") 'next-buffer)

;; Displqy column number
(column-number-mode t)

;; setq inhibit startup message
(setq inhibit-startup-screen t)

(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
		    :slant 'italic)

(global-set-key (kbd "<escape>")  'ryo-modal-mode)

;; straight.el

(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

;; use package

(straight-use-package 'use-package)

;; global hide mode line (probably , the best package)
(straight-use-package 'hide-mode-line)
;;(global-hide-mode-line-mode)

;; keep folders auto save clean
(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

;; magit
(use-package magit
      :straight t)

(defun reload ()
	       (interactive )
		(load-file (expand-file-name "~/.emacs.d/init.el"))
		)
;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

;; selectrum


(straight-use-package 'selectrum)
(selectrum-mode +1)

(use-package consult
  :straight t)

(use-package mini-frame
  :straight t)

(add-hook 'after-init-hook 'mini-frame-mode)

(custom-set-variables
                  '(mini-frame-show-parameters
                    (lambda ()
                      `((top . 0.2)
                        (width . 0.8)
                        (left . 0.5)
                        (child-frame-border-width . 1))))
       '(mini-frame-detach-on-hide nil)
                  '(mini-frame-color-shift-step 0)
                  '(mini-frame-advice-functions '(read-from-minibuffer
                                                  ;; read-string
                                                  save-some-buffers yes-or-no-p))
                  '(mini-frame-ignore-commands '()))

(use-package orderless
  :straight t
  :ensure t
  :custom (completion-styles '(orderless)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                      
(defun kakoune-a (count)
  "Select COUNT lines from the current line.
Note that kakoune's x does behaves exactly like this,
and I like this behavior better."
  (interactive "p")
  (set-mark (point))
  (forward-char count)
  (ryo-modal-mode 0))

(defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
       (interactive)
       (unless (use-region-p) (set-mark (point))))

(defun kakoune-deactivate-mark ()
  "Deactivate the mark.
Deactivate the mark unless mark-region-mode is active."
  (interactive)
  ;;(unless rectangle-mark-mode (deactivate-mark)))
  (deactivate-mark))

(defun kakoune-x (count)
  "Select COUNT lines from the current line.
Note that kakoune's x does behaves exactly like this,
and I like this behavior better."
  (interactive "p")
  (beginning-of-line count)
  (set-mark (point))
  (end-of-line count)
  (forward-char count))

(defun kakoune-d (count)
  "Kill selected text or COUNT chars."
  (interactive "p")
  (if (use-region-p)
      (kill-region (region-beginning) (region-end))
    (delete-char count t)))

(defun kakoune-insert-mode () "Return to insert mode."
       (interactive)
       (ryo-modal-mode 0))

(defun ryo-after () "Enter normal mode"
       (interactive)
       	  (forward-char)
  	  (ryo-modal-mode 0))

(defun kakoune-gg (count)
  "Go to the beginning of the buffer or the COUNTth line."
  (interactive "p")
  (goto-char (point-min))
  (when count (forward-line (1- count))))

(defun kakoune-o (count)
  "Open COUNT lines under the cursor and go into insert mode."
  (interactive "p")
  (end-of-line)
  (dotimes (_ count)
    (electric-newline-and-maybe-indent))
  (ryo-modal-mode 0))

(defun kakoune-set-mark-here () "Set the mark at the location of the point."
       (interactive) (set-mark (point)))

(defun kakoune-p (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (forward-line)
  (dotimes (_ count) (save-excursion (yank))))

(defun kakoune-P (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (dotimes (_ count) (save-excursion (yank))))

(defun kakoune-A (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (end-of-line)
  (ryo-modal-mode 0))

(use-package expand-region
  :straight t
  :bind ("C-+" . er/expand-region))

(use-package ryo-modal
  :straight t
  :commands ryo-modal-mode
  :bind ("C-c SPC" . ryo-modal-mode)
  :config
  (ryo-modal-keys
   (";" kakoune-deactivate-mark)
   ("%" mark-whole-buffer)
   ("," ryo-modal-repeat)
   ("a" ryo-after)
   ("b" backward-word :first '(kakoune-set-mark-here ))
   ("d" kakoune-d)
   ("h" backward-char :first '(kakoune-deactivate-mark))
   ("i" kakoune-insert-mode)
   ("j" next-line :first '(kakoune-deactivate-mark ))
   ("k" previous-line :first '(kakoune-deactivate-mark))
   ("l" forward-char :first '(kakoune-deactivate-mark  ))
   ("o" kakoune-o)
   ("p" kakoune-p)
   ("q" ryo-modal-mode)
   ("u" undo)
   ("w" forward-word :first '(kakoune-set-mark-here))
   ("x" kakoune-x)
   ("y" kill-ring-save)
   ("A" kakoune-A)
   ("B" backward-word :first '(kakoune-set-mark-if-inactive))
   ("H" backward-char :first '(kakoune-set-mark-if-inactive))
   ("J" next-line :first '(kakoune-set-mark-if-inactive))
   ("K" previous-line :first '(kakoune-set-mark-if-inactive))
   ("L" forward-char :first '(kakoune-set-mark-if-inactive))
   ("M-w" forward-symbol :first '(kakoune-set-mark-here))
   ("P" kakoune-P)
   ("W" forward-word :first '(kakoune-set-mark-if-inactive))
)

  (ryo-modal-keys
   ;; First argument to ryo-modal-keys may be a list of keywords.
   ;; These keywords will be applied to all keybindings.
   (:norepeat t)
   ("0" "M-0")
   ("1" "M-1")
   ("2" "M-2")
   ("3" "M-3")
   ("4" "M-4")
   ("5" "M-5")
   ("6" "M-6")
   ("7" "M-7")
   ("8" "M-8")
   ("7" "M-7")
   ("9" "M-9")))

;; general
 
(ryo-modal-key
 "SPC" '(("s" save-buffer)
         ("g" magit-status)
	 ("a" consult-line)
	 ;; ("j" next-buffer)
	 ;; ("k" previous-buffer)
         ("b" ibuffer-list-buffers)
	 ("q" kill-buffer-and-window)
	 ("f" find-file)
	 ("r" reload)))


(ryo-modal-key
 "g" '(("h" beginning-of-line)
         ("j" end-of-buffer)
	 ("g" kakoune-gg)
	 ("l" end-of-line)
	 ("k" beginning-of-buffer)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
