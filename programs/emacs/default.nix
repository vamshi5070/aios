{ pkgs, config, lib, ... }: {

  programs.emacs = {
    enable = true;
    package = pkgs.emacsPgtkGcc;
    extraPackages = epkgs: with epkgs; [ vterm ];
    overrides = self: super: rec {
      haskell-mode = self.melpaPackages.haskell-mode;
      # ...
    };
  };
  # overrides =
  services.emacs = {
    enable = true;
    client = {
      enable = true;
      arguments = [ "-c" ];
    };
    socketActivation.enable = false;
  };

  # home.file.".emacs.d/init.el".source = ./init.el;
  #home.activation.emacs = {
  #  before = [ ];
  #  after = [ ];
  #  data = "$DRY_RUN_CMD mkdir -p ~/.emacs.d/autosave";
  #};

}
