;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Vamshi Krishna"
      user-mail-address "vamshi5070k@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "CaskaydiaCove Nerd Font Mono" :size 27 :weight 'regular)
(setq doom-font (font-spec :family "Hack" :size 27 :weight 'regular)
;; (setq doom-font (font-spec :family "Mononoki Nerd Font" :size 27 :weight 'regular)
      doom-variable-pitch-font (font-spec :family "consolas" :size 23))

;;There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
(setq doom-theme 'modus-vivendi)
;; (setq doom-theme 'doom-molokai)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))


(global-hide-mode-line-mode)

;; (set-frame-parameter (selected-frame) 'alpha '(92 . 90))

;; (add-to-list 'default-frame-alist '(alpha . (92 . 90)))


(map! :leader
      :desc "zComment"
      "z" #'+zen/toggle-fullscreen)

(add-hook 'after-init-hook 'mini-frame-mode)

(custom-set-variables
                  '(mini-frame-show-parameters
                    (lambda ()
                      `((top . 0.2)
                        (width . 0.8)
                        (left . 0.5)
                        (child-frame-border-width . 1))))
       '(mini-frame-detach-on-hide nil)
                  '(mini-frame-color-shift-step 0)
                  '(mini-frame-advice-functions '(read-from-minibuffer
                                                  ;; read-string
                                                  save-some-buffers yes-or-no-p))
                  '(mini-frame-ignore-commands '()))
;; ~/.doom.d/config.el
;; (after!
  ;; (setq lsp-haskell-formatting-provider "brittany"))
(setq +doom-dashboard-banner-file "emacs-dash.png"
      +doom-dashboard-banner-dir "/home/vamshi/.doom.d/")
