(defun raiseFirefox ()
		       (interactive )
		       (switch-to-buffer "Firefox"))


(defun run-dmenu-timeBat ()
      (interactive)
      (async-shell-command "~/aios/dmenu/dateAndTime.sh"))

(defun run-xmenu ()
      (interactive)
  (call-process-shell-command "~/aios/xmenu/xmenu.sh" nil 0))

(call-process-shell-command "brightnessctl s 1" nil 0)

(defun windowSwitcher ()
      (interactive)
  (call-process-shell-command "~/aios/dmenu/windowSwitcher.sh" nil 0))

(defun run-google ()
      (interactive)
  (call-process-shell-command "~/aios/dmenu/google" nil 0))

;;(global-set-key (kbd "s-g") 'run-google)
;;(global-set-key (kbd "s-`") 'run-xmenu)

(global-set-key (kbd "C-SPC") 'kill-buffer-and-window)
;;(global-set-key (kbd "s-o") 'windowSwitcher)
;;(global-set-key (kbd "s-f") 'raiseFirefox)
(defun dmenu ()
      (interactive)
      (async-shell-command "~/aios/dmenu/windowSwitcher.sh"))

   (setq exwm-input-global-keys
	  `(([?\s-\C-r] . exwm-reset)
	    ;;([?\s-r] . hydra-exwm-move-resize/body)
	    ([?\s-E] . (lambda () (interactive) (dired "~")))
	    ([?\s-C] . (lambda () (interactive) (kill-buffer)))
	    ([?\s-`] . run-xmenu)
	    ([?\s-j] . (lambda () (interactive) (next-buffer)))
	    ([?\s-o] . windowSwitcher)
	    ([?\s-k] . (lambda () (interactive) (previous-buffer)))
	    ([?\s-f] . find-file)
	    ;;([?\s-`] . (lambda () (interactive) (exwm-workspace-switch-create 0)))
	    ,@(mapcar (lambda (i)
			`(,(kbd (format "s-%d" i)) .
			   (lambda ()
			    (interactive)
			    (exwm-workspace-switch-create ,i))))
		       (number-sequence 0 9))))

;; (load-theme 'modus-vivendi t)



(setq inhibit-startup-message t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)

(defun reload ()
	       (interactive )
		(load-file (expand-file-name "~/.emacs.d/init.el")))

(defun privateConfig ()
             (interactive)
             (find-file (expand-file-name "~/aios/programs/emacs/README.org")) )

(column-number-mode t)
;; line numbers
(global-display-line-numbers-mode 1)
(setq display-line-numbers 'relative)
;;(setq display-line-numbers 'relative)

(dolist (mode '(org-mode-hook
                term-mode-hook
                vterm-mode-hook
                shell-mode-hook
		dired-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

(set-face-attribute 'default nil
				:font "Consolas"
				:height 200
				:weight 'regular)

(set-face-attribute 'variable-pitch nil
			:font "Consolas"
		      :height 160
		       :weight 'regular)
(set-face-attribute 'fixed-pitch nil
		      :font "Consolas"
		      :height 200
		      :weight 'regular)

  ;; Uncomment the following line if line spacing needs adjusting.
 (setq-default line-spacing 0.34)

  ;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
 ;; (add-to-list 'default-frame-alist '(font . "Consolas-17"))

(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
		    :slant 'italic)

(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

(straight-use-package 'use-package)

(use-package solaire-mode
     :straight t)
(solaire-global-mode +1)

(use-package doom-themes
     :straight t
     :ensure t
     :config
     ;; Global settings (defaults)
     (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	   doom-themes-enable-italic t) ; if nil, italics is universally disabled
     (load-theme 'doom-one t)
;; Corrects (and improves) org-mode's native fontification.
 (doom-themes-org-config)      )

(use-package doom-modeline
      :straight t
      :ensure t)

    (doom-modeline-mode 1)

(setq doom-modeline-height 23)
    (setq-default header-line-format mode-line-format)

		  (straight-use-package 'hide-mode-line)
    (global-hide-mode-line-mode)

(set-face-attribute 'mode-line nil :font "Consolas" :height 170)  
(set-face-attribute 'mode-line-inactive nil :font "Consolas" :height 170)    ;; (setq-default mode-line-format nil)

(use-package highlight-numbers
  :straight t
  :hook ((prog-mode conf-mode) . highlight-numbers-mode)
  :config (setq highlight-numbers-generic-regexp "\\_<[[:digit:]]+\\(?:\\.[0-9]*\\)?\\_>"))

(use-package consult
  :straight t)

;; magit
(use-package magit
      :straight t)

(straight-use-package 'selectrum)
(selectrum-mode +1)
;;(setq selectrum-display-style horizontal)

(use-package mini-frame
  :straight t)

(add-hook 'after-init-hook 'mini-frame-mode)

(custom-set-variables
                  '(mini-frame-show-parameters
                    (lambda ()
                      `((top . 0.2)
                        (width . 0.8)
                        (left . 0.5)
                        (child-frame-border-width . 1))))
       '(mini-frame-detach-on-hide nil)
                  '(mini-frame-color-shift-step 0)
                  '(mini-frame-advice-functions '(read-from-minibuffer
                                                  ;; read-string
                                                  save-some-buffers yes-or-no-p))
                  '(mini-frame-ignore-commands '()))

(use-package orderless
  :straight t
  :ensure t
  :custom (completion-styles '(orderless)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (setq-default cursor-type 'bar) 
	(setq ryo-modal-cursor-color "pink")
	 ;;(global-set-key (kbd "<escape>")  'ryo-modal-mode)
	  (defun kakoune-a (count)
	    "Select COUNT lines from the current line.
	  Note that kakoune's x does behaves exactly like this,
	  and I like this behavior better."
	    (interactive "p")
	    (set-mark (point))
	    (forward-char count)
	    (ryo-modal-mode 0))

	  (defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
		 (interactive)
		 (unless (use-region-p) (set-mark (point))))

	  (defun kakoune-deactivate-mark ()
	    "Deactivate the mark.
	  Deactivate the mark unless mark-region-mode is active."
	    (interactive)
	    ;;(unless rectangle-mark-mode (deactivate-mark)))
	    (deactivate-mark))

  (defun kakoune-O (count)
    "Open COUNT lines above the cursor and go into insert mode."
    (interactive "p")
    (beginning-of-line)
    (dotimes (_ count)
      (newline)
      (forward-line -1)))

	  (defun kakoune-x (count)
	    "Select COUNT lines from the current line.
	  Note that kakoune's x does behaves exactly like this,
	  and I like this behavior better."
	    (interactive "p")
	    (beginning-of-line count)
	    (set-mark (point))
	    (end-of-line count)
	    (forward-char count))

	  (defun kakoune-d (count)
	    "Kill selected text or COUNT chars."
	    (interactive "p")
	    (if (use-region-p)
		(kill-region (region-beginning) (region-end))
	      (delete-char count t)))

  (defun kakoune-insert-mode () "Return to insert mode."
		 (interactive)
		 (ryo-modal-mode 0))

  (defun kakoune-normal-mode () "Return to insert mode."
		 (interactive)
		 (ryo-modal-mode 1))

	  (defun ryo-after () "Enter normal mode"
		 (interactive)
		    (forward-char)
		    (ryo-modal-mode 0))

    (defun kakoune-X (count)
      "Extend COUNT lines from the current line."
      (interactive "p")
      (beginning-of-line)
      (unless (use-region-p) (set-mark (point)))
      (forward-line count))

    (defun kakoune-T (count)
      "Extend COUNT lines from the current line."
      (interactive "p")
      (beginning-of-line)
      (unless (use-region-p) (set-mark (point)))
      (previous-line count))

    (defun kakoune-gg (count)
	    "Go to the beginning of the buffer or the COUNTth line."
	    (interactive "p")
	    (goto-char (point-min))
	    (when count (forward-line (1- count))))

	  (defun kakoune-o (count)
	    "Open COUNT lines under the cursor and go into insert mode."
	    (interactive "p")
	    (end-of-line)
	    (dotimes (_ count)
	      (electric-newline-and-maybe-indent))
	    (ryo-modal-mode 0))

	  (defun kakoune-set-mark-here () "Set the mark at the location of the point."
		 (interactive) (set-mark (point)))

	  (defun kakoune-p (count)
	    "Yank COUNT times after the point."
	    (interactive "p")
	    (forward-line)
	    (dotimes (_ count) (save-excursion (yank))))

	  (defun kakoune-P (count)
	    "Yank COUNT times after the point."
	    (interactive "p")
	    (dotimes (_ count) (save-excursion (yank))))

  (defvar kakoune-last-t-or-f ?f
    "Using t or f command sets this variable.")

  (defun kakoune-select-to-char (arg char)
    "Select up to, and including ARGth occurrence of CHAR.
  Case is ignored if `case-fold-search' is non-nil in the current buffer.
  Goes backward if ARG is negative; error if CHAR not found.
  Ignores CHAR at point."
    (interactive "p\ncSelect to char: ")
    (setq kakoune-last-char-selected-to char)
    (setq kakoune-last-t-or-f ?f)
    (let ((direction (if (>= arg 0) 1 -1)))
      (forward-char direction)
      (unwind-protect
	      (search-forward (char-to-string char) nil nil arg))
      (point)))

  (defun kakoune-select-up-to-char (arg char)
    "Select up to, but not including ARGth occurrence of CHAR.
  Case is ignored if `case-fold-search' is non-nil in the current buffer.
  Goes backward if ARG is negative; error if CHAR not found.
  Ignores CHAR at point."
    (interactive "p\ncSelect up to char: ")
    (setq kakoune-last-char-selected-to char)
    (setq kakoune-last-t-or-f ?t)
    (let ((direction (if (>= arg 0) 1 -1)))
      (forward-char direction)
      (unwind-protect
	      (search-forward (char-to-string char) nil nil arg)
	    (backward-char direction))
      (point)))

	  (defun kakoune-A (count)
	    "Yank COUNT times after the point."
	    (interactive "p")
	    (end-of-line)
	    (ryo-modal-mode 0))

	  (use-package expand-region
	    :straight t
	    :bind ("C-+" . er/expand-region))

	  (use-package ryo-modal
	    :straight t
	    :commands ryo-modal-mode
	    ;;:bind ("C-a" . ryo-modal-mode)
	    :config
	    (ryo-modal-keys
	     (";" kakoune-deactivate-mark)
	     ("%" mark-whole-buffer)
	     ("," ryo-modal-repeat)
	     ("/" consult-line)
	     ("a" ryo-after)
	     ("b" backward-word :first '(kakoune-set-mark-here ))
	     ("c" comment-line)
	     ("d" kakoune-d)
	     ("f" kakoune-select-to-char :first '(kakoune-set-mark-here ))
	     ("h" backward-char :first '(kakoune-deactivate-mark))
	     ("i" kakoune-insert-mode)
	     ("j" next-line :first '(kakoune-deactivate-mark ))
	     ("k" previous-line :first '(kakoune-deactivate-mark))
	     ("l" forward-char :first '(kakoune-deactivate-mark  ))
	     ("o" kakoune-o)
	     ("p" kakoune-p)
	     ("q" ryo-modal-mode)
	     ("s" save-buffer)
	     ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here ))
	     ("w" forward-word :first '(kakoune-set-mark-here))
	     ("x" kakoune-x)
	     ("y" kill-ring-save)
	     ("A" kakoune-A)
	     ("B" backward-word :first '(kakoune-set-mark-if-inactive))
	     ("H" backward-char :first '(kakoune-set-mark-if-inactive))
	     ("J" next-line :first '(kakoune-set-mark-if-inactive))
	     ("K" previous-line :first '(kakoune-set-mark-if-inactive))
	     ("L" forward-char :first '(kakoune-set-mark-if-inactive))
	     ("M-w" forward-symbol :first '(kakoune-set-mark-here))
	     ("O" kakoune-O)
	     ("P" kakoune-P)
	     ("T" kakoune-T)
	     ("W" forward-word :first '(kakoune-set-mark-if-inactive))
	     ("X" kakoune-X)
	  )

	    (ryo-modal-keys
	     ;; First argument to ryo-modal-keys may be a list of keywords.
	     ;; These keywords will be applied to all keybindings.
	     (:norepeat t)
	     ("0" "M-0")
	     ("1" "M-1")
	     ("2" "M-2")
	     ("3" "M-3")
	     ("4" "M-4")
	     ("5" "M-5")
	     ("6" "M-6")
	     ("7" "M-7")
	     ("8" "M-8")
	     ("7" "M-7")
	     ("9" "M-9")))

	  ;; general

	  (ryo-modal-key
	   "SPC" '(("s" save-buffer)
		   ("g" magit-status)
		   ("a" consult-line)
		   ("t" consult-theme)
		   ("SPC" execute-extended-command)
		   ;; ("j" next-buffer)
		   ;; ("k" previous-buffer)
		   ("b" ibuffer-list-buffers)
		   ("q" kill-buffer-and-window)
		   ("f" find-file)
		   ("r" reload)))

	  (ryo-modal-key
	   "g" '(("h" beginning-of-line)
		   ("j" end-of-buffer)
		   ("g" kakoune-gg)
		   ("l" end-of-line)
		   ("k" beginning-of-buffer)))


(global-set-key (kbd "<escape>") 'kakoune-normal-mode)
      (add-hook 'prog-mode-hook #'kakoune-insert-mode)

(use-package marginalia
    :straight t
  :config
  (marginalia-mode))

(use-package embark
  :straight t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)
   ("C-j" . embark-become)))

(use-package undo-tree
:straight t
:config
(global-undo-tree-mode)
:ryo
("u" undo-tree-undo)
("U" undo-tree-redo)
("SPC u" undo-tree-visualize)
:bind (:map undo-tree-visualizer-mode-map
            ("h" . undo-tree-visualize-switch-branch-left)
            ("j" . undo-tree-visualize-redo)
            ("k" . undo-tree-visualize-undo)
            ("l" . undo-tree-visualize-switch-branch-right)))

(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")

(use-package vterm-toggle
    :straight t)

(use-package org-tempo
  :ensure nil)

;;(add-hook 'org-mode-hook 'org-indent-mode)

(setq org-ellipsis "⤵")

;;(use-package org-superstar
      ;; :straight t)
;;(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(use-package haskell-mode
        :straight t)
;;        :config
;;;;        (load "haskell-mode-autoloads"))
    (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
         (setq  haskell-interactive-popup-errors nil)

;;(use-package emms
;;      :straight t)
  ;;(emms-all)
;;  (emms-default-players)
;;  (emms-mode-line 1)
;;  (emms-playing-time 1)
;;  (setq emms-source-file-default-directory "~/aios/music/"
;;        emms-playlist-buffer-name "*Music*"
;;        emms-info-asynchronously t
;;        emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)

;;(use-package company
  ;;  :straight t)
 ;; (global-company-mode)

(use-package company
  :straight t
  :ensure t
  :config
  (add-to-list 'company-backends 'company-capf)
  (global-company-mode))
 ;;(setq ac-ignore-case nil)


