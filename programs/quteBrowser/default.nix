{ pkgs, config, lib, inputs, ... }:{
  programs.qutebrowser = {
    enable= true;
    extraConfig = "
# Default font families to use. Whenever \"default_family\" is used in a
# font setting, it's replaced with the fonts listed here. If set to an
# empty value, a system-specific monospace default is used.
# Type: List of Font, or Font
c.fonts.default_family = '\"SauceCodePro Nerd Font\"'

# Default font size to use. Whenever \"default_size\" is used in a font
# setting, it's replaced with the size listed here. Valid values are
# either a float value with a \"pt\" suffix, or an integer value with a
# \"px\" suffix.
# Type: String
c.fonts.default_size = '11pt'

# Font used in the completion widget.
# Type: Font
c.fonts.completion.entry = '11pt \"SauceCodePro Nerd Font\"'

# Font used for the debugging console.
# Type: Font
c.fonts.debug_console = '11pt \"SauceCodePro Nerd Font\"'

# Font used for prompts.
# Type: Font
c.fonts.prompts = 'default_size sans-serif'

# Font used in the statusbar.
# Type: Font
c.fonts.statusbar = '11pt \"SauceCodePro Nerd Font\"'
# Type: Dict
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}', 'am': 'https://www.amazon.com/s?k={}', 'aw': 'https://wiki.archlinux.org/?search={}', 'goog': 'https://www.google.com/search?q={}', 'hoog': 'https://hoogle.haskell.org/?hoogle={}', 're': 'https://www.reddit.com/r/{}', 'ub': 'https://www.urbandictionary.com/define.php?term={}', 'wiki': 'https://en.wikipedia.org/wiki/{}', 'yt': 'https://www.youtube.com/results?search_query={}'}



# Bindings for normal mode
config.bind('M', 'hint links spawn mpv {hint-url}')

config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')

";
  };
}
