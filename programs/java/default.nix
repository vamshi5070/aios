{ pkgs, config, lib, inputs, ... }:{
   programs.java = {
      enable = true;
      package = pkgs.openjdk8;
   };
}
