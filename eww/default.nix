{config,lib,pkgs,...}:
{
  # Widgets
    programs.eww = {
        enable = true;
        configDir = ./eww ;
    };
}
