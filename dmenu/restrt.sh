#!/bin/sh

choice=$(echo -e "No\nYes" |  dmenu -fn "LucidaMAC:size=30" -p "Restart .., Really??" -i)

echo $choice

if [[ $choice = "Yes" ]]; then
    reboot
fi
