#!/bin/sh

choice=$(echo -e "No\nYes" |  dmenu -fn "LucidaMAC:size=30" -p "Shutdown.., Really??" -i)

echo $choice

if [[ $choice = "Yes" ]]; then
    poweroff
fi
