#!/bin/sh

daTe=$(date +"%H:%M")
BATTERY_POWER=`cat /sys/class/power_supply/BAT0/capacity`


battery_prompt () {
    BATTERY_POWER=`cat /sys/class/power_supply/BAT0/capacity`
    [ $BATTERY_POWER -eq 100 ] \
	&& echo -e "$daTe \n $BATTERY_POWER%" | dmenu -fn "LucidaMAC:size=50" -l 7
    [ $BATTERY_POWER -lt 99 ] && [ $BATTERY_POWER -ge 15 ] \
	&& choice=$(echo -e "$daTe | $BATTERY_POWER%\nblutooth\nemacs" | dmenu -fn "LucidaMAC:size=50" -l 7 -n)
    [ $BATTERY_POWER -lt 15 ] \
	&& choice=$(echo -e "$daTe | $BATTERY_POWER%\nblutooth\nemacs" | dmenu -fn "LucidaMAC:size=50" -l 7 -n -sb "red")

    case $choice in
	blutooth)
	    pavucontrol ; alacritty -e bluetoothctl
	    ;;
	emacs)
	    emacsclient -c -a emacs
	    ;;
    esac
}
battery_prompt
