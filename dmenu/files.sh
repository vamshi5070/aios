#!/bin/sh

mvRes="/home/vamshi/"
home="/home/vamshi/"

#mvTo () {
apps="0 - Nothing\n1 - Emacs\n2 - Zathura\n3 - Move to other Directory\n4 - mpv\n5 - Just run\n6 - Kakoune\n7 - Copy to other Directory"
getFilePath () {
flag=1
while [ $flag -eq 1 ]
do
res=$(echo -e "MV\n$(ls -aF $mvRes)" | dmenu -fn "Consolas:size=23" -sf "black" -sb "white" -nb "black" -nf "white" -i -l 9 -p $mvRes -n)
case $res in
    ./)
        #mvRes=$mvRes
        echo "."
        ;;
    ../)
        mvRes=$(echo -e $mvRes | rev | cut --complement -d '/' -f 2 | rev)
        echo ".."
        ;;
    "")
        exit 1
        ;;
    "MV")
        #mvRes=$
        flag=0
        echo "mv"
        echo $mvRes
        ;;
    *)
       	mvRes="${mvRes}${res}"
	if [ -d $mvRes ]; then
    		echo "1"
	else
    		exit 1
	fi

        # echo "default"
        ;;
esac
done
}
# [ -d "/path/dir/" ]
functio () {
dir=$1
res=$(ls -aF $dir | dmenu -fn "Consolas:size=23" -sf "black" -sb "white" -nb "black" -nf "white" -i -l 100 -p $dir  -n )
case $res in
    ./)
        resCat=$dir
        echo "1"
        ;;
    ../)
        resCat=$(echo -e $dir | rev | cut --complement -d '/' -f 2 | rev)
        echo "2"
        ;;
    "")
        exit 1
        ;;

    *)
        resCat="${dir}${res}"
        # echo "default"
        ;;
esac
# if [[ $res = *[!\ ]* ]]; then
#     exit 1
# fi
if [ -d $resCat ]; then
    functio $resCat
    # echo "'$res' "
else
    choice=$(echo -e $apps | dmenu -fn "Iosevka Nerd Font:size=23" -sf "black" -sb "#f0f0f0" -nb "black" -nf "white" -i -p $resCat -l 14 -n | cut -b 1)
    # $choice $resCat
case $choice in
    0)
        #resCat=$dir
        exit 1
        ;;
    1)
        emacsclient -c -a emacs $resCat
        ;;
    2)
        zathura $resCat
        ;;
    3)
        getFilePath
        mv  $resCat  $mvRes
        ;;
    4)
	mpv $resCat
        ;;
    5)
        $resCat
        ;;
    6)
        alacritty -e kak $resCat
        ;;
    7)
        getFilePath
        cp $resCat  $mvRes
        ;;
esac
fi
}
functio $home

#getFilePath
#echo -e $mvRes

