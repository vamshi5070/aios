#!/bin/sh

dat=$(date +'%d/%m/%Y')
time=$(date +'%H:%M')
day=$(date +'%a')
BATTERY_POWER=`cat /sys/class/power_supply/BAT0/capacity`
battery_prompt () {
    BATTERY_POWER=`cat /sys/class/power_supply/BAT0/capacity`
    [ $BATTERY_POWER -eq 100 ] \
        && echo "Full ,$BATTERY_POWER% "
    [ $BATTERY_POWER -lt 99 ] && [ $BATTERY_POWER -ge 15 ] \
        && echo "Norm, $BATTERY_POWER% "
    [ $BATTERY_POWER -lt 15 ] \
        && echo "Low, $BATTERY_POWER% "
}
xmenu -i -p 12x12:0 <<EOF |  sh &
`battery_prompt`
$day, $time
	$dat

Applications
	Firefox		firefox
	Terminal	alacritty
Emacs	
	emacs		emacsclient -c -a emacs
	restart		systemctl --user restart emacs.service
Bluetooth	pavucontrol ; alacritty -e bluetoothctl
search
	google		/home/vamshi/aios/dmenu/google
	youtube		/home/vamshi/aios/dmenu/youtube
Redshift
	urestart	sudo nixos-rebuild switch ; systemctl --user restart redshift.service
	restart		systemctl --user restart redshift.service
	open		alacritty -e sudo kak /etc/nixos/redshift/default.nix
	stop		systemctl --user stop redshift.service
XMonad	
	restart		xmonad --recompile ; xmonad --restart
	doc		firefox https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Doc-Extending.html
Nixos
	homeManager	firefox https://rycee.gitlab.io/home-manager/options.html
	store		/home/vamshi/aios/dmenu/nixos
XMenu	alacritty -e kak ~/aios/xmenu/xmenu.sh
Git
	vamshi5070	firefox "gitlab.com/vamshi5070"
	dt		firefox "gitlab.com/dwt1/dotfiles"
Exit
	shutdown	~/aios/dmenu/shutdwn.sh
	restart		~/aios/dmenu/restrt.sh
EOF
