{ config, lib,pkgs, ... }:{

# with lib;
# let
  # cfg = config.services.spotif;
services.spotifyd.enable = true;
home.packages = with pkgs; [
  spotify
];
}
