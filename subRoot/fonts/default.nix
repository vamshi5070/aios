{ config, pkgs, ... }:

{

fonts.fonts = with pkgs; [
  (nerdfonts.override { fonts = [ "CascadiaCode" "Hasklig" "JetBrainsMono" "Mononoki" "SourceCodePro" ]; })
  emacs-all-the-icons-fonts
  vistafonts
  hack-font
 # fira-code
 # fira-code-symbols
  font-awesome-ttf
  roboto
 # ubuntu_font_family
];
}
