{  config, lib, pkgs, ... }:{
 location = {
                       latitude = 17.492829;
                       longitude = 78.281284;
  };
  services.redshift = {
    enable = true;
    #latitude = 17.5;
    #longitude = 78.3;
    brightness.day = "0.5";
    brightness.night = "0.4";
    extraOptions = ["-g" "0.8:0.8:0.8" ];
    #settings = {
    #  redshift = {
    #    gamma = "0.8";
    #  };
    #};
    temperature = {
      day = 3400;
      night = 3400;
    };
  };
}
