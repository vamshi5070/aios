{ config, lib, pkgs, ... }:
{
  home.packages = with pkgs; [
      libnotify
  ];

  services.notify-osd= {
    enable = true;
  };
}
