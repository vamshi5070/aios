{  config, lib, pkgs, ... }:{
  # home.username = "vamshi";
  # home.homeDirectory = "/home/vamshi";
  services.random-background = {
  	enable = true;
		imageDirectory = "/home/vamshi/aios/services/randomBg/pictures";
		interval = "59m";
  };
}
