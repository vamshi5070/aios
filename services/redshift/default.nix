{  config, lib, pkgs, ... }:{
  services.redshift = {
    enable = true;
    latitude = 17.5;
    longitude = 78.3;
    settings = {
      redshift = {
        brightness-day = "0.5";
        brightness-night = "0.5";
        gamma = "0.8";
      };
    };
    temperature = {
      day = 3400;
      night = 3400;
    }; 
  };
}
