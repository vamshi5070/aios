{ config, lib, pkgs, ... }:
{
  services.sxhkd = {
  	enable = true;
  	extraConfig = "
	# quit/restart bspwm
super + shift + {q,r}
        bspc {quit,wm -r}

# close and kill
super + shift + c
        bspc node -c

super + Return
    alacritty
super + d
	dmenu_run
super + shift + Return
	~/aios/menu.sh
# set the window state
super + {t,shift + t,s,f}
        bspc node -t {tiled,pseudo_tiled,floating,fullscreen}
	# make sxhkd reload its configuration files:
super + Escape
        pkill -USR1 -x sxhkd
  	";
  };
}
