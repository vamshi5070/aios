{ pkgs, ... }:
{
  programs.mpv = {
   enable = true ;
 };
  home.packages = with pkgs; [
    kdenlive
    simplescreenrecorder
    audacity
    playerctl
    youtube-dl
    vlc
    # obs-studio
  ];
}
