{  config, lib, pkgs, ... }:{

wayland.windowManager.sway = { 
        enable = true;
        config = {
          bars = [];
          fonts = {
  names = [ "Lucida MAC" "FontAwesome5Free" ];
  style = "Normal";
  size = 13.0;
};
          input = {
            Touchpad = {
              tap = "enabled";
              natural_scroll  = "enabled";
                              };
                  };
          keybindings = let 
                         modifier = config.wayland.windowManager.sway.config.modifier;
in lib.mkOptionDefault {
  #"${modifier}+Return" = "exec ${pkgs.rxvt-unicode-unwrapped}/bin/urxvt";
  "${modifier}+q" = "kill";
  "${modifier}+d" = "exec dmenu_run -fn 'Lucida MAC:size=27'";
  "${modifier}+e" = "exec emacsclient -c -a emacs";
  "${modifier}+g" = "exec ~/aios/dmenu/input";

  # "${modifier}+"
 # "${modifier}+d" = "exec ${pkgs.dmenu}/bin/dmenu_path | ${pkgs.dmenu}/bin/dmenu | ${pkgs.findutils}/bin/xargs swaymsg exec --";
};
                modifier = "Mod4";
          output = { eDP-1 = {bg = "~/aios/services/randomBg/pictures/leninwall.png fill" ;};};
          startup = [
            { command = "waybar"; }
            { command = "brightnessctl s 1"; }
            { command = "systemctl --user restart gammastep.service"; }
          ];
          terminal = "alacritty";
        };
    extraConfig = "

                          # Set shut down, restart and locking features
bindsym Mod4+0 mode '$mode_system'
set $mode_system (l)ock, (e)xit, switch_(u)ser, (s)uspend, (h)ibernate, (r)eboot, (Shift+s)hutdown
mode '$mode_system' {
    bindsym l exec alacritty, mode 'default'
    bindsym s exec firefox, mode 'default'
    bindsym u exec qutebrowser, mode 'default'
    bindsym e exec --no-startup-id i3exit logout, mode 'default'
    bindsym h exec --no-startup-id i3exit hibernate, mode 'default'
    bindsym r exec --no-startup-id i3exit reboot, mode 'default'
    bindsym Shift+s exec --no-startup-id i3exit shutdown, mode 'default'

    # exit system mode: 'Enter' or 'Escape'
    bindsym Return mode 'default'
    bindsym Escape mode 'default'
}

";

	};
}
