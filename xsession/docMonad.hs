import XMonad hiding ( (|||) )
import qualified XMonad.StackSet as W
-- import System.IO ()
import System.Exit
import System.IO (hPutStrLn,hClose, hFlush, Handle)

import Data.Tree
import qualified Data.Map as M
import Data.Maybe (isJust,isNothing)

import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Actions.CycleRecentWS
import XMonad.Actions.CycleSelectedLayouts
import XMonad.Actions.CycleWindows
import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Actions.GridSelect
import XMonad.Actions.GroupNavigation
import XMonad.Actions.Minimize
import XMonad.Actions.MouseResize
import XMonad.Actions.Navigation2D
import XMonad.Actions.OnScreen
import XMonad.Actions.Plane
import XMonad.Actions.RotSlaves
import XMonad.Actions.Search
import XMonad.Actions.ShowText
import XMonad.Actions.Submap
import qualified XMonad.Actions.TreeSelect as TS
import XMonad.Actions.WindowGo
import XMonad.Actions.WithAll

import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ServerMode
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen,doCenterFloat,doFullFloat)
import XMonad.Hooks.Minimize
import XMonad.Hooks.ScreenCorners
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

import XMonad.Layout.Accordion
import qualified XMonad.Layout.BoringWindows as BW
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.LimitWindows
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.Minimize
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Prompt
import XMonad.Prompt.AppendFile
import XMonad.Prompt.AppLauncher as AL
import XMonad.Prompt.Input
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Window
import XMonad.Prompt.XMonad

import XMonad.Util.EZConfig
import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.NoTaskbar
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.Scratchpad

import Codec.Binary.UTF8.String (encode)
import Foreign.C.Types (CInt)

myTerminal = "kitty" --"alacritty"
myEditor = "emacsclient -c -a '' "  -- Sets emacs as editor 
myBrowser =  "firefox" --""
myMusic = " spotify"
myVidEdit = "flatpak run org.kde.kdenlive"
myScreenRec = "flatpak run com.obsproject.Studio"
myFont = "xft:Lucida MAC:regular:size=10:antialias=true:hinting=true"
myTreeFont = "xft:Lucida MAC:regular:size=17:antialias=true:hinting=true"
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myClickJustFocuses :: Bool
myClickJustFocuses = False
myBorderWidth   = 2
windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
myModMask       = mod4Mask
-- myWorkspaces    = ["origin","music","read","obs","empty","mpv","editor"]
myNormColor   = "#282c34"   -- Border color of normal windows
myFocusColor  = "#46d9ff"   -- Border color of focused windows
myUpdate = "home-manager switch --flake \"/home/vamshi/aios#cosmos\" && notify-send 'Nixos updated'"

myModifier = "M-"

myStartupHook :: X ()
myStartupHook = do
            --spawnOnce "feh --randomize --bg-fill ~/aios/services/randomBg/pictures/*"  -- feh set random wallpaper"
            --spawnOnce myEditor
            --spawnOnce "nitrogen --restore"
  spawn "systemctl --user restart taffybar.service"
            --spawnOnce "setxkbmap -option caps:swapescape"
            -- spawnOnce "systemctl --user start redshift.service"
  spawnOnce "brightnessctl s 1"
            --spawnOnce $ "alttab -w 1 -d 1 -font \"xft:Lucida MAC-17\" -t 300x200 -i 277x114 -frame "   ++ myFocusColor  ++  " -fg " ++ myFocusColor ++ " & "
            --spawnOnce "systemctl --user start emacs.service"
   -- addScreenCorner SCUpperRight (goToSelected defaultGSConfig { gs_cellwidth = 200})
   -- addScreenCorners [ (SCLowerRight, nextWS)
   --                   , (SCLowerLeft,  prevWS)
   --                   ]
  -- spawnOnce "conky -c \"~/aios/services/conky/conkyrc \""
  setWMName "LG3D"
  makeGrid3 helpList
  addScreenCorner SCLowerLeft $ sendMessage ToggleStruts-- sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts --(goToSelected defaultGSConfig { gs_cellwidth = 200})
  addScreenCorner SCUpperLeft $ makeGrid1 desktopList
  addScreenCorner SCUpperRight $ nextMatch Backward (fmap not (className =? "Alacritty"))
  -- addScreenCorner SCUpperRight $ moveTo Next nonEmptyNonNSP
  addScreenCorner SCLowerRight $ sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts

shellXPConfig = greenXPConfig {
  autoComplete      = Nothing--Just 100000    -- set Just 100000 for .1 sec
  , height            = 80
  ,promptKeymap = vimLikeXPKeymap
  ,  position =  CenteredAt {xpCenterY = 0.19 , xpWidth = 0.88}
  ,  font = "xft:Lucida MAC:size=22"
  ,defaultPrompter = const ">  "
  ,fgColor = myFocusColor
  }
emacsXPConfig = shellXPConfig {
  defaultPrompter = const "Emacs: "
                    -- ,defaultText = "emacsclient -c -a emacs "
  }
zathuraXPConfig = shellXPConfig {
  defaultPrompter = const "Zathura: "
  }

googleXPConfig = greenXPConfig {
  fgColor = "white"--myFocusColor
  ,  position =  CenteredAt {xpCenterY = 0.29 , xpWidth = 0.48}
  ,  font = "xft:Lucida MAC:size=22"
  , height            = 80
  }

youtubeXPConfig = googleXPConfig {
  fgColor = "red"--myFocusColor
  }

keyXPConfig = googleXPConfig {
  autoComplete = Just 100000
  , fgColor = myFocusColor
  , borderColor = myFocusColor
  ,defaultPrompter =  const "Emacs: "
  }

nixosPkgs =  searchEngine "Nixos" "https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&query="

decSound query = spawn $ "amixer set Master " <> query <> "%- unmute"
incSound query = spawn $ "amixer set Master " <> query <> "%+ unmute"

arcMenu = xmonadPromptC emacslist keyXPConfig {defaultPrompter = const "xmonad: "}

helpList = [
  ("session started",return ())
           ]
mainList  = [
                  --("g" ,"google", shellPrompt shellXPConfig{defaultText = "firefox 'https://www.google.com/search?client=firefox-b-d&q="} )-- promptSearch greenXPConfig{position=Top,height = 45,font="xft:Lucida MAC:size=23"} google)--spawn "~/vc/firefox/google.sh")

  ("p", "nothing", return ())
  ,("/","help",makeGrid3 helpList)
  ,("q","nothing",return ())
  ,("e","send and view on empty",tagToEmptyWorkspace)
  ,("d","date",do
       spawn "notify-send \"`date`\" "
       spawn "cbatticon"
   )
  ,("g" ,"google", promptSearchBrowser googleXPConfig myBrowser google)--spawn "~/vc/firefox/google.sh")
  ,("=","incLimit", increaseLimit)
  ,("-","decLimit", decreaseLimit)
  --,("S-<Return>", "win Prompt",windowMultiPrompt shellXPConfig { autoComplete = Just 50 } [(Goto, allWindows), (Goto, wsWindows)])
  ,("y" ,"youtube", promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")
                  -- ,("y" ,"youtube", shellPrompt shellXPConfig{defaultText = "firefox 'https://www.youtube.com/results?search_query="} )-- promptSearch greenXPConfig{position=Top,height = 45,font="xft:Lucida MAC:size=23"} google)--spawn "~/vc/firefox/google.sh")
                  --,("y" ,"google", promptSearch greenXPConfig{position=Top,height = 45,font="xft:Lucida MAC:size=23"} youtube)--spawn "~/vc/firefox/google.sh")
                  --,("d" ,"youtube",spawn "~/vc/firefox/youtube.sh")
                  -- ,("x","exit",makeGrid2 $ fillter exitList)
                  -- ,("M1-<Space> t", "terminal",namedScratchpadAction myScratchPads "terminal")
  ,("S-e","treeselect",vamTree)
  ,("S-l","layout",   cycleThroughLayouts ["Tall", "Grid"])
  ,(";", "gnu emacs",arcMenu)
  ,("M1-m", "minimize",withFocused minimizeWindow)
  ,("M1-l","maximize",withLastMinimized maximizeWindowAndFocus)
    -- ,("<Tab>", "traverse windows",rotAllUp)--windows W.focusDown)

                -- ,("<Space>","overview",makeGrid1 gridList)
  , ("n", "notes", appendFilePrompt shellXPConfig {defaultPrompter = const "Notes: " } "/home/vamshi/notes/notes")
  -- , ("M-C-1", "view on screen 1" , windows (viewOnScreen 0 "1"))
  -- ,("=", "dec spacing", decWindowSpacing 4)         -- Decrease window spacing
  -- , ("-","inc spacing", incWindowSpacing 4)
    -- Increase window spacing
  , ("S-=","dec screen spacing", decScreenSpacing 4)         -- Decrease screen spacing
  , ("S--", "inc screen spacing", incScreenSpacing 4)         -- Increase screen spacing
                  --,("S-<Return>  g" ,"google",spawn "~/vc/firefox/google.sh")
                  --,("S-<Return> d" ,"youtube",spawn "~/vc/firefox/youtube.sh")
                  --,("S-<Return> x","exit",makeGrid2 exitList)
                  -- ,("S-<Return> t", "terminal",namedScratchpadAction myScratchPads "terminal")
                  --,("S-<Return> w m", "minimize",withFocused minimizeWindow)
                  --,("S-<Return> w l","maximize",withLastMinimized maximizeWindowAndFocus)
  ,("<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
  ,("o", "shell prompt",shellPrompt shellXPConfig)
  ,("<Backspace>" , "kill one",kill)
  ,("S-<Backspace>" , "kill all",killAll)
  --,("M1-S-]", "moveto next nonNSP", spawn "firefox" >> spawn "sleep 3" >> shiftTo Next EmptyWS )
    -- ,("M-f f","emacs", makeGrid2 xmonadList)
  ,("<Space>", "fullscreen",sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  ,("2" , "browser", raiseNextMaybe (xmonadPromptC webList keyXPConfig) (className =? "Firefox")) -- Firefox
                    -- ,("M-h" , "firefox", ifWindows (className =? "Firefox") (killWindow ) return ())
                            -- ,("M-<F3>" , "firefox"	, raiseNextMaybe (className =? "Firefox"))
  ,("1" ,"emacs", raiseNextMaybe (AL.launchApp emacsXPConfig "emacsclient -c -a emacs") (className =? "Emacs"))
  ,("3" ,"zathura", raiseNextMaybe (AL.launchApp zathuraXPConfig "zathura") (className =? "Zathura"))
  ,("s","screenshot",spawn "scrot")
  , ("`", "treeWork",TS.treeselectWorkspace myTsConfig myWorkspaces W.view)
  ,("S-`", "treeWork shift",TS.treeselectWorkspace myTsConfig myWorkspaces W.shift)
  ,("C-`", "treeWork shift and focus",TS.treeselectWorkspace myTsConfig myWorkspaces (\i -> W.greedyView i . W.shift i))
  -- , ("e","xmessage",xmessage)

  ,("S-<Space>", "toggle xmobar",sendMessage ToggleStruts)         -- Toggles struts
 -- ,("S-<Return>", "run launcher",runOrRaisePrompt shellXPConfig)         -- Toggles struts
  ,("S-<Return>", "run launcher",spawn "dmenu_run")--runOrRaisePrompt shellXPConfig)         -- Toggles struts
  ,("w","plane Moves",planeMove (Lines 1) Circular ToUp)
  -- ,("S-[", "only prevWs", moveTo Prev emptyNonNSP)
  -- ,("S-]", "only nextWs", moveTo Next emptyNonNSP)
  --,("n n", "only nextWs",nextWS)
  --,("n p", "only prevWs",prevWS)
  -- ,("]", "next non empty",moveTo Next nonEmptyNonNSP)
  -- ,("[",  "Prev non empty",moveTo Prev nonEmptyNonNSP)
  ,("u","display",flashText def 1 "->" )-- >> nextWS)
  -- ,("kill","k",kill)
                  -- ,("<Space>","menu",makeGrid1 gridList)
  ]

  --haskell https://www.google.com/search?client=firefox-b-d&q=
emacsList = [
  ("c c","emacs", AL.launchApp emacsXPConfig "emacsclient -c -a emacs")
  ,("c b", "ibuffer",spawn "emacsclient -c -a '' --eval '(ibuffer)'")
  ,("c d","dired",spawn "emacsclient -c -a '' --eval '(dired nil)'")
              -- ,("c c" ,"emacs",spawn $ "emacsclient -c -a ''")
              -- ,("open",spawn $ "sudo " ++ myEditor ++ " /etc/nixos/programs/emacs/doom.d/" )
  ,("c m", "vterm",spawn "emacsclient -c -a '' --eval '(vterm)'")]

keyConfig str = keyXPConfig {defaultPrompter = const str}
   --              ,("c b", "ibuffer",)
emacslist = [
               -- ,("k kill",kill)
               --, ("r redshift",xmonadPromptC redList keyXPConfig {defaultPrompter = const "Redshift: "})
  ("b bluetooth", spawn $ "pavucontrol ; " <>  myTerminal <> " -e bluetoothctl")
  ,("x xmonad",xmonadPromptC xmonadList $ keyConfig "XMonad: ")
  ,("d delete",xmonadPromptC deleteList $ keyConfig "Delete: ")
               -- , ("e emacs ",spawn myEditor)
  ,("e emacs",AL.launchApp emacsXPConfig "emacsclient -c -a emacs")
  -- ,("e emacs" , raiseNextMaybe (spawn "emacsclient -c -a 'emacs' ") (className =? "Emacs"))
  ,("c firefox" ,  raiseNext (className =? "Firefox"))
  -- ,("r repl", xmonadPromptC replList $ keyConfig "Repl: ")
  ,("r redshift",xmonadPromptC redList $ keyConfig "Redshift: ")
  -- , ("g google", promptSearchBrowser googleXPConfig myBrowser google)--spawn"~/vc/firefox/google.sh")
  ,("y youtube", promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")
  ,("f fullscreen", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)
  ,("z zathura",AL.launchApp zathuraXPConfig "zathura")
  ]

superList = [
  -- , ("M1-C-x","gnu emacs",xmonadPromptC emacslist keyXPConfig)
  -- ,("M-<Right>", "next non empty",moveTo Next NonEmptyWS)
  -- ,("M-<Left>", "prev non empty",moveTo Prev NonEmptyWS)
  -- ,("M-S-<Right>", "next empty",moveTo Next EmptyWS)
  -- ,("M-S-<Right>", "next empty",moveTo Next EmptyWS)
  --,("M-=",  "inc limit",increaseLimit)
  --,("M--",  "dec limit",decreaseLimit)
  --,("C-]", "next non empty",moveTo Next NonEmptyWS)
  --,("C-[",  "prev non empty",moveTo Prev NonEmptyWS)
-- ,("M-1" ,"firefox"	, raiseNext (className =? "Firefox"))
-- ,("M-2" ,"emacs"	, raiseNextMaybe (spawn "emacsclient -c -a 'emacs' ") (className =? "Emacs"))
  -- ,("C-<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
                      --,("M-<Tab>", "windows",alttab)--windows W.focusDown)
               -- ,("M-<Tab>" , "cycleWs",cycleRecentWS [xK_Super_L] xK_Tab xK_g)
  ("S-<Return>","overview",makeGrid1 gridList)
                     , ("M1-<Tab>", "cycleWin" ,cycleRecentWindows [xK_Alt_L] xK_Tab xK_w)
                    --,("M-q" , "kill all",killAll)
  ,("M1--","decrease sound",spawn  "amixer set Master 10%- unmute")
  ,("M1-=","increase sound",spawn  "amixer set Master 10%+ unmute")
  -- ,("M1-<Tab>","alt tab",nextMatch Backward (fmap not (className =? "Alacritty")))
  ,("M1-S-<Tab>","alt tab",nextMatch Forward (fmap not (className =? "Alacritty")))
  -- ,("C-S-[", "only prevWs", moveTo Prev EmptyWS)
  -- ,
  -- ("C-S-]", "only prevWs", moveTo Next EmptyWS)
  -- ,("M-e", "only prevWs", moveTo Prev EmptyWS)
  ,("S-<Space>","overview",makeGrid1 . fillter'' $   mainList <> superList )-- gridList)

                     -- ,("C-z e", "prev empty", moveTo Prev EmptyWS)
                    --,("M-2","screenshot",spawn $ "scrot")
                    -- ,("C-z n", "prev non empty",moveTo Prev NonEmptyWS)
  ]
nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
origin          = WSIs (return (\ws -> W.tag ws == "origin"))
nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
emptyNonNSP  = WSIs (return (\ws -> isNothing (W.stack ws) && W.tag ws /= "NSP"))


redList = [
  ("restart",spawn "systemctl --user restart redshift.service")
  ,("urestart",spawn "sudo nixos-rebuild switch ; systemctl --user restart redshift.service")
  ,("stop",spawn "systemctl --user stop redshift.service")
  ,("open",spawn $  myTerminal <> " -e sudo nvim /etc/nixos/redshift/default.nix")
  ]

xmonadList :: [(String,X())]
xmonadList = [
  -- ("restart",spawn "xmonad --recompile ; xmonad --restart && notify-send \"xmonad recompiled successfully \" ")
  ("restart",spawn $ "rm ~/.xmonad/xmonad-x86_64-linux ; " <> myUpdate)
  ,("open",spawn $ myEditor <> " ~/aios/xsession/xmonad.hs")
  ,("stop",io $ exitWith ExitSuccess)
  ,("doc",spawn $ myBrowser <> " https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Doc-Extending.html")
                 ]

soundList :: [(String,X())]
soundList = [
  ("decrease",inputPrompt googleXPConfig "Decrease sound " ?+ decSound)
  ,("increase",  inputPrompt googleXPConfig "Increase sound " ?+ incSound) --spawn $ "~/vc/sound/increase.sh")
            ]

appList = [
  ("emacs",shellPrompt emacsXPConfig)
  ,("zathura",shellPrompt zathuraXPConfig)
  ,("spotify", spawn  myMusic)
       ]

projects :: [Project]
projects =
  [ Project { projectName      = "P.Docs"
            , projectDirectory = "~/"
            , projectStartHook = Just $ do
                spawn "zathura ~/books/hf1p.pdf"
            }

  , Project { projectName      = "M.Music"
            , projectDirectory = "~/"
            , projectStartHook = Just $ do spawn "spotify"
            }
  ]
projectList = [
                ("goto", switchProjectPrompt shellXPConfig)
                ,("throw", shiftToProjectPrompt shellXPConfig)
              ]

exitList :: [(String,String,X())]
exitList = [
                ("shutdown","Off the computer",spawn "poweroff")
                ,("restart","Off and reopen",spawn "reboot")
           ]

nixosList = [
                ("update",spawn myUpdate )
                ,("homeManager",spawn $ myBrowser ++ " https://rycee.gitlab.io/home-manager/options.html")
                ,("store" , promptSearchBrowser googleXPConfig myBrowser nixosPkgs)--spawn "~/vc/firefox/google.sh")
                ,("packages",spawn $ myEditor <> "~/aios/pkgs/applications/default.nix")
                ,("flakes",spawn $  myEditor <> "~/aios/flake.nix")
            ]

gitList = [("vamshi5070" , spawn $ myBrowser <> " https://gitlab.com/vamshi5070/")
          , ("dt", spawn $ myBrowser <> " https://gitlab.com/dwt1/dotfiles/")
          , ("alonzoAlan", spawn $ myBrowser <> " https://github.com/alonzoAlan")

          ]

replList :: [(String,X())]
replList = [
                ("ghci",spawn $ myTerminal <> " -e ghci" )
                ,("python",spawn $ myTerminal <> " -e python" )
            ]

desktopList = [
            ("nothing",return ())
           ,("alacritty",namedScratchpadAction myScratchPads "terminal")
           ,("windows", alttab)--goToSelected def {gs_cellheight = 100,gs_cellwidth=400,gs_font="xft:Lucida MAC:size=24"})
           ,("delete",  makeGrid2  deleteList)
           ,("redshift",  makeGrid2 redList)
           ,("nixos",makeGrid2 nixosList)
           ,("bluetooth", do
                             spawn $ "pavucontrol ; " <>  myTerminal <> " -e bluetoothctl")
             -- ,("apps" , makeGrid2 appList)
           ,("xshell",shellPrompt shellXPConfig)
           ,("run launcher",runOrRaisePrompt shellXPConfig)         -- Toggles struts
           ,("google" , promptSearchBrowser googleXPConfig myBrowser google)--spawn "~/vc/firefox/google.sh")
           ,("youtube" , promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")
              ]

gridList = [
            ("nothing",return ())
           ,("alacritty",namedScratchpadAction myScratchPads "terminal")
           --windows W.focusDown)
           ,("windows", alttab)--goToSelected def {gs_cellheight = 100,gs_cellwidth=400,gs_font="xft:Lucida MAC:size=24"})
           -- ,("spotifyy",namedScratchpadAction myScratchPads "spotify")
           ,("redshift",  makeGrid2 redList)
           ,("git",  makeGrid2 gitList)
           ,("xmonad",  makeGrid2  xmonadList)
           ,("emacs",  makeGrid2   $ fillter''  emacsList)
           ,("noBorders",sendMessage $ MT.Toggle NOBORDERS)
           ,("firefox",spawn  myBrowser)
           --,("exit",  makeGrid2 $ fillter exitList)
           ,("project",  makeGrid2  projectList)
           -- ,("flatpak",  makeGrid2  flatList)
           ,("sound",makeGrid2 soundList)
           ,("goTo", makeGrid2 goToList)
           ,("nixos",makeGrid2 nixosList)
           ,("delete",  makeGrid2  deleteList)
           ,("google" , promptSearchBrowser googleXPConfig myBrowser google)--spawn "~/vc/firefox/google.sh")
           ,("youtube" , promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")
           -- ,("youtube",spawn "~/vc/firefox/youtube.sh")
           ,("fulls", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
           -- , ("mark",markNoTaskbar withFocused )
           ,("apps" , makeGrid2 appList)
           ,("bluetooth", do
                             spawn $ "pavucontrol ; " <>  myTerminal <> " -e bluetoothctl")
                             -- kill )
           ,("layout",makeGrid2 layoutList)
           -- ,("nextLayout",sendMessage NextLayout)
           ,("xshell",shellPrompt shellXPConfig)
           ]

webList = [
  ("google" , promptSearchBrowser googleXPConfig myBrowser google)--spawn "~/vc/firefox/google.sh")
  ,("youtube" , promptSearchBrowser youtubeXPConfig myBrowser youtube)--spawn "~/vc/firefox/google.sh")

          ]

deleteList =    [
        ("one",kill)
      , ("all" , killAll)
      -- ,("workspace", removeWorkspace)
      ]


layoutList = [("tall",sendMessage $ JumpToLayout "tall")
             ,("magnify",sendMessage $ JumpToLayout "magnify")
             ,("tabs",sendMessage $ JumpToLayout "tabs")
             ,("threeRow",sendMessage $ JumpToLayout "threeRow")
             ]

goToList = [
           ("empty", moveTo Prev emptyNonNSP)
           ,("nonEmpty", moveTo Prev nonEmptyNonNSP)
           ]

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 9
           $ mySpacing 0
           $ ResizableTall 1 (3/100) (1/2) []

magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []

monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full

floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat

grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)

spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)

threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)

threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)

tabs     = renamed [Replace "tabs"]
           $ tabbed shrinkText myTabTheme

tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion

wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- The layout hook
myLayoutHook = screenCornerLayoutHook $ limitWindows 6 $ avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth  tall
                                 ||| magnify
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion

myManageHook = composeAll
                            -- for className: xprop | grep WM_CLASS
                            [ className =? "MPlayer"        --> doFloat
                            , className =? "Gimp"           --> doFloat
                            , className =? "confirm"         --> doFloat
                            , className =? "file_progress"   --> doFloat
                            , className =? "dialog"          --> doFloat
                            , className =? "download"        --> doFloat
                            , className =? "error"           --> doFloat
                            , className =? "mpv"           --> doShift "M.Video"
                            , className =? "Firefox"           --> doShift "M.Browser"
                            , className =? "Emacs"           --> doShift "P.Editor"
                            -- , className =? "Emacs"           --> doShift "Programming.Editor"
                            , className =? "Zathura"           --> doShift "P.Docs"
                            , className =? "Gimp"            --> doFloat
                            , className =? "notification"    --> doFloat
                            , className =? "pinentry-gtk-2"  --> doFloat
                            , className =? "splash"          --> doFloat
                            , className =? "toolbar"         --> doFloat
                            , className =? "Yad"             --> doCenterFloat
                            , className =? "Spotify"        --> doFloat
                            , resource  =? "desktop_window" --> doIgnore
                            , resource  =? "kdesktop"       --> doIgnore
                            , isFullscreen -->  doFullFloat
                            ] <+> namedScratchpadManageHook myScratchPads

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Apple garamond:bold:size=70"
    , swn_fade              = 1.0
    , swn_bgcolor           =  "#1c1f24"   --"#000000"
    , swn_color             = "#FFFFFF"
    }

fillter xs = [(x,z) | (x,_,z) <- xs]
fillter' xs = [(myModifier <> x , z) | (x,_,z) <- xs]
fillter'' xs = [(y,z) | (_,y,z) <- xs]

modalmap :: M.Map (KeyMask, KeySym) (X ()) -> X ()
modalmap s = submap $ M.map (>> modalmap s) s

addKeys = [ ("M-e", modalmap . M.fromList $
            [ ((0, xK_r), spawn "alacritty")
            , ((0, xK_w), spawn "firefox")
            , ((0, xK_s), spawn "kitty")
            , ((0, xK_d), spawn "qutebrowser")
            , ((0, xK_c), kill)
            --, ((shiftMask, xK_w), spawn "rofi -show windowcd -show-icons")
            --, ((0, xK_c), spawn "rofi -show combi -show-icons")
            --, ((0, xK_k), spawn "rofi -show keys -show-icons")
            ])
                ]

main = do
          -- xmproc <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/.xmobarrc"
          xmonad $ pagerHints
            $  additionalNav2DKeys (xK_k, xK_h, xK_j, xK_l)
                                        [(mod4Mask,               windowGo  ),
                                         (mod4Mask .|. shiftMask, windowSwap)]
                                        False
                  -- $ addDescrKeys ((mod4Mask, xK_F1), xMessage) myKeys'
                  $ dynamicProjects projects
                  $ ewmh def {
          -- simple stuff
            terminal           = myTerminal,
            focusFollowsMouse  = myFocusFollowsMouse,
            clickJustFocuses   = myClickJustFocuses,
            borderWidth        = myBorderWidth,
            modMask            = myModMask,
            workspaces         = TS.toWorkspaces  myWorkspaces,
            normalBorderColor  = myNormColor,
            focusedBorderColor = myFocusColor,
          -- key bindings
            -- keys               = myKeys,
            -- keys               = mainKeymap,
            mouseBindings      = myMouseBindings,
          -- hooks, layouts
            layoutHook         = minimize . BW.boringWindows $ showWName' myShowWNameTheme  myLayoutHook ,
            manageHook =  myManageHook <+> manageDocks <+> raiseHook <+> noTaskbar  ,
            handleEventHook    =  serverModeEventHookCmd
                                   <+> serverModeEventHook
                                   <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                                   <+> docksEventHook
                                   <+>  minimizeEventHook
                                   <+> screenCornerEventHook
                                   <+> handleTimerEvent,
            logHook            =  workspaceHistoryHook >> myLogHook,
            startupHook        = myStartupHook
            --, logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
            --                  { ppOutput = \x -> hPutStrLn xmproc x
            --                  , ppCurrent = xmobarColor "#c792ea" "" . wrap "<box type=Bottom width=2 mb=2 color=#c792ea>" "</box>"         -- Current workspace
            --                  , ppVisible = xmobarColor "#c792ea" "" -- . clickable              -- Visible but not current workspace
            --                  , ppHidden = xmobarColor "#82AAFF" "" . wrap "<box type=Top width=2 mt=2 color=#82AAFF>" "</box>" -- . clickable -- Hidden workspaces
            --                  , ppHiddenNoWindows = xmobarColor "#82AAFF" ""
            --                  --, ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]" -- Current workspace in xmobar
            --                  --, ppVisible = xmobarColor "#98be65" ""                -- Visible but not current workspace
            --                  --, ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
            --                  --, ppHiddenNoWindows = xmobarColor "#c792ea" ""        -- Hidden workspaces (no windows)
            --                  , ppTitle = xmobarColor "#b3afc2" "" . shorten 60     -- Title of active window in xmobar
            --                  , ppSep =  "<fc=#666666> <fn=5>|</fn> </fc>"                     -- Separators in xmobar
            --                  , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
            --                  , ppExtras  = [windowCount]                           -- # of windows current workspace
            --                  , ppOrder  = \(ws:l:t:ex) ->  [(\xs -> tail $ dropWhile (/=' ') xs)l] <> ex <> [t]
            --                  }
         } `additionalKeysP` (fillter' mainList  <> fillter' emacsList <> fillter superList)
  -- -- addKeys

myLogHook :: X ()
myLogHook = do
  historyHook
  fadeInactiveLogHook fadeAmount
    where fadeAmount = 1.0


myTerm "alacritty" = "Alacritty"
myTerm "kitty" = "kitty"

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal -- ++ " -n scratchpad"
    findTerm   = className =?  myTerm myTerminal
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.50
                 w = 0.84
                 t = 0
                 l = 0.1
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnSpotify  =  "spotify"-- ++ " -n scratchpad"
    findSpotify   = className =? "Spotify"--myTerminal
    manageSpotify = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w

alttab = goToSelected def {gs_cellheight = 100,gs_cellwidth=390,gs_font="xft:Lucida MAC:size=20",gs_navigate = myNavigation,gs_colorizer = myColorizer'}

makeGrid1 = runSelectedAction (gsconfig1 myColorizer)

makeGrid3 = runSelectedAction (gsconfig3 myColorizerH)

makeGrid2 = runSelectedAction (gsconfig2 myColorizer)

myColorizer' :: a -> Bool -> X (String,String)
myColorizer' _ True = return ("#46eed9","#000000")
myColorizer' _ False = return ("#000000","#dddddd")

myColorizer :: a -> Bool -> X (String,String)
myColorizer _ True = return ("#46d9ee","#000000")
myColorizer _ False = return ("#000000","#dddddd")

myColorizerH :: a -> Bool -> X (String,String)
myColorizerH _ True = return ("#000000","#ffffff")
myColorizerH _ False = return ("#000000","#000000")


myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch navNSearch)--myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_Tab)   , moveNext     >> myNavigation)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         ,((0,xK_space) , select)--setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

mynavNSearch = makeXEventhandler $ shadowWithKeymap navNSearchKeyMap navNSearchDefaultHandler
  where navNSearchKeyMap = M.fromList [
          ((0,xK_Escape) , cancel)
          ,((0,xK_space)     , select)
          ,((controlMask,xK_e)     , select)
          ,((0,xK_Return)     , select)
          ,((0,xK_Left)       , move (-1,0) >> mynavNSearch)
          ,((0,xK_Right)      , move (1,0) >> mynavNSearch)
          ,((0,xK_Down)       , move (0,1) >> mynavNSearch)
          ,((0,xK_Up)         , move (0,-1) >> mynavNSearch)
          ,((0,xK_Tab)        , moveNext >> mynavNSearch)
          ,((shiftMask,xK_Tab), movePrev >> mynavNSearch)
          ,((0,xK_BackSpace), transformSearchString (\s -> if s == "" then "" else init s) >> navNSearch)
          ]
        navNSearchDefaultHandler (_,s,_) = do
          transformSearchString (++ s)
          mynavNSearch

gsconfig2 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 100,
                                                         gs_cellwidth = 200 ,
                                                         gs_navigate = mynavNSearch,
                                                         gs_font = "xft:Lucida MAC:size=23"}

gsconfig3 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 200,
                                                         gs_cellwidth = 500 ,
                                                         gs_navigate = mynavNSearch,
                                                         gs_font = "xft:Lucida MAC:size=33"}


gsconfig1 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 70,
                                                         gs_cellwidth = 150 ,
                                                         gs_navigate = mynavNSearch ,
                                                         gs_bordercolor = myFocusColor,
                                                         gs_font = "xft:Lucida MAC:size=18"}

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings XConfig {XMonad.modMask = modm} = M.fromList
    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), \w -> focus w >> windows W.shiftMaster)
    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

      -- Configuration options for treeSelect
myTsConfig :: TS.TSConfig a
myTsConfig = TS.TSConfig { TS.ts_hidechildren = True
                                    , TS.ts_background   = 0xdd292d3e
                                    , TS.ts_font         = myTreeFont
                                    , TS.ts_node         = (0xffd0d0d0, 0xff202331)
                                    , TS.ts_nodealt      = (0xffd0d0d0, 0xff292d3e)
                                    , TS.ts_highlight    = (0xffffffff, 0xff755999)
                                    , TS.ts_extra        = 0xffd0d0d0
                                    , TS.ts_node_width   = 300
                                    , TS.ts_node_height  = 50
                                    , TS.ts_originX      = 100
                                    , TS.ts_originY      = 200
                                    , TS.ts_indent       = 80
                                    , TS.ts_navigate     = TS.defaultNavigation
                                    }

myFontAwsm = "xft:Font awesome:size=23"
myTree =  TS.treeselectAction myTsConfig
         [ Node (TS.TSNode "Hello"    "displays hello"      (spawn "xmessage hello!")) []
         , Node (TS.TSNode "Shutdown" "Poweroff the system" (spawn "shutdown")) []
         , Node (TS.TSNode "Brightness" "Sets screen brightness using xbacklight" (return ()))
             [ Node (TS.TSNode "Bright" "FULL POWER!!"            (spawn "xbacklight -set 100")) []
             , Node (TS.TSNode "Normal" "Normal Brightness (50%)" (spawn "xbacklight -set 50"))  []
             , Node (TS.TSNode "Dim"    "Quite dark"              (spawn "xbacklight -set 10"))  []
             ]
         ]

vamTree = TS.treeselectAction myTsConfig $ header exit (tailList exitList) <> (tailList  exitList)

header [(a,b)] xs = [Node (TS.TSNode a b (return ())) xs]

--treeList = [("ls","emacs",spawn "alacritty")]

tailList xs = [Node (TS.TSNode a b (c)) [] | (a,b,c) <- xs]

exit = [("2b","Menu")]

myWorkspaces :: Forest String
myWorkspaces = [ Node "origin" [] -- a workspace for your browser
               , Node "M"       -- for everyday activity's
                   [ Node "Music" []   --  with 4 extra sub-workspaces, for even more activity's
                   , Node "Browser" []
                   , Node "Video" []
                   , Node "Docs" []
                   , Node "Misc" []
                   ]
               , Node "P" -- for all your programming needs
                   [ Node "Haskell" []
                   , Node "Docs"    [] -- documentation
                   , Node "Editor"   []
                   ]
               ]

-- $usage
--
-- You can use this module with the following in your @xmonad.hs@ file:
--
-- > import XMonad.Hooks.TaffybarPagerHints (pagerHints)
-- >
-- > main = xmonad $ ewmh $ pagerHints $ def
-- > ...

-- | The \"Current Layout\" custom hint.
xLayoutProp :: X Atom
xLayoutProp = getAtom "_XMONAD_CURRENT_LAYOUT"

-- | The \"Visible Workspaces\" custom hint.
xVisibleProp :: X Atom
xVisibleProp = getAtom "_XMONAD_VISIBLE_WORKSPACES"

-- | Add support for the \"Current Layout\" and \"Visible Workspaces\" custom
-- hints to the given config.
pagerHints :: XConfig a -> XConfig a
pagerHints c =
  c { handleEventHook = handleEventHook c <> pagerHintsEventHook
    , logHook = logHook c <> pagerHintsLogHook
    }

-- | Update the current values of both custom hints.
pagerHintsLogHook :: X ()
pagerHintsLogHook = do
  withWindowSet
    (setCurrentLayoutProp . description . W.layout . W.workspace . W.current)
  withWindowSet
    (setVisibleWorkspacesProp . map (W.tag . W.workspace) . W.visible)

-- | Set the value of the \"Current Layout\" custom hint to the one given.
setCurrentLayoutProp :: String -> X ()
setCurrentLayoutProp l = withDisplay $ \dpy -> do
  r <- asks theRoot
  a <- xLayoutProp
  c <- getAtom "UTF8_STRING"
  let l' = map fromIntegral (encode l)
  io $ changeProperty8 dpy r a c propModeReplace l'

-- | Set the value of the \"Visible Workspaces\" hint to the one given.
setVisibleWorkspacesProp :: [String] -> X ()
setVisibleWorkspacesProp vis = withDisplay $ \dpy -> do
  r  <- asks theRoot
  a  <- xVisibleProp
  c  <- getAtom "UTF8_STRING"
  let vis' = map fromIntegral $ concatMap ((++[0]) . encode) vis
  io $ changeProperty8 dpy r a c propModeReplace vis'

-- | Handle all \"Current Layout\" events received from pager widgets, and
-- set the current layout accordingly.
pagerHintsEventHook :: Event -> X All
pagerHintsEventHook ClientMessageEvent
                      { ev_message_type = mt
                      , ev_data = d
                      } = withWindowSet $ \_ -> do
  a <- xLayoutProp
  when (mt == a) $ sendLayoutMessage d
  return (All True)
pagerHintsEventHook _ = return (All True)

-- | Request a change in the current layout by sending an internal message
-- to XMonad.
sendLayoutMessage :: [CInt] -> X ()
sendLayoutMessage (x:_) | x < 0     = sendMessage FirstLayout
                        | otherwise = sendMessage NextLayout
sendLayoutMessage [] = return ()
