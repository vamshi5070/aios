{ pkgs, ... }:

let

  my-emacs = pkgs.emacsWithPackagesFromUsePackage {
    config = "~/aios/programs/emacs/init.el";
    package = pkgs.emacsGcc;
    alwaysTangle = true;
    extraEmacsPackages = epkgs: [
      epkgs.exwm
      epkgs.emacsql-sqlite
      epkgs.vterm
      epkgs.pdf-tools
    ];
  };
  exwm-load-script = pkgs.writeText "exwm-load.el" ''
    (progn
      (require 'exwm)
      ;;(configure-ivy-posframe-for-exwm)
      ;;(require 'exwm-config)
     ;; (exwm-config-default))
     (exwm-enable)
     (add-hook 'exwm-update-class-hook
          (lambda ()
          (exwm-workspace-rename-buffer exwm-class-name)))
     (setq exwm-input-prefix-keys
     '(?\C-x
       ?\C-u
       ?\C-h
       ?\M-x
       ?\M-`
       ?\M-&
       ?\M-:
       ?\C-\M-j ;; Buffer list
       ?\C-\M-b ;; Buffer list
       ?\C-\   ;; Ctrl+Space
       ?\M-\ ))  ;; Meta+Space
;; Ctrl+Q will enable the next key to be sent directly
  (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

(defun run-xmenu ()
      (interactive)
  (call-process-shell-command "~/aios/xmenu/xmenu.sh" nil 0))

  ;; Workspace switching
   (setq exwm-input-global-keys
	  `(([?\s-\C-r] . exwm-reset)
	    ([?\s-w] . exwm-workspace-switch)
	    ;;([?\s-r] . hydra-exwm-move-resize/body)
	    ([?\s-e] . dired-jump)
	    ([?\s-E] . (lambda () (interactive) (dired "~")))
	    ([?\s-C] . (lambda () (interactive) (kill-buffer)))
	    ([?\s-`] . run-xmenu)
	    ;;([?\s-`] . (lambda () (interactive) (exwm-workspace-switch-create 0)))
	    ,@(mapcar (lambda (i)
			`(,(kbd (format "s-%d" i)) .
			   (lambda ()
			    (interactive)
			    (exwm-workspace-switch-create ,i))))
		       (number-sequence 0 9))))

(setq exwm-workspace-number 5)
 (require 'exwm-randr)
      (setq exwm-randr-workspace-monitor-plist '(0 "eDP-1"))
      (add-hook 'exwm-randr-screen-change-hook
             (lambda ()
               (start-process-shell-command
                "xrandr" nil "xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal")))
      (exwm-randr-enable)
      )
  '';
in {
  xsession = {
    enable = true;
    windowManager.command = ''
          ${my-emacs}/bin/emacs -l "${exwm-load-script}"
         '';
    initExtra = ''
      xset r rate 200 100
    '';
  };
}

# { config, lib, pkgs, ... }:
# {
#   xsession.windowManager.xmonad = {
#     enable = true;
#     enableContribAndExtras = true;
#     config =./xmonad.hs;
#   };
# }
