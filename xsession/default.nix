{pkgs, ... }:
{
  xsession.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
    extraPackages =  haskellPackages: [
  haskellPackages.taffybar
];
    config =./config.hs;
  };
}
