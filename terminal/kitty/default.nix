{ pkgs, config, lib, inputs, ... }:{

  programs.kitty = {
    enable = true;
    theme = "Gruvbox Material Dark Hard";
    settings = {
      # font_family        = "Source Code Pro Semibold";
      #font_family        = "mononoki Nerd Font mono";
      font_family        = "Hasklug Nerd Font Mono";
      bold_font          = "auto";
      italic_font        = "auto";
      bold_italic_font   = "auto";
      font_size          = "21.0";
      background_opacity = "1.0";
      cursor_shape = "beam";

			# include            = "~/aios/programs/kitty/themes/Dracula.conf";
			#include            = "./Borland.conf";
    };
  };

}
