{ ... }:
{
  programs.starship = {
    enable = true;
    enableFishIntegration = true;
    enableBashIntegration = true;
    enableZshIntegration = true;

    settings = {
      add_newline = false;
      # character.symbol = ".";
      character = {
        success_symbol = "[λ>](bold green)";
        error_symbol = "[λ<](bold red) ";
        vicmd_symbol = "[V](bold green) ";
      };
      line_break = {
        disabled = false;
        directory = { truncation_length = "1"; };
        package = { disabled = true; };
      };
     };
   };

}
