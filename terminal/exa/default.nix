{ pkgs, config, lib, inputs, ... }:{
   programs.exa = {
      enable = true;
      enableAliases = true;
   };
}
