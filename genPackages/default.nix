{ pkgs, ... }:
{
  home.packages = with pkgs; [
    # lambda-launcher
    cachix
    # stack
    # haskell.compiler.ghc902
    helix
    # ghc
    cabal-install
    eww
    wmctrl
    exercism
    nitrogen
    # cmatrix
    vscode
    # nodejs
    # libnotify
    # brightnessctl
    # autoandr
    brave
    # vlc
    feh
    unzip
    zip
    # trayer
    # nm-applet
    # nixfmt    neofetch
    transmission-gtk
    # lxappearance
    pcmanfm
    # gnome.gnome-disk-utility
    # libnotify
    # pipes
    # ngrok
    # konsole
    gimp
    libreoffice
    # texlive.combined.scheme-full
    # youtube-dl
    scrot
    # alttab
    # okular
    # vscode
    # haskellPackages.kmonad
    # konsole
    # fzf
    # aspell
    # pop-gtk-theme
  ];
}

