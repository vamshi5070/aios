#+TITLE: XMonad
#+PROPERTY: header-args :tangle config.hs

* Imports
** Xmonad
#+begin_src haskell
import XMonad 
import qualified XMonad.StackSet as W
import qualified XMonad as X
#+end_src
** System
#+begin_src haskell
import System.Exit
#+end_src

** Data
#+begin_src haskell
import Data.Maybe (fromJust,isJust)
import qualified Data.Map as M
#+end_src
** Actions
#+begin_src haskell
import XMonad.Actions.Commands
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Search
import XMonad.Actions.Submap
import XMonad.Actions.WindowGo

#+end_src

** Hooks
#+begin_src haskell
       -- hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (avoidStruts,docks, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.SetWMName

#+end_src

** Layouts
#+begin_src haskell
    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

#+end_src

** Layout modifiers
#+begin_src haskell
    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
#+end_src
** Util
#+begin_src haskell
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab
#+end_src

** Prompt
#+begin_src haskell
  --prompts
import XMonad.Prompt
import XMonad.Prompt.AppLauncher as AL
import XMonad.Prompt.ConfirmPrompt
-- import XMonad.Prompt.OrgMode
import XMonad.Prompt.Shell
import XMonad.Prompt.Window
import XMonad.Prompt.XMonad
#+end_src
* Variables
#+begin_src haskell

myLauncher :: String
myLauncher = "dmenu_run -n -fn \"Consolas:bold:size=34\""

dswitcher :: String
dswitcher = "~/aios/dmenu/windowSwitcher.sh"

xmenu :: String
xmenu = "~/aios/xmenu/xmenu.sh"

myWorkspaces :: [String]
myWorkspaces = ["λ=",">>=", "<*>","<$>","<>"]
myWorkspaceIndices = M.fromList $ zip myWorkspaces [1..] -- (,) == \x y -> (x,y)


myMainKey :: String
myMainKey = "M-"

myEditor :: String
myEditor = "emacsclient -c -a emacs"
-- myTerminal      = "kitty" --"alacritty"

myTerminal :: String
myTerminal = "kitty"

myBrowser :: String
myBrowser =  "firefox" --""

myMulBrowser :: String
myMulBrowser =  "firefox" --""

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth :: Dimension
myBorderWidth   = 2

myNormColor :: String
myNormColor   = "#282c34"   -- Border color of normal windows

myFocusColor :: String
myFocusColor  = "#ffffff"   -- Border color of focused windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length .  W.integrate' . W.stack . W.workspace . W.current . windowset

myModMask       = mod4Mask

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

myDmenu = menuArgs "dmenu" ["-l","10","-n","-fn","Lucida MAC:size=40"]
#+end_src
* Keys
#+begin_src haskell
ezKeys :: [(String,String,X())]
ezKeys = [
  ("`","prev non empty",moveTo Prev nonEmptyNonNSP)
  , ("a","weblist",makeGrid2 appList)
  ,("c f","nixos flake",spawn $ myEditor <> " ~/aios/flake.nix" )
  ,("c x","nixos xmonad",spawn $ myEditor <> " ~/aios/xsession/xmonad.hs" )
  ,("c e","nixos emacs",spawn $ myEditor <> " ~/aios/editor/emacs/rgv.org" )
  ,("c p","nixos genPackages",spawn $ myEditor <> " ~/aios/genPackages/default.nix" )
  ,("e e","recent emacs",spawn $ "emacsclient -c -n -e '(recentf-open-files)'" )
  ,("e r","restart emacs",spawn $ "systemctl --user restart emacs.service" )
  , ("f", "fullScreen",sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  ,("g" ,"google",googlePrompt )--spawn "~/vc/firefox/google.sh")
  -- KB_GROUP Window resizing
        , ("h", "shrink",sendMessage Shrink)                   -- Shrink horiz window width
  ,("i", "hdmi" , makeGrid2 hdmiList)
  , ("d" ,"date", myGround)
  ,("j","windows focus down",do
       res <- windowCount
       if res == Just "1" || res == Just "0"
         then moveTo Next nonEmptyNonNSP
         else windows W.focusDown)
  ,("k","windows focus up",do
       res <- windowCount
       if res == Just "1" || res == Just "0"
         then moveTo Prev nonEmptyNonNSP
         else windows W.focusUp)
        , ("l", "expand", sendMessage Expand)                   -- Expand horiz window width
  ,("m", "focus master",windows W.focusMaster)
  ,("n u","nixos update",  spawn "nix run \"/home/vamshi/aios#cosmos\" --impure  && notify-send \"Successfully updated\" || notify-send -u critical \"Updating failed!!\" ")
  ,("o","window Switcher",alttab)--windowPrompt windowXPConfig Goto windowMap')
  ,("p","playerctl",makeGrid2 playerCtlList)
  ,("q","shellprompt",shellPrompt shellXPConfig)
  ,("r","redshift",  makeGrid2 redList)
  ,("t","force tiling", withFocused $ windows . W.sink)
  ,("u","add workspaces",switchProjectPrompt googleXPConfig  )
  ,("x M-f","project find file",spawn $ "emacsclient -c -n -e '(project-find-file)'")
  ,("y" ,"youtube", youtubePrompt)--spawn "~/vc/firefox/google.sh")
  ,("S-e","exit",  makeGrid2 exitList)
  ,("S-f", "fullScreen", sendMessage ToggleStruts) -- Toggles noborder/full
  ,("S-m", "swapmaster", windows W.swapMaster) -- Swap the focused window and the master window
  ,("S-q","xmonad --logout",io (exitWith ExitSuccess))
  ,("S-w","windows focus up",windows W.focusUp)
  ,("S-[", "shiftToPrev ",shiftTo Next nonNSP )
  ,("S-]","shiftToNext ", shiftTo Prev nonNSP )
  ,("w","windows focus down",do
       res <- windowCount
       if res == Just "1" || res == Just "0"
         then moveTo Next nonEmptyNonNSP
         else windows W.focusDown)
  ,("<Backspace>","kill", mbackspace)
 ,("<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
  , ("<Tab>", "nextLayout", sendMessage NextLayout)           -- Switch to next layout
  ,("[","previous workspace",moveTo Prev nonNSP)
  ,("]","next workspace",moveTo Next nonNSP)
  ,("1","editor emacs", raiseNextMaybe  (confirmPrompt windowXPConfig "Emacs" $ spawn myEditor)(className =? "Emacs"))
  ,("2" , "browser", raiseNextMaybe (xmonadPromptC webList keyXPConfig ) ((className =? "Firefox") ))
  ,("3","pdf reader zathura", raiseNextMaybe (AL.launchApp zathuraXPConfig "zathura") (className =? "Zathura"))
  ,("4","spotify",raiseNextMaybe (confirmPrompt googleXPConfig "spotify" $ spawn "spotify")(className =? "Spotify") )
  ,("5","alacritty",raiseNextMaybe (confirmPrompt googleXPConfig "alacritty" $ spawn "alacritty")(className =? "Alacritty") )
  ,("`","desktop",spawn xmenu)]
  where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
        nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
miscKeys :: [(String,String,X())]
miscKeys = [--("C-<Space>" ,"" ,spawn dswitcher)
           ("M1-<Space>", "infinte recursion",runCommandConfig  myDmenu . skip1element $ ezKeys <> miscKeys )
          , ("M1--","sound decrease",spawn "amixer set Master 10%- unmute")
          -- , ("","terminal",spawn "alacritty")
          ,("M1-<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
          , ("M1-=","sound decrease",spawn "amixer set Master 10%+ unmute")           ]

skip2element xs = [(p,r)| (p,q,r) <- xs]

skip1element xs = [(q,r)| (p,q,r) <- xs]

mainTurner :: [a] -> [([a], b)] -> [([a], b)]
mainTurner key xs = [(key <> p,q)|(p,q) <- xs]


myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

vamKeys =   mainTurner myMainKey (skip2element ezKeys) <> skip2element miscKeys
#+end_src
* Gridselect
#+begin_src haskell
makeGrid2 = runSelectedAction (gsconfig2 myColorizer)


gsconfig2 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 100,
                                                         gs_cellwidth = 200 ,
                                                         gs_navigate = mynavNSearch,
                                                         gs_font = "xft:Consolas:size=23"}

mynavNSearch = makeXEventhandler $ shadowWithKeymap navNSearchKeyMap navNSearchDefaultHandler
  where navNSearchKeyMap = M.fromList [
          ((0,xK_Escape) , cancel)
          ,((0,xK_space)     , select)
          ,((controlMask,xK_e)     , select)
          ,((0,xK_Return)     , select)
          ,((0,xK_Left)       , move (-1,0) >> mynavNSearch)
          ,((0,xK_Right)      , move (1,0) >> mynavNSearch)
          ,((0,xK_Down)       , move (0,1) >> mynavNSearch)
          ,((0,xK_Up)         , move (0,-1) >> mynavNSearch)
          ,((0,xK_Tab)        , moveNext >> mynavNSearch)
          ,((shiftMask,xK_Tab), movePrev >> mynavNSearch)
          ,((0,xK_BackSpace), transformSearchString (\s -> if s == "" then "" else init s) >> navNSearch)
          ]
        navNSearchDefaultHandler (_,s,_) = do
          transformSearchString (++ s)
          mynavNSearch

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch navNSearch)--myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_Tab)     , moveNext     >> myNavigation)
         ,((mod4Mask,xK_w), moveNext     >> myNavigation)
         ,((mod4Mask,xK_e), select)
         ,((mod4Mask,xK_q), select)
         ,((0,xK_e), select)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         -- ,((mod4Mask,xK_w) , select)--setPos (0,0) >> myNavigation)
         ,((0,xK_space) , select)--setPos (0,0) >> myNavigation)
         ,((0,xK_s) , select)--setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

alttab = goToSelected def {gs_cellheight = 100,gs_cellwidth=390,gs_font="xft:Consolas:size=20",gs_navigate = myNavigation,gs_colorizer = myColorizer'}

myColorizer' :: a -> Bool -> X (String,String)
myColorizer' _ True = return ("#46eed9","#000000")
myColorizer' _ False = return ("#000000","#dddddd")

myColorizer :: a -> Bool -> X (String,String)
myColorizer _ True = return ("#46d9ee","#000000")
myColorizer _ False = return ("#000000","#dddddd")


#+end_src

* Prompt
#+begin_src haskell
googlePrompt :: X ()
googlePrompt = promptSearchBrowser googleXPConfig myBrowser google

youtubePrompt :: X ()
youtubePrompt = promptSearchBrowser youtubeXPConfig myMulBrowser youtube


shellXPConfig :: XPConfig
shellXPConfig = greenXPConfig {
  autoComplete      = Nothing--Just 100000    -- set Just 100000 for .1 sec
  , height            = 70
  ,promptKeymap = emacsLikeXPKeymap
  , showCompletionOnTab = False
  ,  position = Top
      --CenteredAt {xpCenterY = 0.19 , xpWidth = 0.88}
   ,  font = "xft:Consolas:size=20"
  ,defaultPrompter = const "λ= "
  ,fgColor = myFocusColor
  }

windowXPConfig :: XPConfig
windowXPConfig = greenXPConfig {
   font = "xft:Consolas:size=20"
   , height            = 70
   , autoComplete      = Just 100000    -- set Just 100000 for .1 sec
   ,  position = CenteredAt {
       xpCenterY = 0.19 , xpWidth = 0.88
       }
                               }
emacsXPConfig :: XPConfig
emacsXPConfig = shellXPConfig {
  defaultPrompter = const "Emacs: "
                    -- ,defaultText = "emacsclient -c -a emacs "
  }

zathuraXPConfig :: XPConfig
zathuraXPConfig = shellXPConfig {
  defaultPrompter = const "Zathura: "
  }

keyXPConfig :: XPConfig
keyXPConfig = googleXPConfig {
  autoComplete = Just 100000
  , fgColor = myFocusColor
  , borderColor = myFocusColor
  ,defaultPrompter =  const "Firefox: "
  }

googleXPConfig :: XPConfig
googleXPConfig = greenXPConfig {
  fgColor = "white"--myFocusColor
  ,  position =  CenteredAt {xpCenterY = 0.29 , xpWidth = 0.48}
  ,  font = "xft:Consolas:size=22"
  , height            = 80
  }
youtubeXPConfig :: XPConfig
youtubeXPConfig = googleXPConfig {
  fgColor = "red"--myFocusColor
  ,bgColor = "white"--myFocusColor
  }

exitXPConfig :: XPConfig
exitXPConfig = googleXPConfig {
  fgColor = "red"--myFocusColor
  ,bgColor = "black"--myFocusColor
  }


#+end_src

* Lists
** App list
#+begin_src haskell
appList :: [(String,X ())]
appList = [("vamshi5070" , spawn $ myBrowser <> " https://gitlab.com/vamshi5070/")
          , ("dt", spawn $ myBrowser <> " https://gitlab.com/dwt1/dotfiles/")
          , ("alonzoAlan", spawn $ myBrowser <> " https://github.com/alonzoAlan")
          , ("bluetooth",spawn $ "pavucontrol ;" <> myTerminal <> " -e bluetoothctl")
         ,("homeManager",spawn $ myBrowser ++ " https://rycee.gitlab.io/home-manager/options.html")
         ,("whatsapp",spawn $ myBrowser <> " web.whatsapp.com")
         ,("search", spawn $ "~/aios/dmenu/nixos")
        , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
        , ("M-M1-k", sendMessage MirrorExpand)          -- Expand vert window width
          ]

#+end_src
** Web list
#+begin_src haskell
webList ::  [(String,X())]
webList = [
  ("google" , googlePrompt)--spawn "~/vc/firefox/google.sh")
  ,("youtube" , youtubePrompt)--spawn "~/vc/firefox/google.sh")
          ]

#+end_src
** Player list
#+begin_src haskell
playerCtlList :: [(String,X())]
playerCtlList = [
  ("play-pause" , spawn "playerctl play-pause")
  ,("next" , spawn "playerctl next")
  ,("prev" , spawn "playerctl prev")
                ]
#+end_src

** Exit list
#+begin_src haskell
exitList :: [(String,X())]
exitList = [
  ("shutdown",confirmPrompt exitXPConfig "Shutdown" $ spawn "poweroff")
  ,("reboot",confirmPrompt exitXPConfig "Reboot" $ spawn "reboot")
  ]
#+end_src

** RedList
#+begin_src haskell
redList :: [(String,X())]
redList = [
  ("restart",spawn "systemctl --user restart redshift.service || systemctl --user restart gammastep.service")
  ,("urestart",spawn "systemctl --user restart redshift.service || systemctl --user restart gammastep.service ")
  ,("stop",spawn "systemctl --user stop redshift.service || systemctl --user stop gammastep.service")
  ,("open",spawn $  myTerminal <> " -e sudo kak /etc/nixos/redshift/default.nix")
    ]

#+end_src
** Hdmi list
#+begin_src haskell
hdmiList :: [(String,X())]
hdmiList = [
  ("hdmi",spawn  "xrandr --output eDP-1 --off ; xrandr --output HDMI-1 --auto")
  ,("edp",spawn  "xrandr --output HDMI-1 --off ; xrandr --output eDP-1 --auto ")
    ]

#+end_src
* window
#+begin_src haskell
-- | A map from window names to Windows, given a windowTitler function.
windowMap'' :: (X.WindowSpace -> Window -> Int -> X String) -> X (M.Map String Window)
windowMap'' titler = do
  ws <- gets X.windowset
  M.fromList . concat <$> mapM keyValuePairs (W.workspaces ws)
  where keyValuePairs ws = mapM (keyValuePair ws) $ zip (W.integrate' (W.stack ws)) [0..]
        keyValuePair ws (w,num) = flip (,) w <$> titler ws w num

decorateName :: X.WindowSpace -> Window -> Int -> X String
decorateName ws w num = do
  name <- show <$> getName w
  return $ (show num) <> " " <> name <> " [" ++ W.tag ws ++ "]"

-- | A map from window names to Windows.
windowMap' :: X (M.Map String Window)
windowMap' = windowMap'' decorateName
#+end_src
* Creative functions
** mBackspace
#+begin_src haskell
mbackspace = do
       res <- windowCount
       len <- allWindows
       if  (M.size len) == 0
         then makeGrid2 exitList
         else if res == Just "0"
              then moveTo Next NonEmptyWS -- makeGrid2 exitList
              else kill
#+end_src
** Ground
#+begin_src haskell

myGround :: X ()
myGround = do
  bat <- runProcessWithInput "bash" []  "cat /sys/class/power_supply/BAT0/capacity"
  dat <- runProcessWithInput "bash" []  "date +'%d/%m/%Y'"
  day <- runProcessWithInput "bash" []  "date +'%A'"
  time <- runProcessWithInput "bash" []  "date +'%H:%M'"
  spawnSelected def{ gs_cellheight = 100,
                                                         gs_cellwidth = 300 , gs_colorizer = myColorizer , gs_font = "xft:Consolas:size=30"} [ bat, dat,day,time]
#+end_src
** myTerm
#+begin_src haskell
myTerm "alacritty" = "Alacritty"
myTerm "kitty" = "kitty"
#+end_src
* ManageHook
#+begin_src haskell
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , className =? "mpv"            --> doShift "mjm"
    , className =? "Spotify"        --> doShift "mjm"
    , className =? "Brave-browser"  --> doShift "mjm"
    , className =? "Zathura"        --> doShift "ir"
    , className =? "Firefox"        --> doShift "arr"
    , className =? "Emacs"        --> doShift "lata"
    , resource  =? "kdesktop"       --> doIgnore ]
  <+> namedScratchpadManageHook myScratchPads
#+end_src
* Spacing
** mySpacing
 Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
#+begin_src haskell
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True
#+end_src
** mySpacing'
Below is a variation of the above except no borders are applied
if fewer than two windows. So a single window has no gaps.
#+begin_src haskell
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True
#+end_src
* Layouts
#+begin_src haskell
-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }
#+end_src
* LayoutHook
#+begin_src haskell
-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout = withBorder myBorderWidth tall
                                 ||| magnify
                                 ||| floats
                                 ||| noBorders monocle
                                 ||| grid
                                 ||| noBorders tabs
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion
#+end_src
* ShowWnTheme
Theme for showWName which prints current workspace when you change workspaces.
#+begin_src haskell
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Consolas:bold:size=70"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }
#+end_src

* Event hook
#+begin_src haskell
myEventHook = docksEventHook <+> handleEventHook def <+> fullscreenEventHook --mempty
#+end_src
* Startuphook
#+begin_src haskell
myStartupHook :: X ()
myStartupHook = do
  -- spawnOnce "sudo ~/kmonad/nix/result/bin/kmonad ~/kmonad/keymap/tutorial.kbd"
  spawnOnce "sudo ~/kmonad/nix/result/bin/kmonad ~/knuth/logitech.kbd &"
  spawn "systemctl --user start start-notifier-watcher.service"
  spawnOnce "~/custom-taffybar/result/bin/taffybar &"
  spawnOnce "brightnessctl s 1"
  -- spawnOnce "xrandr --output HDMI-1 --auto"
  makeGrid2 hdmiList
  setWMName "LG3D"
#+end_src

* Main
#+begin_src haskell
  main = do
    xmproc0 <-spawnPipe "xmobar -x 0 $HOME/.config/xmobar/.xmobarrc"
      -- xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/doom-one-xmobarrc"
      -- xmproc2 <- spawnPipe "xmobar -x 2 $HOME/.config/xmobar/doom-one-xmobarrc"
    xmonad  $ docks $ ewmh def
	  {
	-- simple stuff
	  terminal           = myTerminal,
	  focusFollowsMouse  = myFocusFollowsMouse,
	  clickJustFocuses   = myClickJustFocuses,
	  borderWidth        = myBorderWidth,
	  modMask            = myModMask,
	  workspaces         = myWorkspaces,
	  normalBorderColor  = myNormColor,
	  focusedBorderColor = myFocusColor,

	-- key bindings
	  -- keys               = myKeys,
	  mouseBindings      = myMouseBindings

	-- hooks, layouts
	  ,layoutHook         = showWName' myShowWNameTheme $  myLayoutHook
	  , manageHook         = myManageHook <+> manageDocks
	  ,handleEventHook    =  myEventHook
	  , logHook            = extraLogHook $ myLogHook {ppOutput =  hPutStrLn xmproc0}
	  ,startupHook        = myStartupHook
   }
	     `additionalKeysP` vamKeys
    where extraLogHook =  dynamicLogWithPP . namedScratchpadFilterOutWorkspacePP
#+end_src

* LogHook

#+begin_src haskell
myLogHook =  xmobarPP
              -- the following variables beginning with 'pp' are settings for xmobar.
              { -- ppOutput =  >>= hPutStrLn                   -- xmobar on monitor 1
               ppCurrent = myppCurrent
              , ppVisible = myppVisible
              , ppHidden = myppHidden
              , ppHiddenNoWindows = myppHiddenNoWindows
              , ppTitle = myppTitle
              , ppSep = myppSep
              , ppUrgent = myppUrgent
              , ppExtras  = myppExtras
              , ppOrder  = myppOrder
              }

myppSep = "<fn=1> | </fn>"                 -- Separator character
myppExtras = [windowCount]                                     -- # of windows current workspace
myppCurrent = xmobarColor "#c594c5" "" . wrap "<box type=Bottom width=2 mb=2 color=#c792ea>""</box>"         -- Current workspace
myppHidden = xmobarColor "#82AAFF" "" . wrap "<box type=Top width=2 mt=2 color=#82AAFF>""</box>" --  . clickable -- Hidden workspaces
myppHiddenNoWindows = xmobarColor "#82AAFF" "" --  . clickable     -- Hidden workspaces (no windows)
myppTitle = xmobarColor "#b3afc2" "" . shorten 60               -- Title of active window
myppUrgent =  xmobarColor "#C45500" "" . wrap "!" "!"            -- Urgent workspace
myppVisible = xmobarColor "#6699cc" "" -- . clickable              -- Visible but not current workspace
myppOrder =  \(ws:l:t:ex) -> [ws,l]++ex++[t]                    -- order of things in xmobar
#+end_src

* Scratchpad

#+begin_src haskell
myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal -- ++ " -n scratchpad"
    findTerm   = className =?   myTerm myTerminal
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.50
                 w = 0.84
                 t = 0.07
                 l = 0.1
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnSpotify  =  "spotify"-- ++ " -n scratchpad"
    findSpotify   = className =? "Spotify"--myTerminal
    manageSpotify = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w
#+end_src
