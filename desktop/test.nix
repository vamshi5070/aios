{pkgs, ... }:
{
programs.xmobar = {
    enable = true;
    extraConfig = ''
Config { font    = "xft:Consolas:weight=bold:pixelsize=18:antialias=true:hinting=true"
       , additionalFonts = [ "xft:Mononoki Nerd Font:pixelsize=11:antialias=true:hinting=true"
                           , "xft:Font Awesome 5 Free Solid:pixelsize=15"
                           , "xft:Font Awesome 5 Brands:pixelsize=19"
                           ]
       --, bgColor = "#282c34"
       --, bgColor      = "#2D2A2E"
       --, fgColor = "#ff6c6b"
       --, fgColor      = "#FCFCFA"
       , bgColor      = "#1b2b34"
       , fgColor      = "#ffffff"
       , position = TopSize L 100 34 -- Static { xpos = 0 , ypos = 0, width = 1920, height = 37 }
       , lowerOnStart = True
       , hideOnStart = False
       , allDesktops = True
       , persistent = True
       , iconRoot = "/home/vamshi/aios/programs/xmobar/xpm/"  -- default: "."
       , commands = [
                    -- Time and date
                      Run Date "<fn=2>\xf017</fn> %b %d %Y - (%H:%M) " "date" 50
                      -- Network up and down
                    , Run Network "enp6s0" ["-t", "<fn=2>\xf0ab</fn>  <rx>kb  <fn=2>\xf0aa</fn>  <tx>kb"] 20
                      -- Cpu usage in percent
                    , Run Cpu ["-t", "<fn=2>\xf108</fn>  cpu: (<total>%)","-H","50","--high","red"] 20
                      -- Ram used number and percent
                    , Run Memory ["-t", "<fn=2>\xf233</fn>  mem: <used>M (<usedratio>%)"] 20
                      -- Disk space free
                    , Run DiskU [("/", "<fn=2>\xf0c7</fn>  hdd: <free> free")] [] 60
                      -- Runs custom script to check for pacman updates.
                      -- This script is in my dotfiles repo in .local/bin.
                    , Run Com "/home/dt/.local/bin/pacupdate" [] "pacupdate" 36000
                      -- Runs a standard shell command 'uname -r' to get kernel version
                    , Run Com "uname" ["-r"] "" 3600
                    , Run UnsafeStdinReader
		    , Run Battery [
         	            "-t", "<acstatus> <left>% ",
                        "--",
                        --"-c", "",
                        "-O", "<fn=2> \xf0e7</fn>",
                        "-o", "<fn=2> </fn>",
                        "-h", "<fn=2> </fn>",
                        "-l", "<fn=2> </fn>"
                    ] 10
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "%UnsafeStdinReader% }{ <box type=Bottom width=2 mb=2 color=#A9DC76><fc=#A9DC76><action=`alacritty -e htop`>%cpu%</action></fc></box> <box type=Bottom width=2 mb=2 color=#ff6c6b><fc=#d79921> <action=`alacritty -e htop`>%memory%</action> </fc></box> <box type=Bottom width=2 mb=2 color=#b3afc2> <fc=#b3afc2>%battery%</fc></box> <box type=Bottom width=2 mb=2 color=#51afef><fc=#46d9ff><action=`emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(dt/year-calendar))'`>%date%</action></fc></box>  "
       }
    '';
# <box type=VBoth mt=2 mb=2>#666666</box>
  };
}
