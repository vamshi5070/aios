{ dmenu, pkgs, ... }:
    let dmenu = pkgs.dmenu.override( { patches = [
               #./../../dmenu/patches/center.diff
                      ./instant.diff
                     # ./lineHeight.diff
                      #./grid.diff
                     # ./printIndex.diff
                           ]; } );
                              in
                              {
                          home.packages = with pkgs; [
                              dmenu
                              xmenu
                              wmctrl
                              youtube-dl
                              pavucontrol
                              libnotify
                              nitrogen
                              unzip
                          ];
                              }
