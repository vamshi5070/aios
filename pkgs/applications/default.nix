{ pkgs, ... }:
#let dmenu = pkgs.dmenu.override( { patches = [
#       ./../../dmenu/patches/center.diff
# ./../../dmenu/patches/instant.diff
#    ]; } );
#    in
{
  home.packages = with pkgs; [
    nixfmt
   # xsel
   # brave
   # libreoffice
   # playerctl
   # unzip
   # gcc
    kdenlive
    lxappearance
   taffybar
    # desmume
    # mgba
    # citra
    # pandoc
    fd
    ripgrep
    # findutils
    spotify
    # wmctrl
    transmission-gtk
    # xmenu
    youtube-dl
    texlive.combined.scheme-full
    pcmanfm
    #texlive.combined.scheme-minimal
    #pcmanfm
    #pavucontrol
    # libnotify
    #tree
    #neofetch
    #pfetch
    #mpc_cli
    #xfce.xfce4-terminal
    #zathura
    #mpv
    #xorg.xinit
    #audacity
    #kdenlive
    # sublime
    #gimp
    #scrot
    #xwallpaper
    #nitrogen
    #feh
    #xorg.xmodmap
    gnome.gnome-disk-utility
    # gnome.gnome-tweaks
    #gnomeExtensions.google-search-provider
    #gnomeExtensions.dash-to-dock
    ## gnomeExtensions.youtube-search-provider
    # gnomeExtensions.blur-my-shell
    # gnomeExtensions.blur-me
    # gnome.gnome-shell-extensions
    # gnomeExtensions.ddterm
    #gnomeExtensions.run-or-raise
    # gnomeExtensions.drop-down-terminal
    #dracula-theme
    #orchis-theme
    #nordic
    # whitesur-icon-theme
    # papirus-icon-theme
    # slock
    # alttab
    # vlc
    # jumpapp
    # moka-icon-theme
    # numix-icon-theme-circle
    # arc-icon-theme
    #eclipses.eclipse-java
    #transmission
    #termonad-with-packages
    #redshift
    #alttab
    #haskell-language-server
    #ormolu
    #hlint
    #ghcid
    #cabal2nix
    #cabal-install
    # gnomeExtensions.material-shell
    #gnome-latex
    #unzip
    # conky
    # dmenu
  ];
}

