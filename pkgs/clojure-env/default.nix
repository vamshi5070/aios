{ config, lib, pkgs, ... }:
{
  home.packages = with pkgs; [
    clojure
   leiningen
    #xfce.xfce4-terminal
    #zathura
    #mpv
    #xorg.xinit
    #kdenlive
  ];
}

