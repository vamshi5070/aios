{ config, lib, pkgs, ... }:
let
    haskell-env = pkgs.haskellPackages.ghcWithHoogle (
  hp: with hp; [
random
  # hlint
  xmonad
  xmonad-contrib
  # brittany
  # hlint
 #  lsp
 #  ghcup
  #pretty-simple
 #  hindent
 #  aeson
 #  split
 #  multimap
 #  tuple
 #  hostname
 #  safe
 #  hspec
 # HUnit
 # QuickCheck
 # yi
 ]
);

in
{
  home.packages = with pkgs; [
    haskell-env
  ];
}

